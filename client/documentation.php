<?php
	include('includes/checklogin.php');
	$section="documentation";
	include('includes/global.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php
if ($lang==1) {
	echo 'Customer Access - Alphacasting';
} else {
	echo 'Zone Client - Alphacasting';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/contentheight.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
    <div class="content main">
    <h1><?php echo $usersNom; ?></h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th><?php if ($lang==1) echo 'Document Name'; else echo 'Nom du document'; ?></th>
        <th>Date</th>
        <th>&nbsp;</th>
    </tr>
    <?php
		$SQL = "SELECT * FROM client_fichiers WHERE usersID='$usersID' ORDER BY fichiersNom ASC";
		$req = mysqli_query($link,$SQL);
		if (mysqli_num_rows($req)!=0) {
			$i=1;
			while ($enr=mysqli_fetch_assoc($req)) {
				echo '<tr class='.(($i%2)?'"pair"':'"impair"').'>';
				echo '<td class="nomFichier">'.$enr['fichiersNom'].'</td>';
				echo '<td class="dateFichier">'.$enr['date'].'</td>';
				echo '<td class="supFichier"><a href="download.php?file='.md5($enr['fichiersID']).'">'.(($lang==1)?'Download':'Télécharger').'</a></td>';
				echo '</tr>';
				$i++;
			}
			echo '<tr class="total">';
			echo '<td colspan="6">Total : '.mysqli_num_rows($req).' document'.((mysqli_num_rows($req)>1)?'s':'').'</td>';
			echo '</tr>';
		}
	?>
    </table>
    </div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>