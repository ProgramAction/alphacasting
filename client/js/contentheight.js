	var mainHeight;
	var headerHeight;
	
	function setMainContentHeight() {
		windowHeight = $(window).height();
		headerHeight=$('.header').outerHeight();
		footerHeight=$('.footer').outerHeight();
		paddingMain = parseInt($('.main').css('padding-top'))+parseInt($('.main').css('padding-bottom'));
		contentHeight=windowHeight-(headerHeight+footerHeight+paddingMain);
		
		$('.main').css("min-height",contentHeight);
	}
	
	$(document).ready(function() {
		setMainContentHeight();
		
	});
	
	$(window).resize(function() {
		setMainContentHeight();
	});