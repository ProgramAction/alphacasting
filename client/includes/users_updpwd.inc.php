<div class="dialog_box updpwd">
	<img src="images/close_popup.png" width="11" height="11" class="close_popup" />
    <img src="images/ico_utilisateur_bleu.jpg" width="41" height="41" />
    <h5><?php if ($lang==1) echo 'Please change your password'; else echo 'Veuillez changer votre mot de passe'; ?></h5>
    <table border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td><input type="password" name="motdepasse" id="motdepasse" placeholder="<?php if ($lang==1) echo 'New password'; else echo 'Nouveau mot de passe'; ?>" tabindex="1" /></td>
    <td rowspan="2"><input type="button" name="btn_updt_pwd" id="btn_updt_pwd" value="OK" tabindex="3" /></td>
    </tr>
    <tr>
    <td><input type="password" name="motdepasse2" id="motdepasse2" placeholder="<?php if ($lang==1) echo 'Confirm password'; else echo 'Confirmation mot de passe'; ?>" tabindex="2" /></td>
    </tr>
    </table>
    <img src="images/loading.gif" width="46" height="17" class="loading" />
</div>

<div class="dialog_box updUserInfo">
	<img src="images/close_popup.png" width="11" height="11" class="close_popup" />
    <img src="images/ico_utilisateur_bleu.jpg" width="41" height="41" />
    <h5><?php if ($lang==1) echo 'My Account'; else echo 'Mon compte'; ?></h5>
    <table border="0" cellspacing="0" cellpadding="0">
    <tr>
<?php
	if ($usersIsAdmin!=1) {
	?>
<td align="left">
<select name="langue" id="langue" tabindex="1">
<option value="2" <?php if ($usersLang==2) echo 'selected="selected"'; ?>>Français</option>
<option value="1" <?php if ($usersLang==1) echo 'selected="selected"'; ?>>Anglais</option>
</select>
</td>
<?php
	}
?>
<td rowspan="4" valign="bottom"><input type="button" name="btn_updt_info" id="btn_updt_info" value="OK" tabindex="5" /></td>
</tr>
<tr>
<?php
	if ($usersIsAdmin!=1) {
	?>
<td align="left">
<input type="text" name="email" id="email" placeholder="<?php if ($lang==1) echo 'Email'; else echo 'Courriel'; ?>" value="<?php echo $usersEmail; ?>" tabindex="2" />
</td>
<?php
	}
?>
</tr>
<tr>
<td><input type="password" name="newmotdepasse" id="newmotdepasse" placeholder="<?php if ($lang==1) echo 'New password'; else echo 'Nouveau mot de passe'; ?>" tabindex="3" /></td>
</tr>
    <tr>
    <td><input type="password" name="newmotdepasse2" id="newmotdepasse2" placeholder="<?php if ($lang==1) echo 'Confirm password'; else echo 'Confirmation mot de passe'; ?>" tabindex="4" /></td>
    </tr>
    </table>
    <img src="images/loading.gif" width="46" height="17" class="loading" />
</div>
<script type="text/javascript" src="js/jquery.placeholder.js"></script>
<script>
var utilisateursID = <?php echo $usersID; ?>;
var modpwd=0;
$(function() {
    $( ".dialog_box.updpwd" ).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		width: 410,
		<?php
		if ($usersUpdPwd==0) {
			echo 'autoOpen: false,';
		}
		?>
		open: function(){
            jQuery('#btn_updt_pwd').bind('click',function(){
				$('.dialog_box.updpwd p').remove();
				motdepasse = $('#motdepasse').val();
				motdepasse2 = $('#motdepasse2').val();
				langue = $('#langue').val();
				email = $('#email').val();
				$('.dialog_box.updpwd img.loading').show();
				$.ajax({
					type:"POST",
					url: "includes/ajax_upd_pwd.php",
					data:{motdepasse:motdepasse, motdepasse2:motdepasse2},
					cache: false
				})
				.done(function(html) {
					$('.dialog_box.updpwd p').remove();
					if (html!="") {
						$('#motdepasse, #motdepasse2').val('');
						$(html).insertAfter('.dialog_box.updpwd h5');
						$('.dialog_box.updpwd img.loading').hide();
					} else {
						location.reload();
					}
				});
                
				//jQuery('.dialog_box.updpwd').dialog('close');
            })
			jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.dialog_box.updpwd').dialog('close');
            })
			jQuery('img.close_popup').bind('click',function(){
                jQuery('.dialog_box.updpwd').dialog('close');
            })
        }
	});
	
	//Update Infos
	 $( ".dialog_box.updUserInfo" ).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		width: 410,
		autoOpen: false,
		open: function(){
            jQuery('#btn_updt_info').bind('click',function(){
				$('.dialog_box.updUserInfo p').remove();
				motdepasse = $('#newmotdepasse').val();
				motdepasse2 = $('#newmotdepasse2').val();
				langue = $('#langue').val();
				email = $('#email').val();
				$('.dialog_box.updUserInfo img.loading').show();
				
				$.ajax({
					type:"POST",
					url: "includes/ajax_upd_info.php",
					data:{motdepasse:motdepasse, motdepasse2:motdepasse2, email:email, langue:langue},
					cache: false
				})
				
				.done(function(html) {
					$('.dialog_box.updUserInfo p').remove();
					if (html!="") {
						$('#newmotdepasse, #newmotdepasse2').val('');
						$(html).insertAfter('.dialog_box.updUserInfo h5');
						$('.dialog_box.updUserInfo img.loading').hide();
					} else {
						//$(html).insertAfter('.dialog_box.updUserInfo h5');
						location.reload();
					}
				});
                
				//jQuery('.dialog_box.updpwd').dialog('close');
            })
			jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.dialog_box.updUserInfo').dialog('close');
            })
			jQuery('img.close_popup').bind('click',function(){
                jQuery('.dialog_box.updUserInfo').dialog('close');
            })
        }
	});
});

$( "a.user" ).click(function( event ) {
	$( ".dialog_box.updUserInfo" ).dialog( "open");
	event.preventDefault();
});
</script>