<div class="header">
    <div class="content">
        <a href="?lang=<?php if($lang==1) echo 'fr'; else echo 'en'; ?>" class="lang"><?php if($lang==1) echo 'Français'; else echo 'English'; ?></a>
        <?php
			if ($section!="accueil") {
		?>
        <img src="images/alphacasting_header.png" width="205" height="68" alt="Alphacasting" />
        <ul class="nav">
            <?php
                if ($usersIsAdmin==1) {
                    echo '<li><a href="utilisateurs.php"'.(($section=="utilisateurs")?'class="selected"':'').'>Gestion des utilisateurs</a></li>';
                } else {
                    echo '<li><a href="documentation.php"'.(($section=="documentation")?'class="selected"':'').'>Documentation</a></li>';
                    
                }
            ?>
            <li><a href="#" class="user"><?php if($lang==1) echo 'My Account'; else echo 'Mon compte'; ?></a></li>
            <li><a href="includes/logout.php"><?php if($lang==1) echo 'Sign Out'; else echo 'Déconnexion'; ?></a></li>
        </ul>
        <?php
			}
		?>
    </div>
    <?php
		if ($section!="accueil") {
	?>
    <div class="bandeau"></div>
	<?php
		}
	?>
</div>
<?php
	if (isset($usersID)) {
		require('includes/users_updpwd.inc.php');
	}
?>