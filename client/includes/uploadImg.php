<?php
	//Redimensionner Images 
	function redimension($tmp_file, $taille_min=600) {
		$donnees=getimagesize($tmp_file);
		$image = imagecreatefromjpeg($tmp_file);
		/*
		if ($donnees[0] > $donnees[1]) { //paysage
			$largeur_finale=round(($taille_min/$donnees[1])*$donnees[0]);
			$hauteur_finale=$taille_min;
		}
		else
		{//portrait
		*/
			$hauteur_finale=round(($taille_min/$donnees[0])*$donnees[1]);
			$largeur_finale=$taille_min;
		//}
		$image_mini = imagecreatetruecolor($largeur_finale, $hauteur_finale); //création image finale
		imagecopyresampled($image_mini, $image, 0, 0, 0, 0, $largeur_finale, $hauteur_finale, $donnees[0], $donnees[1]);//copie avec redimensionnement
		imagejpeg ($image_mini, $tmp_file);
	}
	
	//Creer un thumbnail
	function apercu($ficSource, $ficDestination, $largeurMaxDestination, $hauteurMaxDestination, $qualite=8){
		if (is_array($ficSource)){
			$ficSource = $ficSource['tmp_name'];
		}

		$imgSource = ouvrirImage($ficSource);
		$imgDestination = redim($imgSource, $largeurMaxDestination, $hauteurMaxDestination, 0.5, 0.5, $qualite);
		imagepng($imgDestination, $ficDestination, $qualite);
	}
	
	function ouvrirImage($ficSource){
		$imagesize = getimagesize($ficSource);
		switch ($imagesize['mime']) {
			case 'image/jpeg':
				$imgSource = imagecreatefromjpeg($ficSource);
			break;
			case 'image/png':
				$imgSource = imagecreatefrompng($ficSource);
			break;
			case 'image/gif':
				$imgSource = imagecreatefromgif($ficSource);
			break;
			case 'image/bmp':
				$imgSource = imagecreatefromwbmp($ficSource);
			break;
			default:
				return;
		}
		return $imgSource;
	}
	
	function redim($imgSource, $largeurDestination=48, $hauteurDestination=48, $centrageX=0.5, $centrageY=0.5, $qualite=8){
		$largeurSource = imagesx($imgSource);
		$hauteurSource = imagesy($imgSource);
		$ratioLargeurs = $largeurDestination/$largeurSource;
		$ratioHauteurs = $hauteurDestination/$hauteurSource;
		$ratio = max($ratioLargeurs, $ratioHauteurs);
		$ratio = min($ratio,1);
		$largeurImgDestination = floor($largeurSource*$ratio);
		$hauteurImgDestination = floor($hauteurSource*$ratio);
		$decalageX = floor(($largeurDestination-$largeurImgDestination)*$centrageX);
		$decalageY = floor(($hauteurDestination-$hauteurImgDestination)*$centrageY);
		
		$imgDestination = imagecreatetruecolor($largeurDestination, $hauteurDestination);
		ajouterTransparence($imgDestination);
		imagecopyresampled($imgDestination, $imgSource, $decalageX, $decalageY, 0, 0, $largeurImgDestination, $hauteurImgDestination, $largeurSource, $hauteurSource);
		return $imgDestination;
	}
	
	function ajouterTransparence($img){
		$background = imagecolorallocate($img, 0, 0, 0);
		imageColorTransparent($img, $background);
		imagealphablending($img, false);
		imagesavealpha($img, true);
	}
?>