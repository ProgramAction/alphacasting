<?php
	$baseURL = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';
	
	$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', ' - '=>'-', ' '=>'-', "'"=>'', '"'=>'', ', '=>'-', ': '=>'-', ' : '=>'-', '.'=>'-', '!'=>'', '?'=>'', '('=>'', ')'=>'', ' / '=>'-', '/'=>'-', "'\'"=>'-',"«"=>'',"»"=>''," & "=>'-',"&"=>'-' );
	
	session_start();
	if (isset($_SESSION['lang'])) {
		if (isset($_GET['lang'])) {
			if ($_GET['lang']=='fr') {
				$lang=2;
			} elseif ($_GET['lang']=='en') {
				$lang=1;
			}
			$_SESSION['lang']=$lang;
		} else {
			$lang=$_SESSION['lang'];
		}
	} else {	
		if (isset($_GET['lang'])) {
			if ($_GET['lang']=='fr') {
				$lang=2;
				
			} elseif ($_GET['lang']=='en') {
				$lang=1;
			}
			$_SESSION['lang']=$lang;
		}
	}
	
	//Champs dans BD selon la langue
	if ($lang==1) {
		$chBD="EN";
	} else {
		$chBD="FR";
	}
	
	//Titre des fenêtres popup
	if ($lang==1) {
		$titrePopUp1 = 'Email address is missing in your account';
		$titrePopUp2 = 'Your account information';
	} else {
		$titrePopUp1 = "Adresse courriel manquante dans votre compte";
		$titrePopUp2 = 'Les informations de votre compte';
	}

?>