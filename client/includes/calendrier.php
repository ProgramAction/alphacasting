<?php
/*== Constantes */
$bgBbody = "#494949";
setlocale (LC_TIME,"french");
?>
<html>
<head>
<title>CALENDRIER</title>
<style type="text/css">

.tcell {
	font-size: 10px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}
a:active { color: #000000; text-decoration: none;}
a:link { color: #000000; text-decoration: none;}
a:visited { color: #000000; text-decoration: none;}
a:hover { color: #000000; text-decoration: underline;}

.textHead {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	font-style: normal;
	font-weight: normal;
}
</style>
<script language="JavaScript">
/*function returnDate(laDate){
	alert(laDate);
}*/
function returnDate(laDate){
	parent.opener.document.<?php echo $_GET['target']; ?>.value=laDate;
	window.close();
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></head>
<body bgcolor="<?php echo $bgBbody ?>" link="#000000" vlink="#000000" alink="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="calendrier">
<table width="100%" height="100%" align="center" cellpadding="0" cellspacing="0">
<tr> 
    <td align="center" valign="middle">
	<?php 
		$m=$y='';
		mk_drawCalendar($m,$y,$_GET['target']); 
	?>
	</td>
</tr>
</table>
</form>
</body>
</html>

<?php

//*********************************************************
// DRAW CALENDAR
//*********************************************************
/*
    Draws out a calendar (in html) of the month/year
    passed to it date passed in format mm-dd-yyyy 
*/
function mk_drawCalendar($m,$y,$target)
{
    $bgColor = "#AEAAA9";
	$headerColor = "#777777";
	$weekDaysColor = "#C5C3C3";
	$emptyStartColor = "#AEAAA9";
	$emptyEndColor = "#C5C3C3";
	$todayColor = "#FFFFFF";
	$daysColor = "#AEAAA9";
	
	if (isset($_GET['m'])) {
		$m=$_GET['m'];
	}
	if (isset($_GET['y'])) {
		$y=$_GET['y'];	
	}
	if ((!$m) || (!$y))
    { 
        $m = date("m",time());
		$y = date("Y",time());
    }
	$today = date("Y-m-d", time()); // date actuelle mm-jj-aa sans "0"
	
	$arrayMois = array("Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");
	/*== get what weekday the first is on ==*/
    $tmpd = getdate(mktime(0,0,0,$m,1,$y));
    $numMonth = $tmpd["mon"]-1; // retourne le mois, num�rique 1 � 12
    $firstwday= $tmpd["wday"]; // retourne le premier jour de la semaine, num�rique. 0: dimanche jusqu'� 6: samedi 
	$month = $arrayMois[$numMonth];
	$lastday = mk_getLastDayofMonth($m,$y);
?>
<table cellpadding=2 cellspacing=0 border=1 bordercolor="#000000" width="200" align="center" bgcolor="<?php echo $bgColor ?>">
<tr><td colspan=7 bgcolor="<?php echo $headerColor ?>">
    <table width="200" cellspacing="0">
		<tr valign="top" class="textHead"> 
    		<td width="25" align="left">
            <a href="<?php echo $SCRIPT_NAME; ?>?m=<?php echo (($m-1)<1) ? 12 : $m-1; ?>&y=<?php echo (($m-1)<1) ? $y-1 : $y; ?>&target=<?php echo $target; ?>">&lt;&lt;</a>
            </td>
    		<td width="25" align="right">
            <a href="<?php echo $SCRIPT_NAME; ?>?m=<?php echo $m; ?>&y=<?php echo ($y-1); ?>&target=<?php echo $target; ?>">&lt;</a>
            </td>
			<td width="100" align="center"><?php echo $month." ".$y ?></td>
			<td width="25" align="left">
            <a href="<?php echo $SCRIPT_NAME; ?>?m=<?php echo $m; ?>&y=<?php echo ($y+1); ?>&target=<?php echo $target ?>">&gt;</a>
            </td>
    		<td width="25" align="right">
            <a href="<?php echo $SCRIPT_NAME; ?>?m=<?php echo (($m+1)>12) ? 1 : $m+1; ?>&y=<?php echo (($m+1)>12) ? $y+1 : $y; ?>&target=<?php echo $target; ?>">&gt;&gt;</a>
            </td>
  		</tr>
	</table>
</td></tr>
<tr bgcolor="<?php echo $weekDaysColor ?>">
	<td width=28 class="tcell" align="center" valign="top">D</th>
	<td width=28 class="tcell" align="center" valign="top">L</th>
    <td width=29 class="tcell" align="center" valign="top">Ma</th>
	<td width=28 class="tcell" align="center" valign="top">Me</th>
    <td width=28 class="tcell" align="center" valign="top">J</th>
	<td width=28 class="tcell" align="center" valign="top">V</th>
    <td width=28 class="tcell" align="center" valign="top">S</th>
	</tr>
<?php  $d = 1;
    $wday = $firstwday;
    $firstweek = true;

    /*== loop through all the days of the month ==*/
    while ( $d <= $lastday) 
    {

        /*== set up blank days for first week ==*/
        if ($firstweek) {
            print "<tr>";
            for ($i=1; $i<=$firstwday; $i++){
				print "<td class=\"tcell\" bgcolor=".$emptyStartColor.">&nbsp;</td>";
			}
            $firstweek = false;
        }

        /*== Sunday start week with <tr> ==*/
        //if ($wday==0) { print "<tr>"; }
		$tmpDay = $d;
		$tmpMonth = $m;
		if(strlen($tmpDay) == 1){
			$tmpDay = "0".$tmpDay;
		}
		if(strlen($tmpMonth) == 1){
			$tmpMonth = "0".$tmpMonth;
		}
		
		$tmpNewDate = $tmpDay."-".$tmpMonth."-".$y; // String de la date actuelle
		$tmpNewDate = $y."-".$tmpMonth."-".$tmpDay; // String de la date actuelle
		
		// Set la couleur de la cellule pour identifier la journee actuelle
		if($tmpNewDate == $today){
			$todayBoxColor = $todayColor;
		}else{
			$todayBoxColor = $daysColor;
		}
        /*== check for event ==*/  
        print "<td class=\"tcell\" align=\"center\" valign=\"top\" bgcolor=\"".$todayBoxColor."\">";
        
		print "<a href=\"javascript:;\" onClick=\"returnDate('".$tmpNewDate."');\">".$d."</a>";
        print "</td>\n";

        /*== Saturday end week with </tr> ==*/
        if ($wday==6) { print "</tr>\n"; }

        $wday++;
        $wday = $wday % 7; // $wday = le nbre de cases restantes � la fin
        $d++;
    }
	/*== Pour printer les cases vides � la fin */
	$boxLeft = 7 - $wday;
	if($boxLeft == 7){
		$boxLeft = 0;
	}
	for($i=0; $i < $boxLeft; $i++){
		print "<td class=\"tcell\" align=\"center\" valign=\"top\" bgcolor=".$emptyEndColor.">";
		print "&nbsp;";
		print "</td>\n";
	}
?>
</tr></table>
<?php

/*== end drawCalendar function ==*/
} 




/*== get the last day of the month ==*/
function mk_getLastDayofMonth($mon,$year)
{
    for ($tday=28; $tday <= 31; $tday++) 
    {
        $tdate = getdate(mktime(0,0,0,$mon,$tday,$year));
        if ($tdate["mon"] != $mon) 
        { break; }

    }
    $tday--;

    return $tday;
}

?>


