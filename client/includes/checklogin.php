<?php
	date_default_timezone_set('America/Montreal');
	
	$nomdusite = "ZoneClient-Alphacasting";
	$nomCookie = str_replace(" ","",$nomdusite);
	include('connexion.inc.php');
	
	$parts = Explode('/', $_SERVER['PHP_SELF']);
	$pagecourante = $parts[count($parts) - 1];
	
	$pagedestination = "documentation.php";
	$con=date("Y-m-d h:i:s");
	
	if (isset($_COOKIE[$nomCookie])) {
		$username=$_COOKIE[$nomCookie];
		$SQL = "SELECT * FROM client_users WHERE usersEmail = '$username'";
		if($req = mysqli_query($link,$SQL)){
			if(mysqli_num_rows($req)==0){
				header("Location: index.php?erreur=1");
				exit;
			} else {
				if (isset($_POST['btnLogin'])) {
					$username=mysqli_real_escape_string($link,$_POST['courriel']);
					$password=$_POST['password'];
					
					if(filter_var($username, FILTER_VALIDATE_EMAIL) && $_POST['password']!="") {
						$SQL = "SELECT usersPassword, usersIsAdmin FROM client_users WHERE usersEmail='$username'";
						$req = mysqli_query($link,$SQL);
						if(mysqli_num_rows($req)==0){
							header("Location: index.php?erreur=3");
							exit;	
						}else while($enr=mysqli_fetch_assoc($req)){
							if (md5($password)==$enr['usersPassword']) {
								setcookie($nomCookie,$username,0 , "/");
								if ($enr['usersIsAdmin']==1) {
									$pagedestination="utilisateurs.php";
								}
								header("Location: ".$pagedestination);
								exit;
							} else {
								header("Location: index.php?erreur=2");
								exit;
							}
						}
					}
				}
				$enr=mysqli_fetch_assoc($req);
				$SQL="UPDATE client_users SET usersLastCon='$con' WHERE usersID=".$enr['usersID'];
				mysqli_query($link, $SQL);
							
				$usersID=$enr['usersID'];
				$usersNom=$enr['usersPrenom']." ".$enr['usersNom'];
				$usersCie=$enr['usersCie'];
				$usersEmail=$enr['usersEmail'];
				$lang=$enr['usersLang'];
				$usersLang=$enr['usersLang'];
				$usersIsAdmin=$enr['usersIsAdmin'];
				$usersUpdPwd=$enr['usersUpdPwd'];
			}
		}
	} else {
		if (isset($_POST['btnLogin'])) {
			$username=mysqli_real_escape_string($link,$_POST['courriel']);
			$password=$_POST['password'];
			
			if(filter_var($username, FILTER_VALIDATE_EMAIL) && $_POST['password']!="") {
				$SQL = "SELECT usersID, usersPassword, usersIsAdmin FROM client_users WHERE usersEmail='$username'";
				$req = mysqli_query($link,$SQL);
				if(mysqli_num_rows($req)==0){
					header("Location: index.php?erreur=3");
					exit;	
				}else while($enr=mysqli_fetch_assoc($req)){
					if (md5($password)==$enr['usersPassword']) {
						setcookie($nomCookie,$username,0 , "/");
						$SQL="UPDATE client_users SET usersLastCon='$con' WHERE usersID=".$enr['usersID'];
						mysqli_query($link,$SQL);
						if ($enr['usersIsAdmin']==1) {
							$pagedestination="utilisateurs.php";
						}
						header("Location: ".$pagedestination);
						exit;
					} else {
						header("Location: index.php?erreur=2");
						exit;
					}
				}
			}
		} else {
			if ($pagecourante != "index.php") {
				header("Location: index.php?erreur=1");
				exit;
			}
		}
	}
?>