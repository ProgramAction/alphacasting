<?php
	include('checklogin.php');
	
	
	$motdepasse = $_POST['motdepasse'];
	$motdepasse2 = $_POST['motdepasse2'];
	$langue = $_POST['langue'];
	$email = $_POST['email'];
	
	$erreurs = array();
	
	$newPwd=0;
	if ($motdepasse!="") {
		$newPwd=1;
		if ($motdepasse!=$motdepasse2) {
			$erreurs['confirmation']=1;
		} else {
			if (strlen($motdepasse) < 4) {
				$erreurs['length']=1;
			}
		}
	}
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$erreurs['email']=1;
	}
	$SQL = "SELECT usersID FROM client_users WHERE usersEmail='$email' AND usersID<>'$usersID'";
	$req = mysqli_query($link,$SQL);
	if (mysqli_num_rows($req)!=0) {
		$erreurs['emailexiste']=1;
	}
	
	if (count($erreurs)==0) {
		$motdepasse=md5($motdepasse);
		$SQL = "UPDATE client_users SET usersEmail='$email', usersLang='$langue'".(($newPwd==1)?", usersPassword='$motdepasse', usersUpdPwd='0'":"")." WHERE usersID='$usersID'";
		mysqli_query($link,$SQL);
	} else {
		if (isset($erreurs['confirmation'])) {
			if ($lang==1) {
				echo '<p class="rouge">Please confirm your password.</p>';
			} else {
				echo '<p class="rouge">Mot de passe non-confirmé, veuillez recommencer.</p>';
			}
		}
		if (isset($erreurs['length'])) {
			if ($lang==1) {
				echo '<p class="rouge">Your password must contain at least 4 characters.</p>';
			} else {
				echo '<p class="rouge">Votre mot de passe doit contenir au moins 4 caractères.</p>';
			}
		}
		if (isset($erreurs['emailexiste'])) {
			if ($lang==1) {
				echo '<p class="rouge">This email is already registered.</p>';	
			} else {
				echo '<p class="rouge">Ce courriel est déjà enregistré.</p>';	
			}
		}
	}
?>