<?php
	include('includes/checklogin.php');
	
	$idFichierMD5 = $_GET['file'];
	
	$SQL = "SELECT * FROM client_fichiers WHERE usersID='$usersID' AND MD5(fichiersID)='$idFichierMD5'";
	$req = mysqli_query($link,$SQL);
	if (mysqli_num_rows($req)!=0) {
		$enr=mysqli_fetch_assoc($req);
		$idFichier = $enr['fichiersID'];
		$nomFichier = $enr['fichiersNom'];
		$fichiersTelecharge = $enr['fichiersTelecharge'];
		$cheminFichier = "repertoires/".md5($usersID.$usersCie)."/".$nomFichier;
		$file_extension = strtolower(substr(strrchr($nomFichier,"."),1));
		
		switch ($file_extension) {
		   case "pdf": $ctype="application/pdf"; break;
		   case "exe": $ctype="application/octet-stream"; break;
		   case "zip": $ctype="application/zip"; break;
		   case "doc": $ctype="application/msword"; break;
		   case "xls": $ctype="application/vnd.ms-excel"; break;
		   case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		   case "gif": $ctype="image/gif"; break;
		   case "png": $ctype="image/png"; break;
		   case "jpe": case "jpeg":
		   case "jpg": $ctype="image/jpg"; break;
		   default: $ctype="application/force-download";
		}
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: $ctype");
		header("Content-Disposition: attachment; filename=\"".basename($nomFichier)."\";");
		header("Content-Transfer-Encoding: binary");
		set_time_limit(100);
		readfile($cheminFichier);
		
		$fichiersTelecharge=$fichiersTelecharge+1;
		$SQL = "UPDATE client_fichiers SET fichiersTelecharge='$fichiersTelecharge' WHERE usersID='$usersID' AND fichiersID='$idFichier'";
		mysqli_query($link,$SQL);
	} else {
		header("location: documentation.php");
		exit;	
	}

?>