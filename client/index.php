<?php
	include('includes/checklogin.php');
	$section="accueil";
	$lang=2;
	include('includes/global.inc.php');
	
	if (isset($_GET['erreur'])) {
		if ($_GET['erreur']==1) {
			if ($lang==1) {
				$txt_erreur="You must login to access the calculator.";
			} else {
				$txt_erreur="Vous devez vous connecter pour accéder &agrave; cette section.";
			}
		}elseif ($_GET['erreur']==2) {
			if ($lang==1) {
				$txt_erreur="Username or password incorrect.";
			} else {
				$txt_erreur="Utilisateur ou mot de passe incorrect.";
			}
		}elseif ($_GET['erreur']==3) {
			if ($lang==1) {
				$txt_erreur="The account does not exist.";
			} else {
				$txt_erreur="Compte inexistant.";
			}
		}elseif ($_GET['erreur']==4) {
			if ($lang==1) {
				$txt_erreur="Your account has not been activated.";
			} else {
				$txt_erreur="Votre compte n'est pas encore activ&eacute;.";
			}
		}
	}
	
	//Mot de passe oublié / Valider et envoyer le courriel
	if (isset($_POST['btn_email']) && !empty($_POST)) {
		$courriel = $_POST["courriel"];
		if(preg_match("/^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/", $courriel)!=0) {
			
			$sql = "SELECT userEmail FROM users WHERE userEmail='$courriel'";
			$result = mysqli_query($link,$sql);
			$total = mysqli_num_rows($result);
			
			if($total == 1) {
				require("includes/phpmailer.inc");
				require("includes/smtp.inc");
				
				$courriel1=md5($courriel);
				
				if ($lang==1) {
					$html = "<html><head><title></title></head><body>";
					$html.= "Hi,<br /><br />A password renewal application has been made for this email address. <br />";
					$html.= "If you did not initiate this request, ignore this notice. <br />";
					$html.= "If you receive this message repeatedly, please notify us. <br /><br />";
					$html.= "<a href='".$baseURL."?user=$courriel1&lang=en'>Click here to renew your password</a>";
					$html.= "</body></html>";
				} else {
					$html = '<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head><title></title></head><body>';
					$html.= "Bonjour,<br /><br />Une demande de renouvellement de mot de passe a été faite pour cette adresse courriel. <br />";
					$html.= "Si vous n'avez pas initié cette demande, ignorez cet avis. <br />";
					$html.= "Dans le cas où vous receviez ce courriel plusieurs fois, s'il vous plaît avisez nous.<br /><br />";
					$html.= "<a href='".$baseURL."?user=$courriel1&lang=fr'>Cliquez ici pour renouveler votre mot de passe</a>";
					$html.= "</body></html>";
				}
				$html=stripslashes($html);
				$mail = new PHPmailer();
				$mail->CharSet = "UTF-8";
				$mail->IsMail();
				$mail->IsHTML(true);
				$mail->Host='localhost';
				$mail->From="no-reply@alphacasting.com";
				$mail->FromName = "Alphacasting";
				$mail->AddAddress($courriel);
				$mail->WordWrap = 50;	// set word wrap
				$mail->IsHTML(true);	// send as HTML	
				if ($lang==1) {
					$mail->Subject="Request of renewal of password";
				} else {
					$mail->Subject="Demande de renouvellement de mot de passe";
				}
				$mail->Body = $html;
					
				if(!$mail->Send())
				{
					$mailer_error .= "Mailer Error: " . $mail->ErrorInfo;
				} else {
					Header("Location: index.php?sendok");	
				}
			} else {
				if ($lang==1) {
					$txt_erreur = "Your email was not found. Please contact us.";
				} else {
					$txt_erreur = "Votre courriel n'a pas &eacute;t&eacute; trouv&eacute;. Veuillez nous contacter.";
				}
			}
		}
	}
	
	//Reset Mot de passe
	if (isset($_POST['btn_reset']) && !empty($_POST)) {
		$courriel = $_POST["username"];
		
		$password = trim($_POST['password']);
		if($password== ''){
			if ($lang==1) {
				$erreurs['password']="Please enter your password.";
			} else {
				$erreurs['password']="Veuillez entrer votre mot de passe.";
			}
		}
		
		if (strlen($password)<4) {
			if ($lang==1) {
				$erreurs['passwordlen']="Your password must be at least 4 characters.";
			} else {
				$erreurs['passwordlen']="Votre mot de passe doit contenir au moins 4 caractères.";
			}
		}
		
		$confirmation = trim($_POST['confirmation']);
		if($confirmation== ''){
			if ($lang==1) {
				$erreurs['confirmation']="Please confirm your password.";
			} else {
				$erreurs['confirmation']="Veuillez confirmer votre mot de passe.";
			}
		}
		
		if ($password!=$confirmation) {
			if ($lang==1) {
				$erreurs['confirmation']="Please confirm your password.";
			} else {
				$erreurs['confirmation']="Veuillez confirmer votre mot de passe.";
			}
		}
		
		if(count($erreurs)==0){
			$password=md5($password);
			$SQL = "UPDATE users SET userPassword='$password' WHERE md5(userEmail)='$courriel'";
			mysqli_query($link,$SQL);
			Header("Location: index.php?ok");
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php
if ($lang==1) {
	echo 'Customer Access - Alphacasting';
} else {
	echo 'Zone Client - Alphacasting';
}
?>
</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/contentheight.js"></script>

</head>

<body>
<div class="container accueil">
	<?php include('includes/header.php'); ?>
    <div class="content main accueil">
    <img src="images/alphacasting.png" width="253" height="84" alt="Alphacasting" />
	<h1><?php if($lang==1) echo "Client Zone"; else echo "Zone Client"; ?></h1>
    <p>
    <?php
		if($lang==1) {
			if (isset($_GET['motdepasse'])) {
				echo 'Enter your email address registered in your account.';
			} elseif (isset($_GET['user'])) {
				echo 'Forgot Password';
			}else{
				echo "Enter your username and password <br />to access the client area safely."; 
			}
		} else {
			if (isset($_GET['motdepasse'])) {
				echo 'Entrez votre adresse courriel enregistré dans votre compte.';
			} elseif (isset($_GET['user'])) {
				echo 'Mot de passe oublié';
			}else{
				echo "Entrez vos nom d'usager et mot de passe <br />pour accéder à la zone client de façon sécurisée."; 
			}
		}
	?>
    </p>
    <div class="boitelogin">
    <?php
	if (isset($txt_erreur)) {
		echo '<p class="rouge">'.$txt_erreur.'</p>';
	}
	
	if (isset($_GET['sendok'])) {
		if ($lang==1) {
			echo '<p class="rouge">You will receive an email shortly.</p>';
		} else {
			echo '<p class="rouge">Vous receverez un courriel sous peu.</p>';
		}
	}
	
	if (isset($_GET['ok'])) {
		if ($lang==1) {
			echo '<p class="rouge">Your password has been changed.</p>';
		} else {
			echo '<p class="rouge">Votre mot de passe à été changé.</p>';
		}
	}
	
	if (isset($erreurs['password'])) {
		echo '<p class="rouge">'.$erreurs['password'].'</p>';
	}
	if (isset($erreurs['passwordlen'])) {
		echo '<p class="rouge">'.$erreurs['passwordlen'].'</p>';
	}
	if (isset($erreurs['confirmation'])) {
		echo '<p class="rouge">'.$erreurs['confirmation'].'</p>';
	}
	?>
    <form name="frm" method="post" action="">
    <?php
		if (isset($_GET['motdepasse'])) {
	?>
    <div><input type="text" name="courriel" id="courriel" placeholder="<?php if($lang==1) echo "Email"; else echo "Courriel"; ?>" /></div>
    <div><input type="submit" value="<?php if ($lang==1) { echo "Send"; } else { echo "Envoyer"; } ?>" name="btn_email" id="btn_email"/></div>
    <?php
	 	} elseif (isset($_GET['user'])) {
	?>
    <div><input type="password" name="password" id="password" placeholder="<?php if($lang==1) echo "New Password"; else echo "Nouveau mot de passe"; ?>" /></div>
    <div><input type="password" name="confirmation" id="confirmation" placeholder="<?php if($lang==1) echo "Confirm Password"; else echo "Confirmation mot de passe"; ?>" /></div>
    <div><input type="submit" value="<?php if ($lang==1) { echo "Send"; } else { echo "Envoyer"; } ?>" name="btn_reset" id="btn_reset"/></div>
	<input type="hidden" name="username" id="username" value="<?php echo $_GET['user']; ?>" />
    <?php
		} else {
	?>
    <div><input type="text" name="courriel" id="courriel" placeholder="<?php if($lang==1) echo "Email"; else echo "Courriel"; ?>" /></div>
    <div><input type="password" name="password" id="password" placeholder="<?php if($lang==1) echo "Password"; else echo "Mot de passe"; ?>" /></div>
    <div><input type="submit" value="<?php if ($lang==1) { echo "Sign In"; } else { echo "Connexion"; } ?>" name="btnLogin" id="btnLogin"/></div>
    <?php
		}
	?>
    </form>
    <div class="linkmotdepasseoublie">
    <?php
	if (!isset($_GET['motdepasse']) && !isset($_GET['user'])) {
		if ($lang==1) {
			echo '<a href="?motdepasse">I forgot my password</a>';
		} else {
			echo '<a href="?motdepasse">J\'ai oublié mon mot de passe</a>';
		}
	}
    ?>
    </div>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    </div>
    </div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>