<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<div class="markets-home">
		<?php include('../includes/top-title.php'); ?>
		<div class="row py-5">
			<div class="col-xl-10 offset-xl-1 details">
				<h2><strong>Overview</strong></h2>
				<h3 class="mb-3 mt-4">Investment casting process</h3>
				<p>Investment casting, also known as lost wax casting, is a manufacturing method in which a wax pattern is created and then coated with a ceramic slurry to make a mold. The wax is then melted out of the ceramic mold and molten metal is poured into the cavity. The molten metal solidifies, and the ceramic shell is then broken or blasted off, generating a metal casting. The investment casting process does not require a minimum order quantity and can cast parts ranging from a few ounces to upwards of 200 lbs, so it can be a manufacturing solution no matter what size project you are working on.</p>
				<h3 class="mb-3 mt-5">Benefits of Investment Casting</h3>
				<p>Because of the advantages of investment casting, you can create almost any configuration of your precision metal component. You could design any parts as complex, small, or large as you need.</p>
				<p>Eliminate all the barriers holding you back!</p>
				<p>You will reduce secondary operations and tooling costs over multiple production runs. This process has the advantage to give the widest choice of alloys and produces less waste. You will maintain outstanding repeatability and incredibly tight tolerances. Stronger parts will be conceived and thinner walls for a small weight will be incorporated. You will be able to draft highly detailed surfaces and stamps.</p>
				<h3 class="mb-3 mt-5">Some figures</h3>
				<ul class="pl-5">
					<li>Production capacity: 250 clusters per day (parts of a maximum of 500 cubic millimetres). ( 0,03 cubic inches)</li>
					<li>Melting capacity: 6 T per day.</li>
					<li>Surface area: 74,000 ft2 ( 6875 m²) of factories, offices and warehouses.</li>
					<li>Personnel: 160 experienced employees.</li>
				</ul>
			</div>
		</div>
		<div class="row plant d-flex align-items-center justify-content-center">
			<div class="col-12 text-center">
				<div class="py-5">
					<h2 class="pt-4 mb-0">See our plant departments</h2>
					<span class="material-icons-outlined">expand_more</span>
				</div>
			</div>
		</div>
		<?php include('../includes/casting_items.php'); ?>
	</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>