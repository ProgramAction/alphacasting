<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$s1_intro_pos = 'down';
	$s2_intro_pos = 'down';
	$s3_intro_pos = 'down';
	$s1=array(
		1=>Lang::insert('heattreatment-s1-txt-p1'),
		2=>Lang::insert('heattreatment-s1-txt-p2'),
		3=>Lang::insert('heattreatment-s1-txt-p3'),
		4=>Lang::insert('heattreatment-s1-txt-p4'),
	);
	$s3=array(
		1=>Lang::insert('heattreatment-s3-txt-p1'),
		2=>Lang::insert('heattreatment-s3-txt-p2'),
		3=>Lang::insert('heattreatment-s3-txt-p3'),
	);
	$features=array(
		1=>array("titre"=>Lang::insert('heattreatment-features-h3-1'), "p"=>Lang::insert('heattreatment-features-p-1'), "details"=>Lang::insert('heattreatment-features-details-1')),
		2=>array("titre"=>Lang::insert('heattreatment-features-h3-2'), "p"=>Lang::insert('heattreatment-features-p-2'), "details"=>Lang::insert('heattreatment-features-details-2')),
	);
	
	include('../includes/casting-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>