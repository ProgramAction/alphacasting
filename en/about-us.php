<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1">
			<h2>Worldwide leader in precision</h2>
			<h3 class="mb-3 mt-4">investment casting</h3>
			<p>Founded in 1991, Alphacasting became rapidly a worldwide leader in precision investment casting, pouring more than 120 different types of alloys.</p>
			<p>Alphacasting specializes in the production of innovative and modern castings for the high-tech industry. We adhere to strict industry guidelines and respond to customers requests with efficiency and punctuality.</p>
			<p>Quality and on-time delivery are the reasons why Alphacasting continues to be on the leading edge of the casting industry.</p>
		</div>
	</div>
	<div class="row pt-5 s2">
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-black">
			<div class="px-lg-5 py-5 max-w600">
				<h2 data-aos="zoom-out" class="text-uppercase"><strong>Experience</strong></h2>
				<p class="intro" data-aos="zoom-in" data-aos-delay="25">More than 30 years’ experience in investment casting, casting air melt, and vacuum cast alloys.</p>
			</div>
		</div>
		<div class="col-lg-6 px-0">
			<img src="../assets/images/alphacasting-icon-metal.jpg" alt="" role="presentation" class="img-fluid objfitcover">
		</div>
	</div>
	<div class="row s2">
		<div class="col-lg-6 px-0 order-2 order-lg-1">
			<img src="../assets/images/ferrous-alloys.jpg" alt="Worker casting alloy" class="img-fluid objfitcover">
		</div>
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-rouge order-1 order-lg-2">
			<div class="px-lg-5 py-5 max-w600" data-aos="zoom-in">
				<p data-aos="zoom-in"><strong class="text-uppercase">Product range</strong>: Nearly 2,000 different parts made to date.</p>
				<p data-aos="zoom-in"><strong class="text-uppercase">Clientele</strong>: 400 customers, mainly in western Europe, the USA, China and Australia.</p>
				<p data-aos="zoom-in"><strong class="text-uppercase">Exports</strong>: 80%, most of them to the European Community.</p>
			</div>
		</div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1 py-5">
			<h2 class="text-uppercase">Alphacasting in brief</h2>
			<h3 class="mb-3 mt-4">Located in Montreal Canada  <span class="sep"></span>  Over 74,000 ft2 (6875 m²) of office and manufacturing space with 160 employees</h3>
			<p>Specialist in casting by the lost wax technique, Alphacasting produces parts in large, medium and small series in all grades of steel, such as stainless and most alloys of Cobalt Chrome, Ni-Resist and Titanium.</p>
			<p>We invested in modern and sophisticated equipment to be able to fulfill all offers from its customers in terms of reliability, reproducibility, and flexibility.</p>
			<p>Alphacasting has the quality-assurance certifications: ISO 9001:2015 and several specific qualifications to the industrial, aerospace and defence sectors.</p>
			<p>These certifications have each time been renewed and improved as the standards concerned evolve.</p>
			<p><a href="<?php echo $pages['contact']['url']; ?>" class="btn btn-black">Please contact us with any questions you may have</a></p>
		</div>
	</div>
	<div class="row py-5 border-top rouge">
		<div class="col-12 text-center slogan">
			This is where we are heading, here’s our future building...
		</div>
	</div>
	<div class="row">
		<div class="col-12 px-0">
			<img src="../assets/images/alphacasting-plant-3d.jpg" alt="Alphacasting future building" class="img-fluid">
		</div>
	</div>
	<div class="row pt-5 border-bottom rouge clients">
		<div class="container pt-4">
			<div class="row align-items-center justify-content-center flex-wrap">
				<div class="col text-center py-2"><img src="../assets/images/logos/safran.png" alt="Safran" width="237" height="80" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/boeing.png" alt="Boeing" width="214" height="63" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/bombardier.png" alt="Bombardier" width="208" height="63" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/honeywell.png" alt="Honeywell" width="236" height="75" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/thales.png" alt="Thales" width="200" height="54" class="img-fluid"></div>
			</div>
		</div>
		<div class="col-12 pt-5 text-center">
			<a href="../assets/docs/logo-clients.pdf" class="btn noir" target="_blank">See more happy clients</a>
		</div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1 py-5">
			<p class="intro mb-0">The alphacasting’s</p>
			<h2 class="text-uppercase">Inner strength</h2>
			<p>Each member of our teams contributes to different skills to our company, providing the highest quality customer service throughout the casting procedure.</p>
			<p>Our directors and managers are very experimented in foundry. They also have extensive knowledge of the metals industry, the engineering and construction industries. Our avant-garde technical and traditional skills give us the capability to take on any casting requirement.</p>
			<p>Accordingly, we are very well-placed to offer consultation as follow:</p>
			<ul class="pl-5">
				<li class="p">THE ADVICE OF EXPERTS</li>
				<li class="p">SIMULATION OF FILLING THE MOULD AND THE SOLIDIFICATION OF THE METAL</li>
				<li class="p">COMPUTER-ASSISTED DESIGN/DRAWING</li>
				<li class="p">RAPID PROTOTYPING</li>
				<li class="p">METALLURGICAL ADVICE</li>
			</ul>
		</div>
	</div>
<?php include('../includes/cta_template.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>