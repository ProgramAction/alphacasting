<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
    <script src="<?php echo $assetsPath; ?>js/smooth-scrollbar.js"></script>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
    <div class="row video">
        <div class="col-12 px-0">
            <video autoplay loop muted poster="<?php echo $assetsPath; ?>images/videos/accueil.jpg">
                <source src="<?php echo $assetsPath; ?>videos/alphacasting-accueil.mp4?v=1" type="video/mp4">
                <source src="<?php echo $assetsPath; ?>videos/alphacasting-accueil.webm?v=1" type="video/webm">
                <?php Lang::write('cta-novideo'); ?>
            </video>
        </div>
    </div>
    <div class="row shortcuts">
        <div class="col-md-4 pt-5">
            <div>
                <h2>About</h2>
                <p>Alphacasting <br>Worldwide leader in precision <br><span class="text-uppercase">investment casting</span>...</p>
                <a href="<?php echo $pages['about']['url']; ?>" class="btn btn-black">Learn more</a>
            </div>
        </div>
        <div class="col-md-4 pt-5">
            <div>
                <h2>News</h2>
                <p>Aluminum Casting for Ford GT. We are proud to be a key supplier to Multimatic, manufacturer of the Ford GT.</p>
                <a href="<?php echo $pages['news']['url']; ?>" target="_blank" class="btn btn-black">See all news</a>
            </div>
        </div>
        <div class="col-md-4 pt-5">
            <div>
                <h2>Certification</h2>
                <p>Our quality program which meets the most stringent specifications, clearly superior to established standards in many industries...</p>
                <a href="<?php echo $pages['certifications']['url']; ?>" class="btn btn-black">Learn more</a>
            </div>
        </div>
    </div>
    <div class="row markets position-relative">
        <div class="col-lg-6 d-flex align-items-center justify-content-lg-center py-5">
            <div>
                <p class="titre_markets">Markets</p>
                <h2>Precision <br class="d-none d-lg-inline">markets <br class="d-none d-lg-inline">with precise <br class="d-none d-lg-inline">needs</h2>
            </div>
        </div>
    </div>
    <div class="hor position-relative">
	<?php include('../includes/markets_items.php'); ?>
    </div>
<?php include('../includes/footer.php'); ?>
<script>
    var horOffsetTop;
    var mLeft;
    var containerW;
    var footerOffsetTop;
    
	var elem = document.querySelector("#home");
	var scrollbar = window.Scrollbar;
	var scroll;
	var scrollPos;
	
	scrollbar = Scrollbar.init(elem, {
		alwaysShowTracks:true
	});

	scrollbar.addListener(function (status) {
		scroll = status.offset.y;
		if (scroll>10) {
			$("header").addClass("bgblack");
		} else {
			$("header").removeClass("bgblack");
		}

		var scrollStop = parseInt(horOffsetTop)+parseInt($('.hor').height())-parseInt($('.markets').height());
		if (scroll < scrollStop) {
			if ( scroll >= horOffsetTop && ((horOffsetTop - scroll) >= ($(window).width() - containerW)) ) {
				scrollPos = scroll - (horOffsetTop);
				$('.hor, .markets').css( 'transform', 'translateY('+scrollPos+'px)' );
				$('.markets_items_container').css('transform', 'translateX('+(scrollPos*-1)+'px)');
			}
		} else {
			var topPos = scrollStop-parseInt(horOffsetTop);
			$('.hor, .markets').css( 'transform', 'translateY('+topPos+'px)' );
			$('.markets_items_container').css('transform', 'translateX('+(topPos*-1)+'px)');
		}
	});

    function markets() {
        var headerHeight = $('header').height();
        var viewHeight = $(window).height() - headerHeight;
        var mLeft = parseInt( $('.markets_items_container').css('marginLeft') );
        containerW = $(".markets_items_container").width()+mLeft;
        $('.hor').height( $(".markets_items_container").width()+viewHeight-mLeft-8 );
        $('.markets, .markets_items, .markets_items_container').height(viewHeight);
        $('.markets_items').css('margin-top',viewHeight*-1);
		
		///*******///
		horOffsetTop = ($('.video').height()+$('.shortcuts').outerHeight()) - $('header').height();
		///*******///
		
		scrollbar.update();
    }
    
    $(document).ready(function() {
        markets();
    });
	
	var resizeTimer;
    $(window).on('resize', function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			markets()
		}, 1000);
    });

</script>
</body>
</html>