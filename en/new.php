<?php
	$lang="EN";
    setlocale(LC_TIME, $lang == 'EN'?'en_CA':'fr_CA');
	include('../includes/global.inc.php');

    if(isset($_GET['slug'])){
        $new = \Programaction\News::get_by_slug($_GET['slug']);
        if($new['titre'] == null){
            header('Location:/'.strtolower($lang).'/'.Lang::insert('news-url'));
        }
    }else{
        header('Location:/'.strtolower($lang).'/'.Lang::insert('news-url'));
    }

    
    
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<base href="<?php echo '../'.basename(__DIR__) ?>">
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
        <div class="row singlenew mb-5">
            <div class="col-12">
                <div class="row py-5">
                    <div class="col-xl-10 offset-xl-1 pb-lg-5">
                        <p class="date mb-1"><?php echo date("F Y",strtotime($new['date'])); ?></p>
                        <h2 class="mb-5"><?php echo $new['titre'] ?></h2>
                        <?php echo $new['texte']  ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-10 offset-xl-1 pb-5 text-center text-lg-left">
                <a class="btn btn-black" href="/<?php echo strtolower($lang).'/'.Lang::insert('news-url') ?>" class="btn btn-black"><?php Lang::write('news-back') ?></a>
            </div>
        </div>
	<?php include('../includes/cta_template.php'); ?>
	<?php include('../includes/swiper_casting.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>