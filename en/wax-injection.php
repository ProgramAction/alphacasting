<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$s1_intro_pos = 'up';
	$s2_intro_pos = 'up';
	$s3_intro_pos = 'up';
	$s1=array(
		1=>Lang::insert('waxinjection-s1-txt-p1'),
		2=>Lang::insert('waxinjection-s1-txt-p2'),
	);
	$s3=array(
		1=>Lang::insert('waxinjection-s3-txt-p1'),
		2=>Lang::insert('waxinjection-s3-txt-p2'),
		3=>Lang::insert('waxinjection-s3-txt-p3'),
	);
	$features=array(
		1=>array("titre"=>Lang::insert('waxinjection-features-h3-1'), "p"=>Lang::insert('waxinjection-features-p-1'), "details"=>Lang::insert('waxinjection-features-details-1')),
		2=>array("titre"=>Lang::insert('waxinjection-features-h3-2'), "p"=>Lang::insert('waxinjection-features-p-2'), "details"=>Lang::insert('waxinjection-features-details-2')),
	);
	
	include('../includes/casting-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>