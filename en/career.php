<?php
	$lang="EN";
	include('../includes/global.inc.php');
	include('../includes/connexion.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php include('../includes/top-title.php'); ?>

<div class="row">
	<div class="col-12 offset-0 col-xl-10 offset-xl-1">
		<div class="row table-heading">
			<div class="col d-flex flex-column flex-md-row justify-content-end align-items-center flex-wrap">
				<p class="legende mr-0 mr-md-4 mb-3 mb-md-0">Open positions</p>
				<a class="cv" href="https://app.mynjobs.com/e/alphacasting/fr/eyJkIjp0cnVlfQ__" target="_blank">Send us your resume</a>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Department</th>
						<th>#Ext</th>
						<th>Job title</th>
						<th>Day</th>
						<th>Evening</th>
						<th>Night</th>
					</tr>
				</thead>
				<tbody>
					<?php

					$SQL = "SELECT * FROM carriere_departement ORDER BY numPoste";
					if ($req=mysqli_query($conn,$SQL)) {
						if (mysqli_num_rows($req)!=0) {
							// Container pour chaque departement
							while ($enr = $req->fetch_assoc()) {
								$idDepartement = $enr['departementID'];
								$nomDepartement = $enr['nomEN'];
								$SQL_poste = "SELECT * FROM carriere_poste WHERE departementID='$idDepartement' ORDER BY posteID ASC";
									
								if ($req_poste=mysqli_query($conn,$SQL_poste)) {
									if (mysqli_num_rows($req)!=0) {
										$i=1;
										while ($enr_poste=$req_poste->fetch_assoc()) {
											$noPoste = $enr_poste['posteID'];
											$titrePoste = $enr_poste['nomEN'];
											$jour = $enr_poste['jour'];
											$soir = $enr_poste['soir'];
											$nuit = $enr_poste['nuit'];
											
											if ($jour!=0 || $soir!=0 || $nuit!=0) {
												$posteDisponible=1;
											} else {
												$posteDisponible=0;
											}

											echo '<tr '.((mysqli_num_rows($req_poste)==$i)?'class="fin-departement"':'').'>';
											if ($i==1) echo '<td class="departement" rowspan="'.mysqli_num_rows($req_poste).'">'.$nomDepartement.'</td>';
											echo '<td '.(($posteDisponible==1)?'class="disponible"':'').'>'.$noPoste.'</td>';
											echo '<td class="titreposte'.(($posteDisponible==1)?' disponible':'').'">'.(($posteDisponible==1)?'<a href="https://app.mynjobs.com/e/alphacasting/en/eyJkIjp0cnVlfQ__" target="_blank">'.$titrePoste.'</a>':$titrePoste).'</td>';
											echo '<td>'.(($jour!=0)?$jour:'').'</td>';
											echo '<td>'.(($soir!=0)?$soir:'').'</td>';
											echo '<td>'.(($nuit!=0)?$nuit:'').'</td>';
											echo '</tr>';
											$i++;
										}
									}
								}
								
							}
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
	
<?php include('../includes/footer.php'); ?>
</body>
</html>