<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"/>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1">
			<h2 class="mb-4">Quality Program</h2>
			<p>Our goal is to share a common mission and objectives on all levels of our company. Due to its desire and commitment to support the advancement of its clients, Alphacasting has equipped itself with the necessary tools.</p>
			<p>Consequently, we take pride in our quality program which meets the most stringent specifications, clearly superior to established standards in many industries.</p>
			<p>We are recognized by Canadian and U.S. government agencies that oversee all technical exchange data and production of goods related to military manufacturers.</p>
			<ul class="list-unstyled mt-5 d-flex flex-wrap flex-md-nowrap pb-1">
				<li class="pr-5 pb-4"><img src="../assets/images/logo-accredited-nadcap.png" width="223" height="82" alt="Logo Accredited Nadcap"></li>
				<li class="pb-4"><img src="../assets/images/logo-intertek.png" width="75" height="90" alt="Logo Intertek"></li>
			</ul>
		</div>
	</div>
	<div class="row py-5 certificats">
		<div class="col-xl-10 offset-xl-1">
			<div class="row pt-5">
				<div class="col-md-6 col-lg-4">
					<ul>
						<li class="p"><a href="../assets/images/certifications/Nadcap-Non-Destructive.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Non-Destructive Testing</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-Welding-2021.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Welding</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-HeatTreating.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Heat Treating</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-HeatTreating.jpg" data-fancybox="certifications"><strong>Intertek</strong> AS/EN/JISQ9100:2009 <br>ISO 9001-2008</a></li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-4">
					<ul>
						<li class="p"><a href="../assets/images/certifications/CPG.jpg" data-fancybox="certifications"><strong>CPG</strong> Control Goods Program</a></li>
						<li class="p"><a href="../assets/images/certifications/JCP.jpg" data-fancybox="certifications"><strong>JCP Certification</strong> Technical Data Agreement</a></li>
						<li class="p"><a href="../assets/images/certifications/PEP-PIP.png" data-fancybox="certifications"><strong>PEP - PIP</strong> Partners in protection</a></li>
					</ul>
				</div>
				<div class="col-lg-4 pt-5 pt-lg-0">
					<img src="../assets/images/badge-alphacasting-quality.png" width="510" height="510" alt="" role="presentation" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
<?php include('../includes/cta_template.php'); ?>
<?php include('../includes/swiper_casting.php'); ?>
<?php include('../includes/footer.php'); ?>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
</body>
</html>