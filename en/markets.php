<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<div class="markets-home">
		<?php include('../includes/top-title.php'); ?>
		<div class="row py-5">
			<div class="col-xl-10 offset-xl-1">
				<p>We serve several markets with equal intensity, and we know how to meet the challenges.</p>
				<p>The expertise we have acquired in the aviation and medical fields requires not only the competence of our teams, but also the ability to maintain rigor at every stage of production until delivery.</p>
				<p>We have certifications and master the metal alloys for the following industries:</p>
			</div>
		</div>
		<?php include('../includes/markets_items.php'); ?>
	</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>