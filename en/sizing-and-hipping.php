<?php
	$lang="EN";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$s1_intro_pos = 'up';
	$s2_intro_pos = 'up';
	$s3_intro_pos = 'up';
	$s1=array(
		1=>Lang::insert('sizinghipping-s1-txt-p1'),
		2=>Lang::insert('sizinghipping-s1-txt-p2'),
		3=>Lang::insert('sizinghipping-s1-txt-p3'),
		4=>Lang::insert('sizinghipping-s1-txt-p4'),
	);
	$s3=array(
		1=>Lang::insert('sizinghipping-s3-txt-p1'),
		2=>Lang::insert('sizinghipping-s3-txt-p2'),
		3=>Lang::insert('sizinghipping-s3-txt-p3'),
		4=>Lang::insert('sizinghipping-s3-txt-p4'),
		5=>Lang::insert('sizinghipping-s3-txt-p5'),
	);
	$features=array(
		1=>array("titre"=>Lang::insert('sizinghipping-features-h3-1'), "p"=>Lang::insert('sizinghipping-features-p-1'), "details"=>Lang::insert('sizinghipping-features-details-1')),
		2=>array("titre"=>Lang::insert('sizinghipping-features-h3-2'), "p"=>Lang::insert('sizinghipping-features-p-2'), "details"=>Lang::insert('sizinghipping-features-details-2')),
	);
	
	include('../includes/casting-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>