<?php
	include('includes/checklogin.php');
	include('includes/uploadImg.php');
	$section="nouvelles";

	
	$image = $date = $titre = $description = $intro = $lang = "";
	$erreurs = array();

	function slugify($link,$text, $divider = '-'){
		$slug = $text;
		$slug = preg_replace('~[^\pL\d]+~u', $divider, $slug);
		$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
		$slug = preg_replace('~[^-\w]+~', '', $slug);
		$slug = trim($slug, $divider);
		$slug = preg_replace('~-+~', $divider, $slug);
		$slug = strtolower($slug);
		if (empty($text)) {$slug = 'n-a';}
		$modifier = "";
		$iterator = 0;
		while(slug_exists($link,$slug.$modifier)){
			$iterator++;
			$modifier = "-".$iterator;
		}
		return $slug.$modifier;
	}

	function slug_exists($link,$slug){
		$SQL = "SELECT id FROM nouvelles WHERE slug='$slug'";
		$req = mysqli_query($link,$SQL);
		if (mysqli_num_rows($req)!=0) {
			return true;
		}else{
			return false;
		}
	}

	function title_updated($link,$id,$title){
		$SQL = "SELECT titre FROM nouvelles WHERE id='$id'";
		$req = mysqli_query($link,$SQL);
		if (mysqli_num_rows($req)!=0) {
			// Ligne ajoutee pour que $enr existe
			$enr = mysqli_fetch_assoc($req);
			$titre = $enr['titre'];
			return $titre != $title;
		}else{
			return false;
		}
	}

	
	if (isset($_POST['confirm_delete'])) {
		$id = $_POST['sup_id'];
		$sup_item = $_POST['sup_item'];
		if (is_numeric($id)) {
			$SQL = "SELECT * FROM nouvelles WHERE id='$id'";
			$req = mysqli_query($link,$SQL);
			if (mysqli_num_rows($req)!=0) {
				$enr=mysqli_fetch_assoc($req);
				unlink("../images/".$section."/".$enr['image']);
			}
			
			if ($sup_item=="fichier") {
				//$SQL = "UPDATE ".$nomTable." SET fichier='' WHERE id='$id'";
			} elseif ($sup_item=="image") {
				$SQL = "UPDATE nouvelles SET image='' WHERE id='$id'";
			}
			
			mysqli_query($link,$SQL);
			
			header("Location: ".$section."-edit.php?id=".$id);
			exit;
		}
	}
	
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		if (is_numeric($id)) {
			$SQL = "SELECT * FROM nouvelles WHERE id='$id'";
			$req = mysqli_query($link,$SQL);
			if (mysqli_num_rows($req)!=0) {
				$enr=mysqli_fetch_assoc($req);
				$image = $enr['image'];
				$date = $enr['date'];
				$titre = $enr['titre'];
				$intro = $enr['intro'];
				$description = $enr['texte'];
				$lang = $enr['lang'];
			} else {
				header("Location: ".$section.".php");
				exit;
			}
		} else {
			header("Location: ".$section.".php");
			exit;
		}
	}
	
	if(isset($_POST['ajouter'])){
		$date = $_POST['date'];
		$titre = addslashes($_POST['titre']);
		if ($titre=="") {
			$erreurs['titre']=1;	
		}
		$description = $_POST['description'];
		if ($description=="") {
			$erreurs['description']=1;	
		}
		$intro = $_POST['intro'];
		if ($intro=="") {
			$erreurs['intro']=1;	
		}
		$lang = $_POST['lang'];
		if ($lang=="") {
			$erreurs['lang']=1;	
		}
		//validation de l'image
		$newImg = false;
		if(is_uploaded_file($_FILES['image']['tmp_name'])){
			$newImg = true;
			
			$content_dir_img = '../assets/images/'.$section; // dossier où sera déplacé le fichier
			$tmp_file = $_FILES['image']['tmp_name'];
			$mime_type = $_FILES['image']['type'];
		
			// on vérifie maintenant l'extension
			$type_file = strrchr($_FILES['image']['name'], '.');
			$extensions = array('.png', '.jpeg', '.jpg', '.gif', '.PNG', '.JPEG', '.JPG', '.GIF');
			
			if(!in_array($type_file, $extensions)) //Si l'extension n'est pas dans le tableau
			{
				$erreurs['image'] = "Format de fichier non valide.";
			}
				
			switch ($mime_type) {
				case 'image/png':
					$extension = 'png';
					break;
				case 'image/gif':
					$extension = 'gif';
					break;
				case 'image/jpeg':
					default: //
					$extension = 'jpg';
					break;
			}
		}
		
		if(count($erreurs)==0) {
			if ($newImg) {
				$idPhoto = uniqid('',true);
				$image = $idPhoto.'.'.$extension;
				$chemin_big = "$content_dir_img/$image";
				if( !move_uploaded_file($tmp_file, $chemin_big)) {
					$erreurs['image'] = "Problème de transfert d'image";
				}
			}
			
			if(count($erreurs)==0) {
				//$fichierResized="";
				if ($newImg) {
					//$fichierResized = $idPhoto.".png";
					//apercu($chemin_big, "$content_dir_img/".$fichierResized, 155, 255);
					redimension($chemin_big,210);
				}
				$slug = slugify($link,$titre);
				$SQL = "INSERT INTO nouvelles (date, titre,slug, texte, intro, image, lang) VALUES ('$date', '$titre', '$slug', '$description', '$intro', '$image', '$lang')";
				mysqli_query($link,$SQL);
				header("Location: ".$section.".php");
				exit;
			}
		}
	}
	
	if(isset($_POST['modifier'])){
		$date = $_POST['date'];
		$titre = addslashes($_POST['titre']);
		if ($titre=="") {
			$erreurs['titre']=1;	
		}
		$description = $_POST['description'];
		if ($description=="") {
			$erreurs['description']=1;	
		}
		$intro = $_POST['intro'];
		if ($intro=="") {
			$erreurs['intro']=1;	
		}
		$lang = $_POST['lang'];
		if ($lang=="") {
			$erreurs['lang']=1;	
		}
		
		//validation de l'image
		$newImg = false;
		if(is_uploaded_file($_FILES['image']['tmp_name'])){
			$newImg = true;
			
			$content_dir_img = '../assets/images/'.$section; // dossier où sera déplacé le fichier
			$tmp_file = $_FILES['image']['tmp_name'];
			$mime_type = $_FILES['image']['type'];
		
			// on vérifie maintenant l'extension
			$type_file = strrchr($_FILES['image']['name'], '.');
			$extensions = array('.png', '.jpeg', '.jpg', '.gif', '.PNG', '.JPEG', '.JPG', '.GIF');
			
			if(!in_array($type_file, $extensions)) //Si l'extension n'est pas dans le tableau
			{
				$erreurs['image'] = "Format de fichier non valide.";
			}
			
			// on copie le fichier dans le dossier de destination
			$name_file = $_FILES['image']['name'];
				
			switch ($mime_type) {
				case 'image/png':
					$extension = 'png';
					break;
				case 'image/gif':
					$extension = 'gif';
					break;
				case 'image/jpeg':
					default: //
					$extension = 'jpg';
					break;
			}
		}
		
		if(count($erreurs)==0) {
			if ($newImg) {
				$idPhoto = uniqid('',true);
				$image =$idPhoto.'.'.$extension;
				$chemin_big = "$content_dir_img/$image";
				if( !move_uploaded_file($tmp_file, $chemin_big)) {
					$erreurs['image'] = "Problème de transfert d'image";
				}
			}
			
			if(count($erreurs)==0) {
				if ($newImg) {
					redimension($chemin_big,210);
					$SQL = "UPDATE nouvelles SET image='$image' WHERE id='$id'";
					mysqli_query($link,$SQL);
				}
				if(title_updated($link,$id,$titre)){
					$slug = slugify($link,$titre);
					$SQL = "UPDATE nouvelles SET date='$date', titre='$titre', intro='$intro', texte='$description', lang='$lang', slug='$slug' WHERE id='$id'";
				}else{
					$SQL = "UPDATE nouvelles SET date='$date', titre='$titre', intro='$intro', texte='$description', lang='$lang' WHERE id='$id'";
				}
				mysqli_query($link,$SQL);
				header("Location: ".$section.".php");
				exit;
			}
		}
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nouvelles - Zone Client - Alphacasting</title>
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/style.css"/>

<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.supprimer.img').click(function(e) {
			e.preventDefault();
			idItem = $(this).attr("idItem");
			
			$.ajax({
				type:"POST",
				url: "includes/ajax_delete.php",
				data:{idItem:idItem, nomItem:'image'},
				cache: false
			})
			.done(function(html) {
				$( ".confirm.img").html(html);
			});
		});
	});
</script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
<div class="content main">
    <div class="sousmenu">
    	<a href="<?php echo $section; ?>.php">Retour</a>
    </div>
    <form action="" method="post" enctype="multipart/form-data" name="form_modif" class="form_modif">
    <p>
    <?php
        if ($image!="") {
            echo '<img src="../assets/images/nouvelles/'.$image.'" alt="'.$titre.'" />';	
            echo '<div class="edit">';
            echo '<a href="#" class="supprimer img" idItem='.$id.'>Supprimer</a>';
            echo '</div>';
            echo '<div class="confirm img"></div>';
        }
    ?>
    <p>
    <label for="image">Image (Format : jpg, png, gif) : </label><br />
    <input name="image" type="file" id="image" <?php if (isset($erreurs['image'])) echo 'class="erreur"'; ?> />
    </p>
    <p>
    <label for="date">Date : </label><br />
    <input name="date" type="text" id="date" value="<?php if ($date== "") { echo date("Y-m-d", time()); } elseif ($date>0) { echo $date; } ?>" />
    <a href="javascript:;" onClick="top.newWin = window.open('includes/calendrier.php?target=form_modif.date', 'cal', 'dependent=yes, width=240, height=160, screenX=200, screenY=300, titlebar=no')">
    <img src="images/ico_calendrier.gif" width="18" height="18" border="0" class="calendar"></a>
    </p>
    <p>
    <label for="titre">*Titre : </label><br />
    <input name="titre" type="text" id="titre" class="titre <?php if (isset($erreurs['titre'])) echo "erreur"; ?>" value="<?php echo stripslashes($titre); ?>" />
    </p>
    <p>
    <label for="description">*Texte : </label><br />
    <textarea name="description" cols="50" rows="7" id="description" class="ckeditor <?php if (isset($erreurs['description'])) echo "erreur"; ?>"><?php echo stripslashes($description); ?></textarea>
    </p>
	<p>
    <label for="Introduction">*Intro : </label><br />
    <textarea name="intro" cols="50" rows="7" id="Introduction" class="ckeditor <?php if (isset($erreurs['intro'])) echo "erreur"; ?>"><?php echo stripslashes($intro); ?></textarea>
    </p>
    <p>
    <label for="lang">Langue : </label><br />
    <select name="lang" id="lang" <?php if (isset($erreurs['lang'])) echo 'class="erreur"'; ?>>
    <option value="">Choisir...</option>
    <option value="1" <?php if ($lang==1) echo 'selected="selected"'; ?>>Anglais</option>
    <option value="2" <?php if ($lang==2) echo 'selected="selected"'; ?>>Français</option>
    </select>
    </p>
    <p>
    <input type="submit" name="modif" value="Envoyer" class="btsubmit"/>
    <?php
    if(isset($id)){
        echo '<input name="modifier" type="hidden"/>';
    } else {
        echo '<input name="ajouter" type="hidden" id="ajouter" />';
    }
    ?>
    </p>
    </form>
</div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>