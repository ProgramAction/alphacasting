<?php
	include('includes/checklogin.php');
	$section="nouvelles";
	
	$nomTable='nouvelles';
	if (isset($_POST['confirm_delete'])) {
		$id = $_POST['sup_id'];
		if (is_numeric($id)) {
			$SQL = "SELECT * FROM ".$nomTable." WHERE id='$id'";
			$req = mysqli_query($link,$SQL);
			if (mysqli_num_rows($req)!=0) {
				$enr=mysqli_fetch_assoc($req);
				unlink("../assets/images/nouvelles/".$enr['image']);	
			}
			
			$SQL = "DELETE FROM ".$nomTable." WHERE id='$id'";
			mysqli_query($link,$SQL);
			
			header("Location: ".$section.".php");
			exit;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nouvelles - Zone Client - Alphacasting</title>
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/style.css"/>

<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.supprimer').click(function(e) {
			e.preventDefault();
			
			idItem = $(this).attr("idItem");
			
			$.ajax({
				type:"POST",
				url: "includes/ajax_delete.php",
				data:{idItem:idItem, nomItem:'nouvelle'},
				cache: false
			})
			.done(function(html) {
				$( "#confirm_"+idItem).html(html);
			});
		});
	});
</script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
<div class="content nouvelles main">
	<div class="sousmenu">
    	<a href="<?php echo $section.'-edit.php'; ?>">Ajouter une nouvelle</a>
    </div>
<?php
	$SQL = "SELECT * FROM nouvelles ORDER BY date DESC";
	$req = mysqli_query($link,$SQL);
	if (mysqli_num_rows($req)!=0) {
		while ($enr=mysqli_fetch_assoc($req)) {
			echo '<hr />';
			if ($enr['image']!="") {
				echo '<img src="../assets/images/nouvelles/'.$enr['image'].'" alt="'.$enr['titre'].'" />';
			}
			if ($enr['date']!="") {
				echo '<span class="date">'.afficheDate($enr['date']).'</span>';
			}
			echo '<h1>'.$enr['titre'].'</h1>';
			echo $enr['texte'];
			echo '<div class="clear"></div>';
			
			echo '<div class="edit">';
			echo '<a href="'.$section.'-edit.php?id='.$enr['id'].'" class="modifier">Modifier</a>';
			echo '<a href="#" class="supprimer" idItem='.$enr['id'].'>Supprimer</a>';
			echo '</div>';
			echo '<div id="confirm_'.$enr['id'].'" class="confirm"></div>';
		}
	} else {
		echo '<p>Il n\'y a pas de nouvelles pour le moment.</p>';
	}
?>
</div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>