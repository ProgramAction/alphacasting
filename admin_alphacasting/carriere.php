<?php
	include('includes/checklogin.php');
	$section="carriere";
	if (isset($_POST['mod_carriere'])) {
		foreach($_POST as $key => $val){
			${"$key"} = trim($val);
			
			if ($key!="mod_carriere") {
			$posteID = substr($key, strpos($key, "_")+1, strlen($key));
			$item = substr($key,0, strpos($key, '_'));
			//echo $key. " = ".$val." - ".$posteID." - ".substr($key,0, strpos($key, '_'))."<br />";
			
			$SQL = "UPDATE carriere_poste SET ".$item."='".${"$item"."_"."$posteID"}."' WHERE posteID='$posteID'";
			mysqli_query($link,$SQL);
			}
		}	
		header("Location: carriere.php");
		exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carrière - Zone Client - Alphacasting</title>
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/style.css"/>

<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
<div class="content carriere">
<form action="" method="post" enctype="multipart/form-data" name="form1">
<input type="submit" name="mod_carriere" id="mod_carriere" value="Modifier" />
<div class="clear"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<thead>
    <tr>
        <th class="departement">Département</th>
        <th class="noposte">#poste</th>
        <th class="titreposte">Titre du poste</th>
        <th class="jour">Jour</th>
        <th class="soir">Soir</th>
        <th class="nuit">Nuit</th>
    </tr>
</thead>
<?php
	$SQL = "SELECT * FROM carriere_departement ORDER BY departementID ASC";
	$req = mysqli_query($link,$SQL);
	if (mysqli_num_rows($req)!=0) {
		while ($enr=mysqli_fetch_assoc($req)) {
			$idDepartement = $enr['departementID'];
			$nomDepartement = $enr['nomFR'];
			$SQL_poste = "SELECT * FROM carriere_poste WHERE departementID='$idDepartement' ORDER BY posteID ASC";
			$req_poste = mysqli_query($link,$SQL_poste);
			if (mysqli_num_rows($req_poste)!=0) {
				$a=1;
				while ($enr_poste=mysqli_fetch_assoc($req_poste)) {
					$noPoste = $enr_poste['posteID'];
					$titrePoste = $enr_poste['nomFR'];
					$jour = $enr_poste['jour'];
					$soir = $enr_poste['soir'];
					$nuit = $enr_poste['nuit'];
					
					if ($jour!=0 || $soir!=0 || $nuit!=0) {
						$posteDisponible=1;
					} else {
						$posteDisponible=0;
					}
					
					echo '<tr '.((mysqli_num_rows($req_poste)==$a)?'class="finDepartement"':'').'>';
					if ($a==1) echo '<td class="departement" rowspan="'.mysqli_num_rows($req_poste).'">'.$nomDepartement.'</td>';
					echo '<td '.(($posteDisponible==1)?'class="disponible"':'').'>'.$noPoste.'</td>';
					echo '<td class="titreposte'.(($posteDisponible==1)?' disponible':'').'">'.(($posteDisponible==1)?'<a href="#">'.$titrePoste.'</a>':$titrePoste).'</td>';
					echo '<td><input type="text" value="'.(($jour!=0)?$jour:'').'" name="jour_'.$noPoste.'" id="jour_'.$noPoste.'" /></td>';
					echo '<td><input type="text" value="'.(($soir!=0)?$soir:'').'" name="soir_'.$noPoste.'" id="soir_'.$noPoste.'" /></td>';
					echo '<td><input type="text" value="'.(($nuit!=0)?$nuit:'').'" name="nuit_'.$noPoste.'" id="nuit_'.$noPoste.'" /></td>';
					echo '</tr>';
					$a++;
				}
			}
		}
	}
?>
</table>
<input type="submit" name="mod_carriere" id="mod_carriere" value="Modifier" />
<div class="clear"></div>
</form>
</div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>