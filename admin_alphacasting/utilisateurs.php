<?php
	include('includes/checklogin.php');
	$section="utilisateurs";
	
	$erreurs = array();
	$cie = $nom = $prenom = $email = $langue = "";
	$urlEmail = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';
	$urlEmail = str_replace("admin_alphacasting", "client", $urlEmail);
	
	if (isset($_POST['btn_new_user'])) {
		$cie = trim(addslashes($_POST['cie']));
		if ($cie=="") {
			$erreurs['cie']=1;
		}
		$nom = trim(addslashes($_POST['nom']));
		if ($nom=="") {
			$erreurs['nom']=1;
		}
		$prenom = trim(addslashes($_POST['prenom']));
		if ($prenom=="") {
			$erreurs['prenom']=1;
		}
		$email = trim(addslashes($_POST['email']));
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$erreurs['email']=1;
		}
		$langue = trim(addslashes($_POST['langue']));
		
		$SQL = "SELECT * FROM client_users WHERE usersEmail='$email'";
		$req = mysqli_query($link,$SQL);
		if (mysqli_num_rows($req)!=0) {
			$erreurs['emailexistant']=1;
		}
		
		if (count($erreurs)==0) {
			$password=substr($email,0,strpos($email, "@"));
			$passwordToShow = $password;
			$password=md5($password);
			
			$SQL = "INSERT INTO client_users (usersPrenom, usersNom, usersCie, usersEmail, usersPassword, usersLang, usersUpdPwd) VALUES('$prenom', '$nom', '$cie', '$email', '$password', '$langue', '1')";
			$req = mysqli_query($link,$SQL);
			$nomDossier = md5(mysqli_insert_id($link).$cie);
			
			mkdir("../client/repertoires/".$nomDossier."/", 0777);
			//chmod("../images/bijoux/".$urlFR."/", 0777);
		
			require("includes/phpmailer.inc");
			require("includes/smtp.inc");
			
			$html = '<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head><title></title></head><body>';
			$html.= '<p><img src="'.$urlEmail.'images/alphacasting_header.png" width="205" height="68" /></p>';
			if ($langue==1) {
				$html .= "Hi ".$prenom." ".$nom.", <br /><br />";
				$html .= "An account has been created for you to access our client area. <br />";
				$html .= "Your password to start is: ".$passwordToShow." <br />";
				$html .= "You will be prompted to change this password at the first login. <br /><br />";
				$html .= "<a href=\"".$urlEmail."?lang=en\">Click here to access the client area </a><br /><br />";
				$html .= "Best regards, <br /><br />";
				$html .= "Alphasting Inc.";
			} else {
				$html .= "Bonjour ".$prenom." ".$nom.", <br /><br />";
				$html .= "Un compte a été créé pour vous afin d'accéder à notre zone client. <br />";
				$html .= "Votre mot de passe pour débuter est : ".$passwordToShow." <br />";
				$html .= "Vous serez invité à changer ce mot de passe lors de la première connexion. <br /><br />";
				$html .= "<a href=\"".$urlEmail."\">Cliquez ici pour accéder à la zone client. </a><br /><br />";
				$html .= "Cordialement, <br /><br />";
				$html .= "Alphacasting Inc.";
			}
			$html.= "</body></html>";
			$html=stripslashes($html);
			$mail = new PHPmailer();
			$mail->CharSet = "UTF-8";
			$mail->IsMail();
			$mail->IsHTML(true);
			$mail->Host='localhost';
			$mail->From="no-reply@alphacasting.com";
			$mail->FromName = "Alphacasting";
			$mail->AddAddress($email);
			$mail->WordWrap = 50;	// set word wrap
			$mail->IsHTML(true);	// send as HTML	
			if ($langue==1) {
				$mail->Subject="Your Alphacasting client access account";
			} else {
				$mail->Subject="Votre compte zone client Alphacasting";
			}
			$mail->Body = $html;
			
			if(!$mail->Send()) {
				$mailer_error .= "Mailer Error: " . $mail->ErrorInfo;
			} else {
				header("Location: utilisateurs.php");
				exit;
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Utilisateurs - Zone Client - Alphacasting</title>
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<link rel="stylesheet" type="text/css" href="../client/css/jquery-ui.css"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
<script type="text/javascript" src="../client/js/jquery-ui.js"></script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
    <div class="content main">
    <h1>Liste des utilisateurs - Zone client</h1>
    <p>Cliquer sur le nom de l'entreprise pour accéder au dossier du client.</p>
<?php
	if (isset($erreurs['emailexistant'])) {
		echo '<p class="rouge">Utilisateur déjà existant.</p>';
	}
?>
<form action="" method="post" enctype="multipart/form-data" name="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <th>Entreprise</th>
    <th>Nom</th>
    <th>Prénom</th>
    <th>Courriel</th>
    <th>Langue</th>
    <th>&nbsp;</th>
</tr>
<tr class="newemplpye">
    <td><input name="cie" type="text" value="<?php echo stripslashes($cie); ?>" <?php if(isset($erreurs['cie'])) echo 'class="erreur"'; ?> /></td>
    <td><input name="nom" type="text" value="<?php echo stripslashes($nom); ?>" <?php if(isset($erreurs['nom'])) echo 'class="erreur"'; ?> /></td>
    <td><input name="prenom" type="text" value="<?php echo stripslashes($prenom); ?>" <?php if(isset($erreurs['prenom'])) echo 'class="erreur"'; ?> /></td>
    <td><input name="email" type="text" value="<?php echo stripslashes($email); ?>" <?php if(isset($erreurs['email'])) echo 'class="erreur"'; ?> /></td>
    <td>
    <select name="langue" id="langue">
    <option value="2" <?php if ($langue==2) echo 'selected="selected"'; ?>>Français</option>
	<option value="1" <?php if ($langue==1) echo 'selected="selected"'; ?>>Anglais</option>
    </select>
	</td>
    <td class="btn_new_user"><input name="btn_new_user" type="submit" value="Ajouter" /></td>
</tr>
<?php
	$SQL = "SELECT * FROM client_users WHERE usersIsAdmin=0 ORDER by usersNom ASC";
	$req = mysqli_query($link,$SQL);
	if (mysqli_num_rows($req)!=0) {
		$i=1;
		while ($enr=mysqli_fetch_assoc($req)) {
			echo '<tr class='.(($i%2)?'"pair"':'"impair"').'>';
			echo '<td><a href="dossiers-utilisateurs.php?iduser='.$enr['usersID'].'">'.$enr['usersCie'].'</a></td>';
			echo '<td>'.$enr['usersNom'].'</td>';
			echo '<td>'.$enr['usersPrenom'].'</td>';
			echo '<td>'.$enr['usersEmail'].'</td>';
			echo '<td class="employesaction">'.(($enr['usersLang']==1)?'Anglais':'Français').'</td>';
			echo '<td class="employesaction"><a href="#" class="deluser" nomemploye="'.$enr['usersNom'].'" prenomemploye="'.$enr['usersPrenom'].'" numemploye="'.$enr['usersID'].'" usercie="'.$enr['usersCie'].'">Supprimer</a></td>';
			echo '</tr>';
			$i++;
		}
	}
	echo '<tr class="total">';
	echo '<td colspan="6">Total : '.mysqli_num_rows($req).' utilisateur'.((mysqli_num_rows($req)>1)?'s':'').'</td>';
	echo '</tr>';
?>
</table>
</form>
    </div>
<?php include('includes/admin_users.inc.php'); ?>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>