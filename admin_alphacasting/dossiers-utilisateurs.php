<?php
	include('includes/checklogin.php');
	$section="utilisateurs";
	
	if (isset($_GET['iduser'])) {
		$idUser=$_GET['iduser'];
		if (is_numeric($idUser)) {
			$SQL = "SELECT * FROM client_users WHERE usersID='$idUser'";
			$req = mysqli_query($link,$SQL);
			if (mysqli_num_rows($req)!=0) {
				$enr=mysqli_fetch_assoc($req);
				$usersCie=$enr['usersCie'];
				$usersPrenom=$enr['usersPrenom'];
				$usersNom=$enr['usersNom'];
				$nomDossier = md5($idUser.$usersCie);
				$cheminFichier = "../client/repertoires/".$nomDossier."/";
			}
			
			if (isset($_FILES['my_file'])) {
                $myFile = $_FILES['my_file'];
                $fileCount = count($myFile["name"]);
				$dateFichier = date("Y-m-d");
				
                for ($i = 0; $i < $fileCount; $i++) {
					$tmp_file = $myFile["tmp_name"][$i];
					$nomFichier = $myFile["name"][$i];
					
					$SQL = "SELECT * FROM client_fichiers WHERE fichiersNom='$nomFichier' AND usersID='$idUser'";
					$req = mysqli_query($link,$SQL);
					
					if (move_uploaded_file($tmp_file, $cheminFichier.$nomFichier)) {
						if (mysqli_num_rows($req)==0) {
							$SQL = "INSERT INTO client_fichiers (fichiersNom, date, usersID) VALUES ('$nomFichier', '$dateFichier', '$idUser')";
						} else {
							$SQL = "UPDATE client_fichiers SET date='$dateFichier WHERE nomFichier='$nomFichier'";
						}
						mysqli_query($link,$SQL);
					}
                }
				header("Location: dossiers-utilisateurs.php?iduser=".$idUser);
				exit;
            }
		}
	}
	
	if (isset($_POST['confirm_delete'])) {
		$id = $_POST['sup_id'];
		if (is_numeric($id)) {
			$SQL = "SELECT * FROM client_fichiers WHERE fichiersID='$id'";
			$req = mysqli_query($link,$SQL);
			if (mysqli_num_rows($req)!=0) {
				$enr=mysqli_fetch_assoc($req);
				unlink($cheminFichier.$enr['fichiersNom']);	
			}
			
			$SQL = "DELETE FROM client_fichiers WHERE fichiersID='$id'";
			mysqli_query($link,$SQL);
			
			header("Location: dossiers-utilisateurs.php?iduser=".$idUser);
			exit;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Utilisateurs - Zone Client - Alphacasting</title>
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="../client/css/jquery-ui.css"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
<script type="text/javascript" src="../client/js/jquery-ui.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.deleteFichier').click(function(e) {
			e.preventDefault();
			
			idItem = $(this).attr("idItem");
			
			$.ajax({
				type:"POST",
				url: "includes/ajax_delete.php",
				data:{idItem:idItem, nomItem:'fichier'},
				cache: false
			})
			.done(function(html) {
				$( "#confirm_"+idItem).html(html);
			});
		});
	});
</script>
</head>

<body>
<div class="container">
	<?php include('includes/header.php'); ?>
<div class="content main gestionfichiers">
	<div class="sousmenu">
    	<a href="#">Ajouter des fichiers</a>
    </div>
    <h1><?php echo $usersPrenom." ".$usersNom." (".$usersCie.")"; ?></h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th>Nom du document</th>
        <th>Date</th>
        <th>Téléchargement</th>
        <th>&nbsp;</th>
    </tr>
    <?php
		$SQL = "SELECT * FROM client_fichiers WHERE usersID='$idUser' ORDER BY fichiersNom ASC";
		$req = mysqli_query($link,$SQL);
		if (mysqli_num_rows($req)!=0) {
			$i=1;
			while ($enr=mysqli_fetch_assoc($req)) {
				echo '<tr class='.(($i%2)?'"pair"':'"impair"').'>';
				echo '<td class="nomFichier"><a href="#">'.$enr['fichiersNom'].'</a></td>';
				echo '<td class="dateFichier">'.$enr['date'].'</td>';
				echo '<td class="telechargement">'.$enr['fichiersTelecharge'].'</td>';
				echo '<td class="supFichier"><a href="#" idUser="'.$idUser.'" userCie="'.$usersCie.'" idFichier="'.$enr['fichiersID'].'" nomFichier="'.$enr['fichiersNom'].'">Supprimer</a></td>';
				echo '</tr>';
				//echo '<div id="confirm_'.$enr['fichiersID'].'" class="confirm"></div>';
				$i++;
			}
			echo '<tr class="total">';
			echo '<td colspan="6">Total : '.mysqli_num_rows($req).' document'.((mysqli_num_rows($req)>1)?'s':'').'</td>';
			echo '</tr>';
		}
	?>
    </table>
    </div>
<?php include('includes/admin_fichiers.inc.php'); ?>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>