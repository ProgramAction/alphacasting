<?php
	include('includes/checklogin.php');
	if (isset($_GET['erreur'])) {
		if ($_GET['erreur']==1) {
			$txt_erreur="Vous devez vous connecter pour accéder &agrave; la gestion.";
		}elseif ($_GET['erreur']==2) {
			$txt_erreur="Utilisateur ou mot de passe incorrect.";
		}elseif ($_GET['erreur']==3) {
			$txt_erreur="Compte inexistant.";
		}elseif ($_GET['erreur']==4) {
			$txt_erreur="Votre compte n'est pas encore activ&eacute;.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr-CA">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="fr-CA">
<title>Console de Gestion - <?php echo $nomdusite; ?></title>
<link rel="icon" type="image/ico" href="../favicon.ico" />
<link rel="stylesheet" type="text/css" href="../client/css/style.css"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../client/js/contentheight.js"></script>
</head>

<body>
<div class="container accueil">
	<?php include('includes/header.php'); ?>
    <div class="content main accueil">
    <img src="../client/images/alphacasting.png" width="253" height="84" alt="Alphacasting" />
	<h1>Console de gestion</h1>
    <div class="boitelogin">
    <?php
	if (isset($txt_erreur)) {
		echo '<p class="rouge">'.$txt_erreur.'</p>';
	}
	?>
    <form name="frm" method="post" action="">
    
    <div><input type="text" name="courriel" id="courriel" placeholder="Nom d'usager" /></div>
    <div><input type="password" name="password" id="password" placeholder="Mot de passe" /></div>
    <div><input type="submit" value="Connexion" name="btnLogin" id="btnLogin"/></div>

    </form>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    </div>
    </div>
<?php include('includes/footer.php'); ?>
</div>
</body>
</html>