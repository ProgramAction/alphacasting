<?php
	$parts = Explode('/', $_SERVER['PHP_SELF']);
	$pagecourante = $parts[count($parts) - 1];
?>
<?php if ($login==1 && $pagecourante!="index.php") { ?>
<div class="header">
    <div class="content">
        <img src="../client/images/alphacasting_header.png" width="205" height="68" alt="Alphacasting" />
        <ul class="nav">
        	<li><a href="utilisateurs.php" <?php if ($section=="utilisateurs") echo 'class="selected"'; ?>>Gestion des utilisateurs</a></li>
        	<li><a href="nouvelles.php" <?php if ($section=="nouvelles") echo 'class="selected"'; ?>>Nouvelles</a></li>
        	<li><a href="carriere.php" <?php if ($section=="carriere") echo 'class="selected"'; ?>>Carrière</a></li>
            <li><a href="includes/logout.php">Déconnexion</a></li>
        </ul>
    </div>
    <div class="bandeau"></div>
</div>
<?php } ?>