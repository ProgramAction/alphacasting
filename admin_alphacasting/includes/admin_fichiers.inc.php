<div class="dialog_action uploadFichier">
	<form method="post" enctype="multipart/form-data">
    	<label for="file">Sélectionner vos fichiers</label>
		<input type="file" name="my_file[]" id="file" multiple>
		<input type="submit" value="Envoyer">
	</form>
</div>
<script>
$(function() {
    $( ".dialog_action.uploadFichier" ).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		buttons: [
		{
			text: "Annuler",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
	});
});

$(".gestionfichiers .sousmenu a" ).click(function( event ) {
	$( ".dialog_action.uploadFichier" ).dialog( "open" );
	event.preventDefault();
});
</script>

<div class="dialog_action supFichier">
	<p class="action"></p>
    <p class="nom"></p>
    <img src="images/loading.gif" width="46" height="17" />
</div>
<script>
var idUser
var idFichier;
var nomFichier;
var userCie;

$(function() {
    $( ".dialog_action.supFichier" ).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		buttons: [
		{
			text: "Oui",
			click: function() {
				$('.dialog_action img').show();
				$.ajax({
					type:"POST",
					url: "includes/ajax_admin_fichiers.php",
					data:{idUser:idUser, userCie:userCie, idFichier:idFichier, nomFichier:nomFichier},
					cache: false
				})
				.done(function(html) {
					//$('.dialog_action.supFichier p.nom').html(html);
					location.reload();
				});
			}
		},
		{
			text: "Non",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
	});
});

$(".main td.supFichier a" ).click(function( event ) {
	idUser = $(this).attr("idUser");
	userCie = $(this).attr("userCie");
	idFichier = $(this).attr("idFichier");
	nomFichier = $(this).attr("nomFichier");
	
	$('.dialog_action p.action').html("Voulez vous vraiment supprimer le fichier : ");
	$('.dialog_action p.nom').html(nomFichier);
	
	$( ".dialog_action.supFichier" ).dialog( "open" );
	event.preventDefault();
});
</script>