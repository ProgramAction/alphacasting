<div class="dialog_action">
	<p class="action"></p>
    <p class="nom"></p>
    <img src="images/loading.gif" width="46" height="17" />
</div>
<script>
var numemploye;
var nomemploye;
var actionemploye;
var usercie;

$(function() {
    $( ".dialog_action" ).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		autoOpen: false,
		buttons: [
		{
			text: "Oui",
			click: function() {
				$('.dialog_action img').show();
				$.ajax({
					type:"POST",
					url: "includes/ajax_admin_employes.php",
					data:{numemploye:numemploye, nomemploye:nomemploye, actionemploye:actionemploye, usercie:usercie},
					cache: false
				})
				.done(function(html) {
					//$('.dialog_action p.nom').html(html);
					location.reload();
				});
			}
		},
		{
			text: "Non",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
	});
});

$( "a.deluser" ).click(function( event ) {
	nomemploye = $(this).attr("nomemploye");
	prenomemploye = $(this).attr("prenomemploye");
	numemploye = $(this).attr("numemploye");
	usercie = $(this).attr("usercie");
	actionemploye = "deluser";
	
	$('.dialog_action p.action').html("Voulez vous vraiment supprimer l'utilisateur : ");
	$('.dialog_action p.nom').html(prenomemploye+' '+nomemploye);
	
	$( ".dialog_action" ).dialog( "open" );
	event.preventDefault();
});
</script>