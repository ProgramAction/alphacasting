<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$s1_intro_pos = 'up';
	$s2_intro_pos = 'up';
	$s3_intro_pos = 'up';
	$s1=array(
		1=>Lang::insert('shipping-s1-txt-p1'),
		2=>Lang::insert('shipping-s1-txt-p2'),
		3=>Lang::insert('shipping-s1-txt-p3'),
	);
	$s3=array(
		1=>Lang::insert('shipping-s3-txt-p1'),
		2=>Lang::insert('shipping-s3-txt-p2'),
		3=>Lang::insert('shipping-s3-txt-p3'),
		4=>Lang::insert('shipping-s3-txt-p4'),
	);
	$features=array(
		// 1=>array("titre"=>Lang::insert('shipping-features-h3-1'), "p"=>Lang::insert('shipping-features-p-1'), "details"=>Lang::insert('shipping-features-details-1')),
	);
	
	include('../includes/casting-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>