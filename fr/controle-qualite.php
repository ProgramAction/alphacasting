<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php include('../includes/top-title.php'); ?>
<?php
	$s1_intro_pos = 'up';
	$s2_intro_pos = 'up';
?>
<div class="row overflow-hidden py-5">
	<div class="col-lg-12 col-xl-10 offset-xl-1 d-flex align-items-center py-5">
		<div>
			<?php if ($s1_intro_pos=='up') { ?><p class="intro mb-0" data-aos="zoom-in"><?php Lang::write($pagesKey.'-s1-intro'); ?></p><?php } ?>
			<h2 data-aos="zoom-out" class="<?php if ($s1_intro_pos=='up') { ?>mb-4<?php } ?>"><strong><?php Lang::write($pagesKey.'-s1-h2'); ?></strong></h2>
			<?php if ($s1_intro_pos=='down') { ?><p class="intro" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s1-intro'); ?></p><?php } ?>
			<p data-aos="zoom-in"><?php Lang::write($pagesKey.'-s1-txt-p1'); ?></p>
			<ul data-aos="zoom-in" class="ml-3 ml-sm-5"><?php Lang::write($pagesKey.'-s1-txt-p2'); ?></ul>
			<p data-aos="zoom-in" class="mt-4 pb-4"><?php Lang::write($pagesKey.'-s1-txt-p3'); ?></p>
			<div data-aos="zoom-in" class="d-flex flex-wrap"><?php Lang::write($pagesKey.'-s1-txt-p4'); ?></div>
		</div>
	</div>
</div>
<div class="row s2">
	<div class="col-lg-6 d-flex align-items-center justify-content-center bg-black">
		<div class="px-lg-5 py-5 max-w600">
			<p data-aos="zoom-in" data-aos-delay="50"><?php Lang::write($pagesKey.'-s2-p1'); ?></p>
		</div>
	</div>
	<div class="col-lg-6 px-0 bg-grispale d-flex align-items-center justify-content-center">
		<img src="../assets/images/<?php echo $pages[$pagesKey]['s2-img-1']; ?>" alt="<?php Lang::write($pagesKey.'-s2-img-ALT'); ?>" class="img-fluid objfitcover">
	</div>
</div>
<div class="row s2">
	<div class="bas-prix col-lg-6 order-2 order-lg-1  d-flex align-items-center justify-content-center">
		<div class="px-lg-5 py-5">
			<img src="../assets/images/svg/<?php echo $pages[$pagesKey]['icon']['file']; ?>" width="<?php echo $pages[$pagesKey]['icon']['width']; ?>" role="presentation" alt="" class="img-fluid">
		</div>
	</div>
	<div class="col-lg-6 d-flex align-items-center justify-content-center bg-rouge order-1 order-lg-2">
		<div class="px-lg-5 py-5 max-w600" data-aos="zoom-in">
			<?php Lang::write($pagesKey.'-s2-p2'); ?>
		</div>
	</div>
</div>
<div class="row s2">
	<div class="col-lg-6 px-0 order-1 order-lg-2">
		<img src="../assets/images/<?php echo $pages[$pagesKey]['s2-img-2']; ?>" alt="<?php Lang::write($pagesKey.'-s2-img-2-ALT'); ?>" class="img-fluid objfitcover">
	</div>
	<div class="col-lg-6 d-flex align-items-center justify-content-center order-2 order-lg-1">
		<div class="px-lg-5 py-5 max-w600" data-aos="zoom-in">
			<?php Lang::write($pagesKey.'-s2-p3'); ?>
		</div>
	</div>
</div>
<?php include('../includes/cta_template.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>