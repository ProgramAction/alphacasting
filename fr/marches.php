<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<div class="markets-home">
		<?php include('../includes/top-title.php'); ?>
		<div class="row py-5">
			<div class="col-xl-10 offset-xl-1">
				<p>Nous servons plusieurs marchés avec la même intensité, et nous savons comment relever les défis.</p>
				<p>L'expertise que nous avons acquise dans les domaines de l'aviation et du médical requiert non seulement la compétence de nos équipes, mais aussi la capacité à maintenir la rigueur à chaque étape de la production jusqu'à la livraison.</p>
				<p>Nous disposons de certifications et maîtrisons les alliages métalliques pour les industries suivantes&nbsp;:</p>
			</div>
		</div>
		<?php include('../includes/markets_items.php'); ?>
	</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>