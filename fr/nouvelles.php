<?php
	$lang="FR";
    setlocale(LC_TIME, $lang == 'EN'?'en_CA':'fr_CA');
	include('../includes/global.inc.php');
    $news = \Programaction\News::get_news_by_lang($lang);
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php if(isset($_GET['page'])): ?>
    <base href="<?php echo '../'.basename(__DIR__) ?>">
<?php endif; ?>
<?php include('../includes/head.inc.php'); ?>

</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
        <div class="row">
            <div class="col-12">
            <?php if(count($news) == 0): ?>
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<h2 class="py-5"><?php Lang::write('news-no-res') ?></h2>
					</div>
				</div>
            <?php else: ?>
                <?php foreach($news as $new): ?>
                    <div class="row py-5 new-row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-5 pb-0 pb-lg-5 pt-5 pl-0 pr-0 pr-lg-3 pr-xl-5 ">
										<?php if ($new['image']!="" && file_exists($assetsPath.'images/nouvelles/'.$new['image'])) { ?>
                                        <figure>
                                            <img src="<?php echo $assetsPath.'images/nouvelles/'.$new['image']; ?>" alt="">
                                        </figure>
										<?php } ?>
                                    </div>
                                    <div class="col-lg-7 py-5 content pl-0 pl-lg-3 pl-xl-5">
                                        <p class="date mb-1"><?php echo date("F Y",strtotime($new['date'])); ?></p>
                                        <h2><strong><?php echo $new['titre'] ?></strong></h2>
                                        <p class="pt-2">
                                            <?php echo $new['intro'] ?>
                                        </p>
										<?php if ($new['texte']!="") { ?>
										<p><a href="<?php echo Lang::insert('news-url').'/'.$new['slug'] ?>" class="btn btn-black"><?php Lang::write('news-read-more') ?></a></p>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php \Programaction\News::paginator($lang); ?>
            <?php endif; ?>
            </div>
        </div>
	<?php include('../includes/cta_template.php'); ?>
	<?php include('../includes/swiper_casting.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>