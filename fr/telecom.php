<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$mainP=array(
		1=>Lang::insert('telecom-p-1'),
		2=>Lang::insert('telecom-p-2'),
		3=>Lang::insert('telecom-p-3'),
		4=>Lang::insert('telecom-p-4')
	);
	$features=array(
		1=>array("titre"=>Lang::insert('telecom-features-h3-1'), "details"=>Lang::insert('telecom-features-details-1')),
		2=>array("titre"=>Lang::insert('telecom-features-h3-2'), "details"=>Lang::insert('telecom-features-details-2')),
	);
	$horizontal=array();
	include('../includes/markets-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>