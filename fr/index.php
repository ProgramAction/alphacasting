<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
    <script src="<?php echo $assetsPath; ?>js/smooth-scrollbar.js"></script>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
    <div class="row video">
        <div class="col-12 px-0">
            <video autoplay loop muted poster="<?php echo $assetsPath; ?>images/videos/accueil.jpg">
                <source src="<?php echo $assetsPath; ?>videos/alphacasting-accueil.mp4?v=1" type="video/mp4">
                <source src="<?php echo $assetsPath; ?>videos/alphacasting-accueil.webm?v=1" type="video/webm">
                <?php Lang::write('cta-novideo'); ?>
            </video>
        </div>
    </div>
    <div class="row shortcuts">
        <div class="col-md-4 pt-5">
            <div>
                <h2>À propos de</h2>
                <p>Alphacasting <br>Leader mondial du moulage de précision coulant plus de 120 types d'alliages différents. </p>
                <a href="<?php echo $pages['about']['url']; ?>" class="btn btn-black">En savoir plus</a>
            </div>
        </div>
        <div class="col-md-4 pt-5">
            <div>
                <h2>Nouvelles</h2>
                <p>Moulage en aluminium pour la Ford GT. Nous sommes fiers d'être un fournisseur clé de Multimatic, fabricant de la Ford GT.</p>
                <a href="<?php echo $pages['news']['url']; ?>" target="_blank" class="btn btn-black">Voir toutes les nouvelles</a>
            </div>
        </div>
        <div class="col-md-4 pt-5">
            <div>
                <h2>Certifications</h2>
                <p>Nous sommes fiers de notre programme de qualité qui répond aux spécifications les plus strictes, nettement supérieur aux normes établies dans de nombreuses industries.</p>
                <a href="<?php echo $pages['certifications']['url']; ?>" class="btn btn-black">En savoir plus</a>
            </div>
        </div>
    </div>
    <div class="row markets position-relative">
        <div class="col-lg-6 d-flex align-items-center justify-content-lg-center py-5">
            <div>
                <p class="titre_markets">Marchés</p>
                <h2>Des marchés <br class="d-none d-lg-inline">de précision <br class="d-none d-lg-inline">aux besoins <br class="d-none d-lg-inline">précis</h2>
            </div>
        </div>
    </div>
    <div class="hor position-relative">
	<?php include('../includes/markets_items.php'); ?>
    </div>
<?php include('../includes/footer.php'); ?>
<script>
    var horOffsetTop;
    var mLeft;
    var containerW;
    var footerOffsetTop;
    
	var elem = document.querySelector("#home");
	var scrollbar = window.Scrollbar;
	var scroll;
	var scrollPos;
	
	scrollbar = Scrollbar.init(elem, {
		alwaysShowTracks:true
	});

	scrollbar.addListener(function (status) {
		scroll = status.offset.y;
		if (scroll>10) {
			$("header").addClass("bgblack");
		} else {
			$("header").removeClass("bgblack");
		}

		var scrollStop = parseInt(horOffsetTop)+parseInt($('.hor').height())-parseInt($('.markets').height());
		if (scroll < scrollStop) {
			if ( scroll >= horOffsetTop && ((horOffsetTop - scroll) >= ($(window).width() - containerW)) ) {
				scrollPos = scroll - (horOffsetTop);
				$('.hor, .markets').css( 'transform', 'translateY('+scrollPos+'px)' );
				$('.markets_items_container').css('transform', 'translateX('+(scrollPos*-1)+'px)');
			}
		} else {
			var topPos = scrollStop-parseInt(horOffsetTop);
			$('.hor, .markets').css( 'transform', 'translateY('+topPos+'px)' );
			$('.markets_items_container').css('transform', 'translateX('+(topPos*-1)+'px)');
		}
	});

    function markets() {
        var headerHeight = $('header').height();
        var viewHeight = $(window).height() - headerHeight;
        var mLeft = parseInt( $('.markets_items_container').css('marginLeft') );
        containerW = $(".markets_items_container").width()+mLeft;
        $('.hor').height( $(".markets_items_container").width()+viewHeight-mLeft-8 );
        $('.markets, .markets_items, .markets_items_container').height(viewHeight);
        $('.markets_items').css('margin-top',viewHeight*-1);
		
		///*******///
		horOffsetTop = ($('.video').height()+$('.shortcuts').outerHeight()) - $('header').height();
		///*******///
		
		scrollbar.update();
    }
    
    $(document).ready(function() {
        markets();
    });
	
	var resizeTimer;
    $(window).on('resize', function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			markets()
		}, 1000);
    });

</script>
</body>
</html>