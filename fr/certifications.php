<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"/>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1">
			<h2 class="mb-4">Programme de qualité</h2>
			<p>Notre but est de partager une mission et des objectifs communs à tous les niveaux de notre entreprise. En raison de son désir et de son engagement à soutenir l'avancement de ses clients, Alphacasting s'est dotée des outils nécessaires.</p>
			<p>Par conséquent, nous sommes fiers de notre programme de qualité qui répond aux spécifications les plus strictes, nettement supérieur aux normes établies dans de nombreuses industries.</p>
			<p>Nous sommes reconnus par les organismes gouvernementaux canadiens et américains qui supervisent tous les échanges de données techniques et la production de biens liés aux fabricants militaires.</p>
			<ul class="list-unstyled mt-5 d-flex flex-wrap flex-md-nowrap pb-1">
				<li class="pr-5 pb-4"><img src="../assets/images/logo-accredited-nadcap.png" width="223" height="82" alt="Logo Accredited Nadcap"></li>
				<li class="pb-4"><img src="../assets/images/logo-intertek.png" width="75" height="90" alt="Logo Intertek"></li>
			</ul>
		</div>
	</div>
	<div class="row py-5 certificats">
		<div class="col-xl-10 offset-xl-1">
			<div class="row pt-5">
				<div class="col-md-6 col-lg-4">
					<ul>
						<li class="p"><a href="../assets/images/certifications/Nadcap-Non-Destructive.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Essais non destructifs</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-Welding-2021.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Soudure</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-HeatTreating.jpg" data-fancybox="certifications"><strong>Nadcap</strong> Traitement thermique</a></li>
						<li class="p"><a href="../assets/images/certifications/Nadcap-HeatTreating.jpg" data-fancybox="certifications"><strong>Intertek</strong> AS/EN/JISQ9100:2009 <br>ISO 9001-2008</a></li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-4">
					<ul>
						<li class="p"><a href="../assets/images/certifications/CPG.jpg" data-fancybox="certifications"><strong>CPG</strong> Programme des marchandises contrôlées</a></li>
						<li class="p"><a href="../assets/images/certifications/JCP.jpg" data-fancybox="certifications"><strong>Certification JCP</strong> Accord sur les données techniques</a></li>
						<li class="p"><a href="../assets/images/certifications/PEP-PIP.png" data-fancybox="certifications"><strong>PEP - PIP</strong> Partenaires pour la protection</a></li>
					</ul>
				</div>
				<div class="col-lg-4 pt-5 pt-lg-0">
					<img src="../assets/images/badge-alphacasting-quality.png" width="510" height="510" alt="" role="presentation" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
<?php include('../includes/cta_template.php'); ?>
<?php include('../includes/swiper_casting.php'); ?>
<?php include('../includes/footer.php'); ?>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
</body>
</html>