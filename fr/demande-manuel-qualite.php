<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php include('../includes/top-title.php'); ?>
<?php include('../includes/quality-form.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>