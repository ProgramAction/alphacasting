<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$mainP=array(
		1=>Lang::insert('commercial-p-1'),
		2=>Lang::insert('commercial-p-2'),
		3=>Lang::insert('commercial-p-3'),
		4=>Lang::insert('commercial-p-4')
	);
	$features=array();
	$horizontal=array();
	include('../includes/markets-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>