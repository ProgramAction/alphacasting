<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$s1_intro_pos = 'up';
	$s2_intro_pos = 'up';
	$s3_intro_pos = 'up';
	$s1=array(
		1=>Lang::insert('rapidprototyping-s1-txt-p1'),
		2=>Lang::insert('rapidprototyping-s1-txt-p2'),
		3=>Lang::insert('rapidprototyping-s1-txt-p3'),
	);
	$s3=array(
		1=>Lang::insert('rapidprototyping-s3-txt-p1'),
		2=>Lang::insert('rapidprototyping-s3-txt-p2'),
		3=>Lang::insert('rapidprototyping-s3-txt-p3'),
		4=>Lang::insert('rapidprototyping-s3-txt-p4'),
	);
	$features=array(
		// 1=>array("titre"=>Lang::insert('rapidprototyping-features-h3-1'), "p"=>Lang::insert('rapidprototyping-features-p-1'), "details"=>Lang::insert('rapidprototyping-features-details-1')),
	);
	
	include('../includes/casting-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>