<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1">
			<h2>Leader mondial de la fonderie de précision</h2>
			<p>Fondée en 1991, Alphacasting est rapidement devenue un leader mondial de la fonderie de précision, coulant plus de 120 types d'alliages différents.</p>
			<p>Alphacasting est spécialisée dans la production de pièces moulées innovantes et modernes pour l'industrie de haute technologie.</p>
			<p>Nous adhérons à des directives industrielles strictes et répondons aux demandes des clients avec efficacité et ponctualité.</p>
			<p>La qualité et la livraison à temps sont les raisons pour lesquelles Alphacasting continue d'être à la fine pointe de l'industrie du moulage.</p>
		</div>
	</div>
	<div class="row pt-5 s2">
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-black">
			<div class="px-lg-5 py-5 max-w600">
				<h2 data-aos="zoom-out" class="text-uppercase"><strong>Expérience</strong></h2>
				<p class="intro" data-aos="zoom-in" data-aos-delay="25">Plus de 30 ans d'expérience dans le moulage à la cire perdue, le moulage par fusion à l’air et le moulage sous vide des alliages. </p>
			</div>
		</div>
		<div class="col-lg-6 px-0">
			<img src="../assets/images/alphacasting-icon-metal.jpg" alt="" role="presentation" class="img-fluid objfitcover">
		</div>
	</div>
	<div class="row s2">
		<div class="col-lg-6 px-0 order-2 order-lg-1">
			<img src="../assets/images/ferrous-alloys.jpg" alt="Worker casting alloy" class="img-fluid objfitcover">
		</div>
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-rouge order-1 order-lg-2">
			<div class="px-lg-5 py-5 max-w600" data-aos="zoom-in">
				<p data-aos="zoom-in"><strong class="text-uppercase">Gamme de produits&nbsp;</strong>: Près de 2 000 pièces différentes réalisées à ce jour.</p>
				<p data-aos="zoom-in"><strong class="text-uppercase">Clientèle&nbsp;</strong>: 400 clients, principalement en Europe occidentale, aux États-Unis, en Chine et en Australie.</p>
				<p data-aos="zoom-in"><strong class="text-uppercase">Exportations&nbsp;</strong>: 80 %, dont la plupart vers la Communauté européenne.</p>
			</div>
		</div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1 py-5">
			<h2 class="text-uppercase">Alphacasting en bref</h2>
			<h3 class="mb-3 mt-4">Située à Montréal, au Canada. <span class="sep"></span> Plus de 74 000 pi2 (6875 m²) d’espace de bureaux et de locaux de fabrication, avec 160 employés.</h3>
			<p>Spécialiste du moulage par la technique de la cire perdue, Alphacasting produit des pièces en grandes, moyennes et petites séries dans toutes les catégories d’alliages, telles que l'inox, l’aluminium et les alliages de Titane</p>
			<p>Nous avons investi dans des équipements modernes et sophistiqués afin de pouvoir répondre à toutes les demandes de nos clients en termes de fiabilité, de reproductibilité et de flexibilité.</p>
			<p>Alphacasting possède des certifications d'assurance qualité : AS9100D et ISO 9001:2015 et plusieurs qualifications spécifiques aux secteurs de l'industrie, de l'aérospatiale et de la défense. </p>
			<p>Ces certifications ont été renouvelées et bonifiées à chaque fois, conformément à l'évolution des normes concernées. </p>
			<p><a href="<?php echo $pages['contact']['url']; ?>" class="btn btn-black">Veuillez nous contacter pour toute question que vous pourriez avoir</a></p>
		</div>
	</div>
	<div class="row py-5 border-top rouge">
		<div class="col-12 text-center slogan">
            C'est là que nous nous dirigeons, c'est notre futur bâtiment.
		</div>
	</div>
	<div class="row">
		<div class="col-12 px-0">
			<img src="../assets/images/alphacasting-plant-3d.jpg" alt="Alphacasting future building" class="img-fluid">
		</div>
	</div>
	<div class="row pt-5 border-bottom rouge clients">
		<div class="container pt-4">
			<div class="row align-items-center justify-content-center flex-wrap">
				<div class="col text-center py-2"><img src="../assets/images/logos/safran.png" alt="Safran" width="237" height="80" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/boeing.png" alt="Boeing" width="214" height="63" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/bombardier.png" alt="Bombardier" width="208" height="63" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/honeywell.png" alt="Honeywell" width="236" height="75" class="img-fluid"></div>
				<div class="col text-center py-2"><img src="../assets/images/logos/thales.png" alt="Thales" width="200" height="54" class="img-fluid"></div>
			</div>
		</div>
		<div class="col-12 pt-5 text-center">
			<a href="../assets/docs/logo-clients.pdf" class="btn noir" target="_blank">Voir plus de clients heureux</a>
		</div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1 py-5">
			<p class="intro mb-0">L'énergie de </p>
			<h2 class="text-uppercase">l’équipe Alphacasting</h2>
			<p>Chaque membre de nos équipes apporte des compétences différentes à notre entreprise, en assurant un service client de la plus haute qualité tout au long de la procédure de coulée.</p>
			<p>Nos directeurs et responsables des départements sont très expérimentés en fonderie. Ils ont également une connaissance approfondie de l'industrie des métaux, de l'ingénierie et de la construction. Nos compétences techniques d'avant-garde et traditionnelles nous donnent la capacité de répondre à n'importe quelle demande de moulage. </p>
			<p>En conséquence, nous sommes très bien placés pour offrir des consultations comme suit&nbsp;:</p>
			<ul class="pl-5">
				<li class="p">LES CONSEILS D'EXPERTS</li>
				<li class="p">LA SIMULATION DU REMPLISSAGE DU MOULE ET DE LA SOLIDIFICATION DU MÉTAL</li>
				<li class="p">LA CONCEPTION/DESSIN ASSISTÉE PAR ORDINATEUR</li>
				<li class="p">LE PROTOTYPAGE RAPIDE</li>
				<li class="p">CONSEILS MÉTALLURGIQUES</li>
			</ul>
		</div>
	</div>
<?php include('../includes/cta_template.php'); ?>
<?php include('../includes/footer.php'); ?>
</body>
</html>