<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<?php include('../includes/top-title.php'); ?>
	<a href="<?php echo $pages['career']['url']; ?>" class="lk_carriere fr">Carrière</a>
	<?php include('../includes/contact-coord.php'); ?>
	<?php include('../includes/contact-form-email.php'); ?>
	<div class="row bg-yellow">
		<div class="col-12 text-center py-5 slogan">
			<?php Lang::write('f-slogan-leading'); ?>
		</div>
	</div>
	<div class="row">
		<div id="map" class="col-lg-12 px-0"></div>
	</div>
<?php include('../includes/footer.php'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi9pS-uWjNckLyi9IqolfpR9HTM4gHJ8M&callback=initMap" defer></script>
</body>
</html>