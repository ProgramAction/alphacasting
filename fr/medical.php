<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
<?php 
	$mainP=array(
		1=>Lang::insert('medical-p-1'),
		2=>Lang::insert('medical-p-2'),
		3=>Lang::insert('medical-p-3'),
		4=>Lang::insert('medical-p-4'),
	);
	$features=array(
		1=>array("titre"=>Lang::insert('medical-features-h3-1'), "details"=>Lang::insert('medical-features-details-1')),
		2=>array("titre"=>Lang::insert('medical-features-h3-2'), "details"=>Lang::insert('medical-features-details-2')),
	);
	$horizontal=array(
		1=>array("titre"=>Lang::insert('medical-horizontal-h3-1'), "details"=>Lang::insert('medical-horizontal-p-1')),
		2=>array("titre"=>Lang::insert('medical-horizontal-h3-2'), "details"=>Lang::insert('medical-horizontal-p-2')),
	);
	include('../includes/markets-template.php'); 
?>
<?php include('../includes/footer.php'); ?>
</body>
</html>