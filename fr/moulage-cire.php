<?php
	$lang="FR";
	include('../includes/global.inc.php');
?>
<!doctype html>
<html lang="<?php echo strtolower($lang); ?>-CA">
<head>
<?php include('../includes/head.inc.php'); ?>
</head>
<body class="<?php echo $pagesKey; ?>">
<?php include('../includes/header.php'); ?>
	<div class="markets-home">
		<?php include('../includes/top-title.php'); ?>
		<div class="row py-5">
			<div class="col-xl-10 offset-xl-1 details">
				<h2><strong>Vue d'ensemble</strong></h2>
				<h3 class="mb-3 mt-4">Le processus de moulage à la cire perdue</h3>
				<p>Le moulage à la cire perdue est une méthode de fabrication dans laquelle un modèle en cire est créé, puis recouvert d'un coulis céramique pour fabriquer un moule. La cire est ensuite fondue hors du moule en céramique et le métal fondu est versé dans la cavité. Le métal fondu se solidifie, et la coquille de céramique est ensuite cassée ou décapée, ce qui génère un moulage métallique. Le processus de moulage à la cire perdue ne nécessite pas de quantité minimale de commande et peut couler des pièces allant de quelques onces à plus de 200 livres, ce qui en fait une solution de fabrication quelle que soit la taille du projet sur lequel vous travaillez. </p>
				<h3 class="mb-3 mt-5">Avantages du moulage à la cire perdue</h3>
				<p>Grâce aux avantages de la fonderie à la cire perdue, vous pouvez créer presque toutes les configurations de votre composant métallique de précision. Vous pouvez concevoir des pièces aussi complexes, petites ou grandes que vous le souhaitez. </p>
				<p>Éliminez tous les obstacles qui vous retiennent !</p>
				<p>Vous réduirez les opérations secondaires et les coûts d'outillage sur plusieurs séries de production. Ce procédé a l'avantage d'offrir le plus grand choix d'alliages et de produire moins de déchets. Vous maintiendrez une répétitivité exceptionnelle et des tolérances incroyablement strictes. Des pièces plus solides seront conçues et des parois plus fines pour un poids réduit seront utilisées. Vous serez en mesure d'ébaucher des surfaces et des estampages très détaillés.</p>
				<h3 class="mb-3 mt-5">Quelques chiffres</h3>
				<ul class="pl-5">
					<li>Capacité de production&nbsp;: 250 grappes par jour (pièces d'un maximum de 500 millimètres cubes). ( 0,03 pouce cube)</li>
					<li>Capacité de fusion : 6 T par jour.</li>
					<li>Surface : 74 000 pi2 ( 6875 m²) d'usines, de bureaux et d'entrepôts.</li>
					<li>Personnel : 160 employés expérimentés.</li>
				</ul>
			</div>
		</div>
		<div class="row plant d-flex align-items-center justify-content-center">
			<div class="col-12 text-center">
				<div class="py-5">
					<h2 class="pt-4 mb-0">Voir nos départements d'usine</h2>
					<span class="material-icons-outlined">expand_more</span>
				</div>
			</div>
		</div>
		<?php include('../includes/casting_items.php'); ?>
	</div>
<?php include('../includes/footer.php'); ?>
</body>
</html>