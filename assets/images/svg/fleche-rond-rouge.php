<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 108 108" style="enable-background:new 0 0 108 108;" xml:space="preserve" class="fleche">
<g>
	<circle class="rond-rouge" cx="54" cy="54" r="54"/>
	<path class="fleche-blanc" d="M45.5,84.6l-3.9-3.9L68.4,54L41.7,27.3l3.9-3.9l28.7,28.7c1.1,1.1,1.1,2.8,0,3.9L45.5,84.6z"/>
</g>
</svg>