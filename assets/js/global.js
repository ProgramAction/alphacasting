AOS.init();

$(document).ready(function() {
    menuBG();
	contactForm();
    
    $('.open-menu, .close-menu').on('click', function(e) {
        e.preventDefault();
        $('.container_menu').toggleClass("menuopened");
    });
    
	$('.features .titre').on('click', function() {
		var ele = $(this).closest('.features');
		$(ele).toggleClass('active');
		openFeature(ele);
	});
	
	//Form File input
	$('#cf-fichier').on('change',function(){
		//get the file name
		//var fileName = $(this).val();
		var fileName = $(this).val().replace('C:\\fakepath\\', " ");
		//replace the "Choose a file" label
		$(this).next('.custom-file-label').html(fileName);
	})
});

$(window).on("scroll", function() {
    menuBG();
});

function menuBG() {
    if($(window).scrollTop() > 10) {
        $("header").addClass("bgblack");
    } else {
       $("header").removeClass("bgblack");
    }
}

function openFeature(ele) {
	if ( $(ele).hasClass('active') ) {
		$(ele).find('.details').slideDown( 1000 );
	} else {
		$(ele).find('.details').slideUp( 1000 );
	}
}

const swiper_markets = new Swiper('.swiper-container.markets', {
    spaceBetween: 24,
    slidesPerView: 1,
    loop: true,
    autoplay: {
        delay: 1,
        disableOnInteraction: true,
    },
    autoplayDisableOnInteraction: false,
    freeMode: true,
    speed: 5000,
    freeModeMomentum: false,
    breakpoints: {
        576: {
          slidesPerView: 2,
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 48,
        },
    }
});

const swiper_news = new Swiper('.swiper-news',{
	spaceBetween: 100,
	loop:true,
    slidesPerView: 1,
	slidesOffsetBefore:0,
	breakpoints: {
        576: {
          	slidesPerView: 2,
			spaceBetween: 100,
			slidesOffsetBefore:function(){
				var sliderOffset = (this.width / 3) - 25;
				return sliderOffset
			},
        },
        992: {
			slidesPerView: 3,
			spaceBetween: 100,
			slidesOffsetBefore:function(){
				var sliderOffset = (this.width / 4) - 150;
				return sliderOffset
			},
        },
    }
});

//Contact Form
function contactForm() {
	var contactForm = $('.contact-form');
	if( contactForm.length < 1 ){ return true; }

	contactForm.each( function(){
		var el = $(this);
        var elID = el.find('form').attr('id');
        var elResult = el.find('.'+elID+'-form-result');
        var formType = el.find('#sujetRequete').val();
        var formName = el.find('#form').val();
		var eventName = 'generate_lead';
		
		if (formName=='quality') {
			eventName = 'quality';
		}
        
		el.find('form').validate({
			submitHandler: function(form) {
				elResult.fadeOut( 500 );

				$(form).ajaxSubmit({
					target: elResult,
					dataType: 'json',
					success: function( data ) {
						elResult.html( data.message ).fadeIn( 500 );
						if( data.alert != 'error' ) {
							
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': eventName,
								'formType': formType
							});
							
							$(form).clearForm();
							setTimeout(function(){
								elResult.fadeOut( 500 );
							}, 5000);
						}
					}
				});
			}
		});
	});
}

//Map page Contact
let map;
function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 45.5101638, lng: -73.6685625 },
        zoom: 12,
        mapId: "7540371351f754d2"
    });
	
    // Create markers.
    let markerURL1='../assets/images/pin-location.png';
    var marker1 = new google.maps.Marker({
        position: {lat:  45.5101638, lng: -73.6685625},
        icon: markerURL1,
        map: map
    });
}