<?php
$baliseTitre='h3';
?>
<div class="row markets_items" id="markets_items">
	<div class="col-12 px-0 d-flex markets_items_container" id="markets_items_container">
		<div class="py-5 item">
			<a href="<?php echo $pages['rapidprototyping']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-rapid-prototyping-thumb.jpg" width="498" height="532" alt="<?php echo $pages['rapidprototyping']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['rapidprototyping']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('rapidprototyping-home-txt'); ?></p>
					<a href="<?php echo $pages['rapidprototyping']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['waxinjection']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-wax-injection-thumb.jpg" width="498" height="532" alt="<?php echo $pages['waxinjection']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['waxinjection']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('waxinjection-home-txt'); ?></p>
					<a href="<?php echo $pages['waxinjection']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['moulding']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-moulding-thumb.jpg" width="498" height="532" alt="<?php echo $pages['moulding']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['moulding']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('moulding-home-txt'); ?></p>
					<a href="<?php echo $pages['moulding']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>		
		<div class="py-5 item">
			<a href="<?php echo $pages['alloys']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-alloys-thumb.jpg" width="498" height="532" alt="<?php echo $pages['alloys']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['alloys']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('alloys-home-txt'); ?></p>
					<a href="<?php echo $pages['alloys']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['titanium']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-titanium-thumb.jpg" width="498" height="532" alt="<?php Lang::write('titanium-nom'); ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['titanium']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('titanium-home-txt'); ?></p>
					<a href="<?php echo $pages['titanium']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['sizinghipping']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-sizing-hipping-thumb.jpg" width="498" height="532" alt="<?php echo $pages['sizinghipping']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['sizinghipping']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('sizinghipping-home-txt'); ?></p>
					<a href="<?php echo $pages['sizinghipping']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['toolroom']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-tool-room-thumb.jpg" width="498" height="532" alt="<?php echo $pages['toolroom']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['toolroom']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('toolroom-home-txt'); ?></p>
					<a href="<?php echo $pages['toolroom']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['heattreatment']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-heat-treatment-thumb.jpg" width="498" height="532" alt="<?php echo $pages['heattreatment']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['heattreatment']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('heattreatment-home-txt'); ?></p>
					<a href="<?php echo $pages['heattreatment']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['shipping']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/casting-shipping-thumb.jpg" width="498" height="532" alt="<?php Lang::write('shipping-nom'); ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('casting-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['shipping']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p><?php Lang::write('shipping-home-txt'); ?></p>
					<a href="<?php echo $pages['shipping']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>