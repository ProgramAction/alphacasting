<div class="details">
	<div class="row align-items-end bandeau">
		<div class="col-11 offset-1 pb-5">
			<h1><?php echo $pages[$pagesKey]['nom']; ?></h1>
		</div>
	</div>
	<?php include('../includes/breadcrumb.php'); ?>
	<div class="row pt-5 overflow-hidden">
		<div class="col-lg-6 col-xl-5 offset-xl-1 d-flex align-items-center pb-5">
			<div>
				<?php if ($s1_intro_pos=='up') { ?><p class="intro mb-0" data-aos="zoom-in"><?php Lang::write($pagesKey.'-s1-intro'); ?></p><?php } ?>
				<h2 data-aos="zoom-out" class="<?php if ($s1_intro_pos=='up') { ?>mb-4<?php } ?>"><strong><?php Lang::write($pagesKey.'-s1-h2'); ?></strong></h2>
				<?php if ($s1_intro_pos=='down') { ?><p class="intro" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s1-intro'); ?></p><?php } ?>
				<?php
					$d=0;
					foreach ($s1 as $s) {
						echo '<p data-aos="zoom-in" data-aos-delay="'.$d.'">'.$s.'</p>';
						//$d+=50;
					}
				?>
			</div>
		</div>
		<div class="col-lg-6 col-xl-5 pb-5">
			<img src="../assets/images/<?php echo $pages[$pagesKey]['s1-img']; ?>" alt="<?php Lang::write($pagesKey.'-s1-img-ALT'); ?>" class="img-fluid">
		</div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 mx-auto pb-5">
			<?php include('../assets/images/svg/alphacasting-outline.php'); ?>
		</div>
	</div>
	<div class="row s2">
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-black">
			<div class="px-lg-5 py-5 max-w600">
				<?php if ($s1_intro_pos=='up' && Lang::insert($pagesKey.'-s2-intro')!="") { ?><p class="intro mb-0" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s2-intro'); ?></p><?php } ?>
				<?php if (Lang::insert($pagesKey.'-s2-h2')!="") { ?><h2 data-aos="zoom-out" class="<?php if ($s1_intro_pos=='up') { ?>mb-4<?php } ?>"><strong><?php Lang::write($pagesKey.'-s2-h2'); ?></strong></h2><?php } ?>
				<?php if ($s1_intro_pos=='down' && Lang::insert($pagesKey.'-s2-intro')!="") { ?><p class="intro" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s2-intro'); ?></p><?php } ?>
				<p data-aos="zoom-in" data-aos-delay="50"><?php Lang::write($pagesKey.'-s2-p1'); ?></p>
			</div>
		</div>
		<div class="col-lg-6 bg-grispale d-flex align-items-center justify-content-center">
			<div class="px-lg-5 py-5">
				<img src="../assets/images/svg/<?php echo $pages[$pagesKey]['icon']['file']; ?>" width="<?php echo $pages[$pagesKey]['icon']['width']; ?>" role="presentation" alt="" class="img-fluid">
			</div>
		</div>
	</div>
	<div class="row s2">
		<div class="col-lg-6 px-0 order-2 order-lg-1">
			<img src="../assets/images/<?php echo $pages[$pagesKey]['s2-img']; ?>" alt="<?php Lang::write($pagesKey.'-s2-img-ALT'); ?>" class="img-fluid objfitcover">
		</div>
		<div class="col-lg-6 d-flex align-items-center justify-content-center bg-rouge order-1 order-lg-2">
			<div class="px-lg-5 py-5 max-w600" data-aos="zoom-in">
				<?php Lang::write($pagesKey.'-s2-p2'); ?>
			</div>
		</div>
	</div>
	<div class="row py-5 overflow-hidden s3">
		<div class="col-xl-10 offset-xl-1 py-5">
			<?php if ($s3_intro_pos=='up' && Lang::insert($pagesKey.'-s3-intro')!="") { ?><p class="intro mb-0" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s3-intro'); ?></p><?php } ?>
			<?php if (Lang::insert($pagesKey.'-s3-h2')!="") { ?><h2 data-aos="zoom-out" class="<?php if ($s1_intro_pos=='up') { ?>mb-4<?php } ?>"><strong><?php Lang::write($pagesKey.'-s3-h2'); ?></strong></h2><?php } ?>
			<?php if ($s3_intro_pos=='down' && Lang::insert($pagesKey.'-s3-intro')!="") { ?><p class="intro" data-aos="zoom-in" data-aos-delay="25"><?php Lang::write($pagesKey.'-s3-intro'); ?></p><?php } ?>
			<?php
				$d=0;
				foreach ($s3 as $s) {
					echo '<p data-aos="zoom-in" data-aos-delay="'.$d.'">'.$s.'</p>';
					//$d+=50;
				}
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-12 px-0">
			<img src="../assets/images/<?php echo $pages[$pagesKey]['img-full']; ?>" width="1920" height="778" alt="<?php Lang::write($pagesKey.'-img-full-ALT'); ?>" class="img-fluid w-100">
		</div>
	</div>
	<?php if (count($features)>0) { ?>
	<div class="row my-1">
		<?php
		foreach ($features as $f) {
		?>
		<div class="col-12 features">
			<div class="row align-items-center py-5 titre">
				<div class="col-9 col-sm-10 col-xl-8 offset-xl-1">
					<h2><?php echo $f['titre']; ?></h2>
					<?php if ($f['p']!="") { ?><p><?php echo $f['p']; ?></p><?php } ?>
				</div>
				<div class="col-3 col-sm-2 text-right">
					<?php include('../assets/images/svg/fleche-rond-rouge.php'); ?>
				</div>
			</div>
			<div class="row align-items-center details">
				<div class="col-xl-10 offset-xl-1 pb-5">
					<?php echo $f['details']; ?>
					<p class="pt-4">
						<a href="<?php echo $pages['contact']['url']; ?>" class="btn noir"><?php Lang::write('cta-contact'); ?></a>
					</p>
				</div>
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<?php } ?>
	<?php include('cta_template.php'); ?>
	<?php include('../includes/swiper_casting.php'); ?>
</div>