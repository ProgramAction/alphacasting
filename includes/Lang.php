<?php

class Lang {

    public static $textes = null;
    public static $lang = 'FR';


    // ------------------------------
    // ------------------------------
    // fonction : à spécifier
    // ------------------------------
    // ------------------------------
    
    public static function textes(){
        if(Self::$textes == null) {
            $text_pool = [];
            foreach (glob(__dir__."/Locales/*.texte.json") as $fichier_json) {
                $txts = json_decode(file_get_contents($fichier_json),true);
                foreach($txts as $texte => $val){
                    $text_pool[$texte] = $val;
                }
            }
            Self::$textes = $text_pool;
        }
        return Self::$textes;
    }


    // ------------------------------
    // ------------------------------
    // fonction : à spécifier
    // ------------------------------
    // ------------------------------
    
    public static function write($txt_key,$options = []){
        $lang = (isset($options['lang']))?$options['lang']:Self::$lang;
        if(isset(Self::textes()[$txt_key])){
            if(in_array('include',$options)){
                return Self::textes()[$txt_key][$lang];
            }else {
                echo Self::textes()[$txt_key][$lang];
            }
        }else {
            if(in_array('include',$options)){
                return 'Lang::error';
            }else {
                echo 'Lang::error';
            }
        }
        
    }

    public static function insert($txt_key,$options = [] ) {
        $lang = (isset($options['lang']))?$options['lang']:Self::$lang;
        return ((isset(Self::textes()[$txt_key]))?Self::textes()[$txt_key][$lang]:'Lang::error');
    }

}