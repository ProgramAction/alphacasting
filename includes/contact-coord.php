<div class="row coord border-bottom rouge">
	<div class="col-xl-10 offset-xl-1">
		<div class="row">
			<div class="col-lg-6 py-5 phone border-right rouge">
				<div class="pl-lg-5 pl-xl-0 pr-lg-5">
					<img src="<?php echo $assetsPath; ?>images/logoalphanoir.svg" width="485" alt="Logo Alphacasting" class="img-fluid mb-4">
					<span class="leading"><?php Lang::write('f-slogan-leading'); ?></span>
					<p class="fenix stitre mb-2 mt-4"><?php Lang::write('f-phone'); ?></h2>
					<p class="mb-2">T <a href="tel:+15147487511" class="mr-3">514 748-7511</a>&nbsp; <?php Lang::write('f-phone-sf'); ?> <a href="tel:18005677511">1 800 567-7511</a></p>
					<p>F <a href="fax:+15147480237">514 748-0237</a></p>
				</div>
			</div>
			<div class="col-lg-6 col-xl-5 offset-xl-1 pt-lg-5 pb-5 address">
				<div class="pl-lg-5 pl-xl-0">
					<h2 class="fenix mb-0"><?php Lang::write('contact-address'); ?></h2>
					<ul class="list-unstyled mt-4 d-flex flex-wrap flex-xl-nowrap">
						<li class="mr-5 mr-lg-4 mr-xl-5 text-nowrap pb-4">
							<p class="fenix stitre mb-1">Canada</p>
							<p>391, Sainte-Croix <br>Saint-Laurent (Québec)  <br>H4N 2L3</p>
						</li>
						<li class="text-nowrap pb-4">
							<p class="fenix stitre mb-1">USA</p>
							<p>151, New Park Avenue <br>Hartford <br>Connecticut, 06106</p>
						</li>
					</ul>
					<div class="d-flex rs pt-3">
						<a href="<?php echo $urlFB; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-facebook.png" width="14" height="29" alt="Facebook"></a>
						<a href="<?php echo $urlLinkedin; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-linkedin.png" width="28" height="29" alt="Linkedin"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>