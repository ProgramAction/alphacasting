<?php
$sujet='';
if (isset($_GET['sujet'])) {
	$sujet=urldecode($_GET['sujet']);
}
?>
<div class="row py-5 bg-grispale" id="nousjoindre">
	<div id="form" class="col-lg-6 col-xl-5 offset-xl-1 pb-lg-5 pt-5">
		<h2 class="mb-5"><?php Lang::write('contact-titreform'); ?></h2>
		<div class="contact-form">
			<form id="cf" name="cf" action="../includes/sendemail.php" method="post" autocomplete="off" novalidate="novalidate" enctype="multipart/form-data">
				<div class="form-process"></div>
				<div class="form-group">
					<label for="cf-name"><?php Lang::write('c-form-Name'); ?></label>
					<input type="text" id="cf-name" name="cf-name" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-cie"><?php Lang::write('c-form-Company'); ?></label>
					<input type="text" id="cf-cie" name="cf-cie" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-email"><?php Lang::write('c-form-Email'); ?></label>
					<input type="email" id="cf-email" name="cf-email" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-phone"><?php Lang::write('c-form-Telephone'); ?></label>
					<input type="text" id="cf-phone" name="cf-phone" placeholder="" class="form-control rounded-0">
				</div>

				<div class="form-group d-none">
					<label for="website">Website</label>
					<input id="website" name="website" type="text" value="" placeholder="website"  />
				</div>

				<div class="form-group">
					<label for="cf-message"><?php Lang::write('c-form-message'); ?></label>
					<textarea name="cf-message" id="cf-message" placeholder="" class="form-control rounded-0 required" rows="7"></textarea>
				</div>
				<div class="form-group mb-4">
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="cf-fichier" name="cf-fichier" lang="<?php echo strtolower($lang); ?>">
						<label class="custom-file-label" for="cf-fichier"><?php Lang::write('c-form-fichier'); ?></label>
					</div>
				</div>
				<div class="mb-4">
					<div class="g-recaptcha" data-sitekey="6LfGHAATAAAAAAYWE7uAA4x_wHcla3xwcML9lyXZ"></div>
				</div>
				<div class="form-group">
					<button class="btn btn-black" type="submit" id="cf-submit" name="cf-submit"><?php Lang::write('c-form-Send'); ?></button>
				</div>
				<input type="hidden" name="form" id="form" value="contact">
				<input type="hidden" name="lang" value="<?php echo $lang; ?>">
				<input type="hidden" name="assetsPath" value="<?php echo $assetsPath; ?>">
				<input type="hidden" name="sujetRequete" id="sujetRequete" value="Requête site web">
			</form>
			<div class="cf-form-result mt-4"></div>
		</div>
	</div>
	<div class="col-lg-5 offset-lg-1 contacts pb-lg-5 pt-5">
		<div>
			<h2 class="mb-5">Contacts</h2>
			<p class="mb-0 text-uppercase"><strong>Frederik Centazzo</strong></p>
			<p class="mb-0"><?php Lang::write('contact-jobtitle-1'); ?></p>
			<p class="mb-0"><script>writeMail("fcentazzo@alphacasting.com","fcentazzo");</script></p>
			<p class="mb-5">Ext. 236</p>
			<p class="mb-0 text-uppercase"><strong>Raymond Bonin</strong></p>
			<p class="mb-0"><?php Lang::write('contact-jobtitle-2'); ?></p>
			<p class="mb-0"><script>writeMail("rbonin@alphacasting.com","rbonin");</script></p>
			<p class="mb-5">Ext. 224</p>
			<p class="mb-0 text-uppercase"><strong>Joe Khouzam</strong></p>
			<p class="mb-0"><?php Lang::write('contact-jobtitle-3'); ?></p>
			<p class="mb-0"><script>writeMail("jkhouzam@alphacasting.com","jkhouzam");</script></p>
			<p class="mb-0">Ext. 334</p>
		</div>
	</div>
</div>