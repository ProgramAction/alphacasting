<div class="row py-5 bg-black">
	<div class="swiper-container py-4 markets">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<a href="<?php echo $pages['rapidprototyping']['url']; ?>">
					<span><?php echo $pages['rapidprototyping']['nom']; ?></span>
					<img src="../assets/images/casting-rapid-prototyping-thumb.jpg" alt="<?php echo $pages['rapidprototyping']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['waxinjection']['url']; ?>">
					<span><?php echo $pages['waxinjection']['nom']; ?></span>
					<img src="../assets/images/casting-wax-injection-thumb.jpg" alt="<?php echo $pages['waxinjection']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['moulding']['url']; ?>">
					<span><?php echo $pages['moulding']['nom']; ?></span>
					<img src="../assets/images/casting-moulding-thumb.jpg" alt="<?php echo $pages['moulding']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['alloys']['url']; ?>">
					<span><?php echo $pages['alloys']['nom']; ?></span>
					<img src="../assets/images/casting-alloys-thumb.jpg" alt="<?php echo $pages['alloys']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['titanium']['url']; ?>">
					<span><?php echo $pages['titanium']['nom']; ?></span>
					<img src="../assets/images/casting-titanium-thumb.jpg" alt="<?php echo $pages['titanium']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['sizinghipping']['url']; ?>">
					<span><?php echo $pages['sizinghipping']['nom']; ?></span>
					<img src="../assets/images/casting-sizing-hipping-thumb.jpg" alt="<?php echo $pages['sizinghipping']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['toolroom']['url']; ?>">
					<span><?php echo $pages['toolroom']['nom']; ?></span>
					<img src="../assets/images/casting-tool-room-thumb.jpg" alt="<?php echo $pages['toolroom']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['heattreatment']['url']; ?>">
					<span><?php echo $pages['heattreatment']['nom']; ?></span>
					<img src="../assets/images/casting-heat-treatment-thumb.jpg" alt="<?php echo $pages['heattreatment']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['shipping']['url']; ?>">
					<span><?php echo $pages['shipping']['nom']; ?></span>
					<img src="../assets/images/casting-shipping-thumb.jpg" alt="<?php echo $pages['shipping']['nom']; ?>" class="img-fluid w-100">
				</a>
			</div>
		</div>
	</div>
</div>