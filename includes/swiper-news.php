<div class="row bg-black py-5">
            <div class="col-12 swiper swiper-news">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <figure>
                            <img src="<?php echo $assetsPath ?>images/swiper-news/swiper-news-1.jpg" alt="">
                        </figure>
                    </div>
                    <div class="swiper-slide">
                        <figure>
                            <img src="<?php echo $assetsPath ?>images/swiper-news/swiper-news-2.jpg" alt="">
                        </figure>
                    </div>
                    <div class="swiper-slide">
                        <figure>
                            <img src="<?php echo $assetsPath ?>images/swiper-news/swiper-news-3.jpg" alt="">
                        </figure>
                    </div>
                    <div class="swiper-slide">
                        <figure>
                            <img src="<?php echo $assetsPath ?>images/swiper-news/swiper-news-4.jpg" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>