<?php
require 'PHPMailer/PHPMailer.php';
use PHPMailer\PHPMailer\PHPMailer;

function slugify($text,$strict = true) {
	$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
	// replace non letter or digits by -
	$text = preg_replace('~[^\\pL\d.]+~u', '-', $text);

	// trim
	$text = trim($text, '-');
	setlocale(LC_CTYPE, 'en_GB.utf8');
	// transliterate
		if (function_exists('iconv')) {
	   $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	}
	// lowercase
	$text = strtolower($text);
	// remove unwanted characters
	$text = preg_replace('~[^-\w.]+~', '', $text);
	if (empty($text)) {
	   return 'empty_$';
	}
	if ($strict) {
		$text = str_replace(".", "", $text);
	}
	return $text;
}

$toemails = array();

// Form Processing Messages
$message_success = 'We have successfully received your Message and will get Back to you as soon as possible.';
$lang='en';
$mail = new PHPMailer();
if(isset($_POST['lang'])){
	$lang = strtolower($_POST['lang']);
}
if(isset($_POST['assetsPath'])){
	$assetsPath = strtolower($_POST['assetsPath']);
}

$txt = [
	['en'=>'We have successfully received your Message and will get Back to you as soon as possible.','fr'=>'Merci <br>Votre message a bien été envoyé. Nous communiquerons avec vous dans les meilleurs délais.'],
	['en'=>'Email could not be sent due to some Unexpected Error. Please Try Again later.<br /><br />Reason:<br />','fr'=>'Une erreur s\'est produite. Votre message n\'a pas été envoyé. <br /><br />Raison:<br />'],
	['en'=>'Bot Detected.! Clean yourself Botster.!','fr'=>'Script détecté.'],
	['en'=>'Please Fill up all the Fields and Try Again.','fr'=>'Remplissez tout les champs et réessayez.'],
	['en'=>'An unexpected error occured. Please Try Again later.','fr'=>'Une erreur s\'est produite. Veuillez réessayer plus tard.']
];

// If you intend you use SMTP, add your SMTP Code after this Line
$erreurs=array();
if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if( $_POST['cf-email'] != '' ) {
		$sujetRequete = isset( $_POST['sujetRequete'] ) ? trim($_POST['sujetRequete']) : '';
		$name = isset( $_POST['cf-name'] ) ? trim($_POST['cf-name']) : '';
		$email = isset( $_POST['cf-email'] ) ? trim($_POST['cf-email']) : '';
		$phone = isset( $_POST['cf-phone'] ) ? trim($_POST['cf-phone']) : '';
		$cie = isset( $_POST['cf-cie'] ) ? trim($_POST['cf-cie']) : '';
		
		$titre = isset( $_POST['cf-titre'] ) ? trim($_POST['cf-titre']) : '';
		$adr = isset( $_POST['cf-adr'] ) ? trim($_POST['cf-adr']) : '';
		$ville = isset( $_POST['cf-ville'] ) ? trim($_POST['cf-ville']) : '';
		$state = isset( $_POST['cf-state'] ) ? trim($_POST['cf-state']) : '';
		$zip = isset( $_POST['cf-zip'] ) ? trim($_POST['cf-zip']) : '';
		$pays = isset( $_POST['cf-pays'] ) ? trim($_POST['cf-pays']) : '';
		$fax = isset( $_POST['cf-fax'] ) ? trim($_POST['cf-fax']) : '';
		$formatmanuel = isset( $_POST['cf-format'] ) ? trim($_POST['cf-format']) : '';		
		
		if ($_POST['form']=='contact') {
			$message = isset( $_POST['cf-message'] ) ? trim($_POST['cf-message']) : '';
			if (empty($message)) {
				$erreurs['message']=1;
			}
		}
        
		/* SPAMS HONEY POT */
		if ($_POST['website']!="") {
			$erreurs['website']=1;
		}
		
        if (isset($_FILES['cf-fichier'])) {
            if(is_uploaded_file($_FILES['cf-fichier']['tmp_name'])){
                $fichierTmp = $_FILES['cf-fichier']['tmp_name'];
                $mime_type = $_FILES['cf-fichier']['type'];

                $type_file = strrchr($_FILES['cf-fichier']['name'], '.');
                
                $extensionsFichiers = array('.doc', '.docx', '.xls', '.xlsx', '.pdf');
                if(!in_array(strtolower($type_file), $extensionsFichiers)) { //Si l'extension n'est pas dans le tableau
                    $erreurs['fichier'] = "Format de fichier non valide.";
                } else {
                    $fichier = slugify(stripslashes(substr($_FILES['cf-fichier']['name'],0,strlen($_FILES['cf-fichier']['name'])-4))).$type_file;
                    $fichierfilePath = '../assets/upload/'.$fichier;

                    if( !move_uploaded_file($fichierTmp, $fichierfilePath)) {
                        $erreurs['fichier'] = "Problème de transfert de fichier.";
                    }
                }
            }
        }

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => '6LfGHAATAAAAAIaBAVzTwkpk1GIDfH7hVLeRfFHW',
            'response' => $_POST["g-recaptcha-response"]
        );
        $options = array(
            'http' => array (
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success=json_decode($verify);
        if (!$captcha_success->success) {
            $erreurs['recaptcha']=1;
        }
        
		if(count($erreurs)==0) {
			if ($_POST['form']=='contact') {
				$toemails[] = array(
					'email' => 'fcentazzo@alphacasting.com',
					'name' => 'Alphacasting' // Your Name
				);
			} elseif ($_POST['form']=='quality') {
				$toemails[] = array(
					'email' => 'fcentazzo@alphacasting.com',
					'name' => 'Alphacasting' // Your Name
				);
			}
			
			$mail->CharSet = 'UTF-8';
			$mail->SetFrom( $email , $name );
			$mail->AddReplyTo( $email , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}
			$mail->Subject = $sujetRequete;

			if ($_POST['form']=='contact') {
				$name = isset($name) ? "Nom : $name<br><br>" : '';
				$email = isset($email) ? "Courriel : $email<br><br>" : '';
				$phone = isset($phone) ? "Téléphone : $phone<br><br>" : '';
				if ($cie!="") {
					$cie = isset($cie) ? "Entreprise : $cie<br><br>" : '';
				}
				$message = isset($message) ? "Détails : $message<br><br>" : '';

				$body = "$name $email $phone $cie $message";
			} elseif ($_POST['form']=='quality') {
				$name = isset($name) ? "Nom : $name<br><br>" : '';
				$cie = isset($cie) ? "Entreprise : $cie<br><br>" : '';
				$titre = isset($titre) ? "Titre : $titre<br><br>" : '';
				$adr = isset($adr) ? "Adresse : $adr<br><br>" : '';
				$ville = isset($ville) ? "Ville : $ville<br><br>" : '';
				$state = isset($state) ? "Province/état : $state<br><br>" : '';
				$zip = isset($zip) ? "Code postal/zip : $zip<br><br>" : '';
				$pays = isset($pays) ? "Pays : $pays<br><br>" : '';
				$phone = isset($phone) ? "Téléphone : $phone<br><br>" : '';
				$fax = isset($fax) ? "Fax : $fax<br><br>" : '';
				$email = isset($email) ? "Courriel : $email<br><br>" : '';
				$format = isset($format) ? "Format : $format<br><br>" : '';

				$body = "$name $cie $titre $adr $ville $state $zip $pays $phone $fax $email $format";
			}

			$mail->MsgHTML( $body );
            
            if (isset($fichierfilePath)) {
				$mail->AddAttachment($fichierfilePath);
			}
            
			$sendEmail = $mail->Send();

            if (isset($fichierfilePath)) {
				unlink($fichierfilePath);
			}
            
			if( $sendEmail == true ):
				echo '{ "alert": "success", "message": "'.$txt[0][$lang].'" }';
			else:
				echo '{ "alert": "error", "message": "'.$txt[1][$lang]. $mail->ErrorInfo . '" }';
			endif;
		} else {
			echo '{ "alert": "error", "message": "'.$txt[2][$lang].'" }';
		}
	} else {
		echo '{ "alert": "error", "message": "'.$txt[3][$lang].'" }';
	}
} else {
	echo '{ "alert": "error", "message": "'.$txt[4][$lang].'" }';
}

?>