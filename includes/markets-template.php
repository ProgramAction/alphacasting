<div class="details">
	<div class="row align-items-end bandeau">
		<div class="col-11 offset-1 pb-5">
			<h1><?php echo $pages[$pagesKey]['nom']; ?></h1>
		</div>
	</div>
	<?php include('../includes/breadcrumb.php'); ?>
	<div class="row align-items-end pt-5 video">
		<div class="col-lg-6 col-xl-5 offset-xl-1">
			<p class="mb-4"><?php Lang::write($pagesKey.'-video-p'); ?></p>
		</div>
		<div class="col-lg-6 col-xl-5">
			<video autoplay loop muted poster="<?php echo $assetsPath; ?>images/videos/<?php echo strtolower($pages[$pagesKey]['video']); ?>.jpg">
				<source src="<?php echo $assetsPath; ?>videos/<?php echo strtolower($pages[$pagesKey]['video']); ?>.mp4?v=2" type="video/mp4">
				<source src="<?php echo $assetsPath; ?>videos/<?php echo strtolower($pages[$pagesKey]['video']); ?>.webm?v=2" type="video/webm">
				<?php Lang::write('cta-novideo'); ?>
			</video>
		</div>
		<div class="col-xl-10 mx-auto mt-2"><div class="border-bottom rouge"></div></div>
	</div>
	<div class="row py-5">
		<div class="col-xl-10 offset-xl-1">
			<div class="row align-items-end">
				<div class="col-lg-6 mb-1">
					<div class="pr-xl-5">
						<div class="diag-line-bar"></div>
					</div>
				</div>
				<div class="col-lg-6">
					<h2 data-aos="zoom-in"><?php Lang::write($pagesKey.'-h2-1'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="row pt-lg-5 pb-5">
		<div class="col-xl-10 offset-xl-1">
			<div class="row align-items-end">
				<div class="col-lg-5 col-xl-5 pb-5">
					<?php
					$d=0;
					foreach ($mainP as $p) {
						echo '<p data-aos="zoom-in" data-aos-delay="'.$d.'">'.$p.'</p>';
						$d+=50;
					}
					?>
				</div>
				<div class="col-lg-6 offset-lg-1 d-flex flex-column pb-5">
					<div><img src="../assets/images/<?php Lang::write($pagesKey.'-img-model'); ?>" alt="<?php Lang::write($pagesKey.'-img-model-ALT'); ?>" class="img-fluid mb-5"></div>
					<p class="outline mt-auto pt-5 text-center text-lg-left"><?php Lang::write($pagesKey.'-outline'); ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row py-5 parts">
		<div class="container pb-5 pt-0 pt-md-5">
			<div class="row justify-content-center justify-content-sm-between position-relative flex-md-nowrap mb-0 mb-lg-5">
				<div class="hor_rouge" data-aos="anim-hor_rouge"></div>
				<div class="col mb-5 mb-md-0">
					<div class="item p-5" data-aos="anim-border">
						<img src="../assets/images/<?php Lang::write($pagesKey.'-img-part-1'); ?>" alt="<?php Lang::write($pagesKey.'-img-part-1-ALT'); ?>" class="img-fluid">
					</div>
				</div>
				<div class="col mb-5 mb-md-0">
					<div class="item p-5" data-aos="anim-border" data-aos-delay="1500">
						<img src="../assets/images/<?php Lang::write($pagesKey.'-img-part-2'); ?>" alt="<?php Lang::write($pagesKey.'-img-part-2-ALT'); ?>" class="img-fluid">
					</div>
				</div>
				<div class="col mb-5 mb-md-0">
					<div class="item p-5" data-aos="anim-border" >
						<img src="../assets/images/<?php Lang::write($pagesKey.'-img-part-3'); ?>" alt="<?php Lang::write($pagesKey.'-img-part-3-ALT'); ?>" class="img-fluid">
					</div>
				</div>
				<div class="col mb-5 mb-md-0">
					<div class="item p-5" data-aos="anim-border">
						<img src="../assets/images/<?php Lang::write($pagesKey.'-img-part-4'); ?>" alt="<?php Lang::write($pagesKey.'-img-part-4-ALT'); ?>" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-10 offset-xl-1 pt-0 pt-lg-5 pb-5">
			<div class="row align-items-center">
				<div class="col-lg-6 col-xl-5 offset-lg-6">
					<p><?php Lang::write($pagesKey.'-part-p'); ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-10 offset-xl-1">
			<div class="row align-items-center">
				<div class="col-lg-6 col-xl-5 img-vertical">
					<img src="../assets/images/<?php Lang::write($pagesKey.'-img-vertical'); ?>" alt="<?php Lang::write($pagesKey.'-img-vertical-ALT'); ?>" class="img-fluid"  data-aos="slide-up" data-aos-duration="500">
				</div>
				<div class="col-lg-6 col-xl-5 offset-xl-1 py-5">
					<h3 data-aos="zoom-in"><strong><?php Lang::write($pagesKey.'-img-vertical-h3-1'); ?></strong></h3>
					<?php Lang::write($pagesKey.'-img-vertical-p-1'); ?>
					<p data-aos="zoom-in" data-aos-delay="50">
						<a href="<?php echo $pages['contact']['url']; ?>" class="btn noir"><?php Lang::write('cta-'.$pagesKey.'-contact-button'); ?></a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php if (count($horizontal)>0) { ?>
	<div class="col-xl-10 offset-xl-1 horizontal py-5">
	<?php foreach ($horizontal as $h) { ?>
		<div class="py-5">
			<h3 data-aos="zoom-in"><strong><?php echo $h['titre']; ?></strong></h3>
			<?php echo $h['details']; ?>
		</div>
	<?php } ?>
	</div>
	<?php } ?>
	<div class="row py-5">
		<div class="col-xl-10 mx-auto py-5">
			<?php include('../assets/images/svg/alphacasting-outline.php'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-12 px-0">
			<img src="../assets/images/<?php Lang::write($pagesKey.'-img-full'); ?>" width="1920" height="779" alt="<?php Lang::write($pagesKey.'-img-full-ALT'); ?>" class="img-fluid w-100">
		</div>
	</div>
	<?php if (count($features)>0) { ?>
	<div class="row my-1">
		<?php
		foreach ($features as $f) {
		?>
		<div class="col-12 features">
			<div class="row align-items-center py-5 titre">
				<div class="col-9 col-sm-10 col-xl-8 offset-xl-1">
					<h3><strong><?php echo $f['titre']; ?></strong></h3>
				</div>
				<div class="col-3 col-sm-2 text-right">
					<?php include('../assets/images/svg/fleche-rond-rouge.php'); ?>
				</div>
			</div>
			<div class="row align-items-center details">
				<div class="col-xl-10 offset-xl-1 pb-5">
					<?php echo $f['details']; ?>
					<p class="pt-4">
						<a href="<?php echo $pages['contact']['url']; ?>" class="btn noir"><?php Lang::write('cta-contact'); ?></a>
					</p>
				</div>
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<?php } ?>
	<?php include('cta_template.php'); ?>
	<?php include('../includes/swiper_markets.php'); ?>
</div>