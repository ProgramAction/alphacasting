<?php include('../includes/ga.php'); ?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $pageTitre; ?></title>
<meta name="description" content="<?php echo $pageDesc; ?>">

<?php if ($_SERVER['SERVER_NAME']=="localhost" || $_SERVER['SERVER_NAME']=="www.localhost" || $_SERVER['SERVER_NAME']=="alphacasting.lan"): ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $assetsPath; ?>css/style.css?v=<?php echo strtotime('now') ?>">
<?php else: ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $assetsPath; ?>css/style.css?v=<?php echo $FilesVersion; ?>">
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $assetsPath; ?>css/aos.css">
<link rel="stylesheet" type="text/css" href="<?php echo $assetsPath; ?>css/swiper-bundle.min.css">
<link rel="icon" href="../favicon.ico" />
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/mail.js"></script>
<script src="https://www.google.com/recaptcha/api.js?hl=<?php echo strtolower($lang) ?>" async defer></script>
<?php if ($pagesKey=='home') { ?>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Alphacasting inc.",
  "image" : "https://www.alphacasting.com/images/logo-alphacasting.png",
  "telephone" : "(514) 748-7511",
  "email" : "fcentazzo@alphacasting.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "391 Sainte-Croix",
    "addressLocality" : "Saint-Laurent",
    "addressRegion" : "QC",
    "addressCountry" : "Canada",
    "postalCode" : "H4N 2L3"
  },
  "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "(514) 748-7511",
    "contactType": "customer service",
    "areaServed": [
      "CA","US"
    ],
    "availableLanguage": [
      "English",
      "French"
    ]
  },{
    "@type": "ContactPoint",
    "telephone": "(800) 567-7511",
    "contactType": "customer service",
    "areaServed": [
      "CA","US"
    ],
    "availableLanguage": [
      "English",
      "French"
    ]
  }],
  "sameAs": [
    "https://www.facebook.com/Alphacasting/",
    "https://www.linkedin.com/company/alphacasting-mtl"
  ]
}
</script>
<?php } ?>