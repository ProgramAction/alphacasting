<?php
$baliseTitre='h3';
if ($pagesKey=='markets') {
	$baliseTitre='h2';
}
?>
<div class="row markets_items" id="markets_items">
	<div class="col-12 px-0 d-flex markets_items_container" id="markets_items_container">
		<div class="py-5 item">
			<a href="<?php echo $pages['aerospace']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/aerospace.jpg" width="752" height="536" alt="<?php echo $pages['aerospace']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['aerospace']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('aerospace-home-titre'); ?></p>
					<p><?php Lang::write('aerospace-home-txt'); ?></p>
					<a href="<?php echo $pages['aerospace']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['medical']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/medical.jpg" width="752" height="536" alt="<?php echo $pages['medical']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['medical']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('medical-home-titre'); ?></p>
					<p><?php Lang::write('medical-home-txt'); ?></p>
					<a href="<?php echo $pages['medical']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['defence']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/defence.jpg" width="752" height="536" alt="<?php echo $pages['defence']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['defence']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('defence-home-titre'); ?></p>
					<p><?php Lang::write('defence-home-txt'); ?></p>
					<a href="<?php echo $pages['defence']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['telecom']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/telecom.jpg" width="752" height="536" alt="<?php echo $pages['telecom']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['telecom']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('telecom-home-titre'); ?></p>
					<p><?php Lang::write('telecom-home-txt'); ?></p>
					<a href="<?php echo $pages['telecom']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['transport']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/transport.jpg" width="752" height="536" alt="<?php echo $pages['transport']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['transport']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('transport-home-titre'); ?></p>
					<p><?php Lang::write('transport-home-txt'); ?></p>
					<a href="<?php echo $pages['transport']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
		<div class="py-5 item">
			<a href="<?php echo $pages['commercial']['url']; ?>"><img src="<?php echo $assetsPath; ?>images/commercial.jpg" width="752" height="536" alt="<?php echo $pages['commercial']['nom']; ?>" class="img-fluid"></a>
			<div class="row">
				<div class="col-2 pr-0"><hr></div>
				<div class="col-10">
					<p class="titre_markets"><?php Lang::write('markets-nom'); ?></p>
					<<?php echo $baliseTitre; ?>><?php echo $pages['commercial']['nom']; ?></<?php echo $baliseTitre; ?>>
					<p class="intro"><?php Lang::write('commercial-home-titre'); ?></p>
					<p><?php Lang::write('commercial-home-txt'); ?></p>
					<a href="<?php echo $pages['commercial']['url']; ?>" class="btn btn-black"><?php Lang::write('cta-learmore'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>