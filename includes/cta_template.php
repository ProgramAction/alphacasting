<div class="row pb-5 cta bg-yellow align-items-center">
	<div class="col-lg-8 offset-xl-1 pt-5 text-center text-lg-left">
		<p><?php Lang::write('cta-'.$pagesKey.'-contact'); ?></p>
	</div>
	<div class="col-lg-4 col-xl-2 pt-5 text-center text-xl-right">
		<a href="<?php echo $pages['contact']['url']; ?>" class="btn noir"><?php Lang::write('cta-'.$pagesKey.'-contact-button'); ?></a>
	</div>
</div>