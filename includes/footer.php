	</main>
	<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-lg-4 bgblanc d-flex justify-content-center">
					<div class="pb-5 px-5 d-flex flex-column">
						<img src="<?php echo $assetsPath; ?>images/logoalphanoir.svg" width="226" height="23" alt="Logo Alphacasting" class="img-fluid mb-4">
						<span class="leading"><?php Lang::write('f-slogan-leading'); ?></span>
						<div class="d-flex rs pt-5">
							<a href="<?php echo $urlFB; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-facebook.png" width="14" height="29" alt="Facebook"></a>
							<a href="<?php echo $urlLinkedin; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-linkedin.png" width="28" height="29" alt="Linkedin"></a>
							<!--<a href="<?php echo $urlInstagram; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-instagram.png" width="29" height="29" alt="Instagram"></a>-->
						</div>
						<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41.74 27.08" class="plane"><polygon class="plane-red" points="28.43 6 29.43 4.56 25.74 3.97 25.5 4.37 27.28 6.09 28.43 6"/><path class="plane-red" d="M34.69,20.9l2.22-.64,3.34-3.42a9.34,9.34,0,0,1,1.06-1,7,7,0,0,1,.65-.53l.88-.62a7.58,7.58,0,0,1,2-1,2.63,2.63,0,0,0-1.19-.28A5.92,5.92,0,0,0,40.2,14.9a17.78,17.78,0,0,0-3,2.78l-1.68,2A9.19,9.19,0,0,0,34.69,20.9Z" transform="translate(-6.13 -13.46)"/><path class="plane-red" d="M24.2,25.3l8.17-.42,4.15-4.05L24.9,24.08a1.88,1.88,0,0,0-.67.39,1,1,0,0,0-.25.63C24,25.2,24.05,25.26,24.2,25.3Z" transform="translate(-6.13 -13.46)"/><path class="plane-red" d="M42.45,21.35s1.69-2.09,2.33-2.93A12.59,12.59,0,0,0,46,17a5.55,5.55,0,0,0,.75-1.34h0a1.42,1.42,0,0,0,.11-.5,1.11,1.11,0,0,0-1.24-1.27,3.9,3.9,0,0,0-1.8.66,19.88,19.88,0,0,0-3.21,2.84l-2.66,2.69S28,30.24,23.41,34.1C15.59,40.64,6.31,40,6.7,28.79c0,0-2,5.5,1.59,9.05,5.18,5.08,12,1.82,15.69-.53A83,83,0,0,0,38.88,25.2l7.6-.22a1.37,1.37,0,0,0,1.19-.9l.2-.37Z" transform="translate(-6.13 -13.46)"/><path class="plane-bottom" d="M29.64,33.35a82.43,82.43,0,0,0,9.24-8.15l7.6-.22a1.37,1.37,0,0,0,1.19-.9l.2-.37-5.42-2.36s1.69-2.09,2.33-2.93A12.59,12.59,0,0,0,46,17a5.55,5.55,0,0,0,.75-1.34h0a1.42,1.42,0,0,0,.11-.5,1.11,1.11,0,0,0-1.24-1.27,3.9,3.9,0,0,0-1.8.66,19.88,19.88,0,0,0-3.21,2.84l-2.66,2.69s-6,6.16-10.85,10.71C28,31.65,28.83,32.51,29.64,33.35Z" transform="translate(-6.13 -13.46)"/><path class="plane-bottom" d="M23.41,34.1c-3.25,2.71-6.74,4.19-9.7,4.19-4.16,0-7.24-2.94-7-9.5,0,0-2,5.5,1.59,9.05a9,9,0,0,0,6.49,2.69c3.5,0,6.93-1.78,9.2-3.22.64-.41,1.25-.82,1.86-1.23-.64-.73-1.33-1.5-2.08-2.29Z" transform="translate(-6.13 -13.46)"/><path class="plane-tail-end" d="M14,21.88c-2.48-.16-6.08,2.45-6.4,5.3,3.82-4.55,10.88,1.05,15.94,6.33,1-.81,2.09-1.84,3.3-3C22.25,26.2,17.11,22.08,14,21.88Z" transform="translate(-6.13 -13.46)"/><path class="plane-tail-end" d="M29.3,40.38a.35.35,0,0,0,.3.16h5.57c.34,0,.52-.48.28-.78-1-1.22-3-3.56-5.55-6.15-1.25,1-2.52,1.87-3.82,2.74C27.76,38.31,29,39.88,29.3,40.38Z" transform="translate(-6.13 -13.46)"/><polygon class="plane-red" points="24.78 8.5 28.14 7.56 24.94 4.42 25.36 3.68 19.74 5.43 18.84 7.11 23.85 6.52 24.78 8.5"/></svg>
					</div>
				</div>
				<div class="col-md-6 col-lg-8 bggris d-flex justify-content-center">
					<div class="d-flex flex-wrap pb-5 coords">
						<div class="coord">
							<p><?php Lang::write('f-phone'); ?></p>
							<p class="mb-1"><a href="tel:+15147487511">514 748-7511</a></p>
							<p><a href="tel:18005677511">1 800 567-7511</a></p>
						</div>
						<div class="coord">
							<p>Canada</p>
							<p>391, ave Sainte-Croix <br>Saint-Laurent (Montréal) <br>QC  H4N 2L3</p>
						</div>
						<div class="coord">
							<p>USA</p>
							<p>151, New Park Avenue <br>Hartford, Connecticut <br>06106</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom align-items-center">
				<div class="col-xl-4 d-flex justify-content-center order-2 order-xl-1 pb-4 pb-xl-0">
					<div>
						<?php echo date("Y"); ?> <span class="sep"></span> <strong>ALPHACASTING INC.</strong> <span class="sep"></span> <?php Lang::write('f-copyright'); ?>.
						<div class="mt-1">
							Made with 
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 147.6 111.6" style="enable-background:new 0 0 147.6 111.6;" xml:space="preserve">
								<path class="st0" d="M73.8,16.93c0,0,12.1-16.66,36-16.66c21.31,0,36,16.06,36,35.05c0,25.7-27.98,44.12-72,76 c-44.02-31.88-72-50.3-72-76c0-18.99,14.69-35.05,36-35.05C61.7,0.27,73.8,16.93,73.8,16.93z"/>
							</svg> 
							by <a href="https://programaction.com/?utm_source=alphacasting.com" target="_blank">Programaction</a>
						</div>
					</div>
				</div>
				<div class="col-xl-8 py-4 order-1 order-xl-2">
					<ul class="d-flex justify-content-center">
						<li<?php if($pagesKey=='home') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['home']['url']; ?>"><?php echo $pages['home']['nom']; ?></a>
						</li>
						<li<?php if($pagesKey=='about' || $pages[$pagesKey]['sub']=='about') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['about']['url']; ?>"><?php echo $pages['about']['titre']; ?></a>
						</li>
						<li<?php if($pagesKey=='casting' || $pages[$pagesKey]['sub']=='casting') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['casting']['url']; ?>"><?php echo $pages['casting']['nom']; ?></a>
						</li>
						<li<?php if($pagesKey=='markets' || $pages[$pagesKey]['sub']=='markets') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['markets']['url']; ?>"><?php echo $pages['markets']['nom']; ?></a>
						</li>
						<li<?php if($pagesKey=='news') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['news']['url']; ?>"><?php echo $pages['news']['nom']; ?></a>
						</li>
						<li<?php if($pagesKey=='career') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['career']['url']; ?>"><?php echo $pages['career']['nom']; ?></a>
						</li>
						<li<?php if($pagesKey=='contact') echo ' class="active"'; ?>>
							<a href="<?php echo $pages['contact']['url']; ?>"><?php echo $pages['contact']['nom']; ?></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</div>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/aos.js"></script>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/jquery.validate.min.js"></script>
<?php if ($lang=='FR') { ?>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/localization/messages_fr.min.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo $assetsPath; ?>js/global.js?v=<?php echo $FilesVersion; ?>"></script>