<?php 

namespace Programaction;

/**
 * La classe news utlise les paramètres GET "PAGE et SLUG"
 */
class News {
    static $DB;
    static $limit = 5;

    public static function connect(){
        if(self::$DB == null){
            include "connexion.inc.php";
            self::$DB = $conn;
        }
    }

    public static function get_by_slug($slug){
        self::connect();
        $sql = "SELECT date,titre,image,texte FROM nouvelles WHERE slug = ? ";
        $stmt = self::$DB->prepare($sql);
        $stmt->bind_param("s", $slug);
        $stmt->bind_result($date,$titre,$image,$texte);
        $result = $stmt->execute();
        $result = $stmt->fetch();
        return [
            'date'=>$date,'titre'=>$titre,'image'=>$image,'texte'=>$texte
        ];
    }
        
    /**
     * Va chercher les nouvelles selon la langue, doit donner une limite.
     *
     * @param  mixed $lang
     * @return array
     */
    public static function get_news_by_lang($lang){
        self::connect();
        $langID=$lang=='EN'?1:2;
        $offset = ( ( isset($_GET['page']) ? intval($_GET['page']) : 1 ) -1 ) * self::$limit;
        $limit = self::$limit;

        $sql = "SELECT id,date,titre,slug,intro,texte,image FROM nouvelles WHERE lang = $langID AND YEAR(date)>='2022' ORDER BY date DESC LIMIT $limit OFFSET $offset ";
        $result = self::$DB->query($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    /**
     * Va cherche le nombre de nouvelle selon la langue
     *
     * @param  int $lang
     * @return int
     */
    public static function get_news_count($lang){
        self::connect();
        $langID=$lang=='EN'?1:2;
        $sql = "SELECT id FROM nouvelles WHERE lang = $langID AND YEAR(date)>='2022'";
        $result = self::$DB->query($sql);
        return $result->num_rows;
    }
    
    /**
     * Affiche une pagination selon la limite de résulta par page.
     *
     * @param  int $lang
     * @return void
     */
    public static function paginator($lang){
        $curpage = isset($_GET['page'])?$_GET['page']:1;
        $total_news = self::get_news_count($lang);
        $pages = intval(floor($total_news / self::$limit )) + (($total_news % self::$limit > 0)?1:0);
        $canNext = $curpage < $pages;
        $canPrev = $curpage > 1;

        ?>
            <ul class="pagination">
                <?php if($canPrev): ?>
                    <li><a href="<?php echo self::url_with_page($curpage-1) ?>">‹</a></li>
                <?php endif; ?>
                <?php for($i = 1;$i<=$pages;$i++): ?>
                    <li class="number <?php echo $i == $curpage?'current':'' ?>" ><a href="<?php echo self::url_with_page($i) ?>"><?php echo $i ?></a></li>
                <?php endfor; ?>
                <?php if($canNext): ?>
                    <li><a href="<?php echo self::url_with_page($curpage+1) ?>">›</a></li>
                <?php endif; ?>
            </ul>
        <?php
    }
    
    /**
     * addParam
     *
     * @param  array $params
     * @return void
     */
    public static function url_with_page($page){
        $url = parse_url($_SERVER['REQUEST_URI']);
        $q = [];
        if(isset($url['query'])){
            parse_str($url['query'], $q);
        }
        $path_arr = explode('/',$url['path']);

        if(end($path_arr) == 'news' || end($path_arr) == 'nouvelles'){
            return $url['path'].'/'.$page. (count($q)>0?'?' . http_build_query($q):'');
        }else{
            array_pop($path_arr);
            array_push($path_arr, $page);
            return implode('/',$path_arr). (count($q)>0?'?' . http_build_query($q):'');
        }
        
    }

}