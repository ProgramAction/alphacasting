<?php
$headerClass=$pages[$pagesKey]['headerClass'];
?>
<header class="<?php echo $headerClass; ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2 col-md-3 col-lg-4 py-4">
                <a href="#" class="open-menu ml-lg-4">Menu</a>
            </div>
            <div class="col-10 col-md-6 col-lg-4 py-4 text-right text-lg-center">
                <a href="./">
                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41.74 27.08" class="plane"><polygon class="plane-red" points="28.43 6 29.43 4.56 25.74 3.97 25.5 4.37 27.28 6.09 28.43 6"/><path class="plane-red" d="M34.69,20.9l2.22-.64,3.34-3.42a9.34,9.34,0,0,1,1.06-1,7,7,0,0,1,.65-.53l.88-.62a7.58,7.58,0,0,1,2-1,2.63,2.63,0,0,0-1.19-.28A5.92,5.92,0,0,0,40.2,14.9a17.78,17.78,0,0,0-3,2.78l-1.68,2A9.19,9.19,0,0,0,34.69,20.9Z" transform="translate(-6.13 -13.46)"/><path class="plane-red" d="M24.2,25.3l8.17-.42,4.15-4.05L24.9,24.08a1.88,1.88,0,0,0-.67.39,1,1,0,0,0-.25.63C24,25.2,24.05,25.26,24.2,25.3Z" transform="translate(-6.13 -13.46)"/><path class="plane-red" d="M42.45,21.35s1.69-2.09,2.33-2.93A12.59,12.59,0,0,0,46,17a5.55,5.55,0,0,0,.75-1.34h0a1.42,1.42,0,0,0,.11-.5,1.11,1.11,0,0,0-1.24-1.27,3.9,3.9,0,0,0-1.8.66,19.88,19.88,0,0,0-3.21,2.84l-2.66,2.69S28,30.24,23.41,34.1C15.59,40.64,6.31,40,6.7,28.79c0,0-2,5.5,1.59,9.05,5.18,5.08,12,1.82,15.69-.53A83,83,0,0,0,38.88,25.2l7.6-.22a1.37,1.37,0,0,0,1.19-.9l.2-.37Z" transform="translate(-6.13 -13.46)"/><path class="plane-bottom" d="M29.64,33.35a82.43,82.43,0,0,0,9.24-8.15l7.6-.22a1.37,1.37,0,0,0,1.19-.9l.2-.37-5.42-2.36s1.69-2.09,2.33-2.93A12.59,12.59,0,0,0,46,17a5.55,5.55,0,0,0,.75-1.34h0a1.42,1.42,0,0,0,.11-.5,1.11,1.11,0,0,0-1.24-1.27,3.9,3.9,0,0,0-1.8.66,19.88,19.88,0,0,0-3.21,2.84l-2.66,2.69s-6,6.16-10.85,10.71C28,31.65,28.83,32.51,29.64,33.35Z" transform="translate(-6.13 -13.46)"/><path class="plane-bottom" d="M23.41,34.1c-3.25,2.71-6.74,4.19-9.7,4.19-4.16,0-7.24-2.94-7-9.5,0,0-2,5.5,1.59,9.05a9,9,0,0,0,6.49,2.69c3.5,0,6.93-1.78,9.2-3.22.64-.41,1.25-.82,1.86-1.23-.64-.73-1.33-1.5-2.08-2.29Z" transform="translate(-6.13 -13.46)"/><path class="plane-tail-end" d="M14,21.88c-2.48-.16-6.08,2.45-6.4,5.3,3.82-4.55,10.88,1.05,15.94,6.33,1-.81,2.09-1.84,3.3-3C22.25,26.2,17.11,22.08,14,21.88Z" transform="translate(-6.13 -13.46)"/><path class="plane-tail-end" d="M29.3,40.38a.35.35,0,0,0,.3.16h5.57c.34,0,.52-.48.28-.78-1-1.22-3-3.56-5.55-6.15-1.25,1-2.52,1.87-3.82,2.74C27.76,38.31,29,39.88,29.3,40.38Z" transform="translate(-6.13 -13.46)"/><polygon class="plane-red" points="24.78 8.5 28.14 7.56 24.94 4.42 25.36 3.68 19.74 5.43 18.84 7.11 23.85 6.52 24.78 8.5"/></svg>
                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 258.67 17.42" class="alphacasting"><path class="alpha" d="M21.94,35.43a.66.66,0,0,1-.17.14.46.46,0,0,1-.23.08H15.09a.4.4,0,0,1-.36-.2.43.43,0,0,1,0-.44l9-16.5a.38.38,0,0,1,.17-.16.48.48,0,0,1,.21-.06H31a.49.49,0,0,1,.22.06.45.45,0,0,1,.15.16l9,16.5a.39.39,0,0,1,0,.44.43.43,0,0,1-.38.2H33.53a.4.4,0,0,1-.22-.08.52.52,0,0,1-.15-.14L31.83,33l-2.67-5-.77-1.48h0c-.11-.17-.19-.32-.25-.43a3.07,3.07,0,0,0-.17-.31c-.06-.09-.13-.21-.2-.34l-.29-.53Z" transform="translate(-14.66 -18.29)"/><path class="alpha" d="M58,30.21a.37.37,0,0,1,.42.42v4.6a.44.44,0,0,1-.12.3.41.41,0,0,1-.3.12H41.68a.41.41,0,0,1-.3-.12.44.44,0,0,1-.12-.3V18.71a.41.41,0,0,1,.42-.42h5.7a.41.41,0,0,1,.42.42v11.5Z" transform="translate(-14.66 -18.29)"/><path class="alpha" d="M66,35.23a.44.44,0,0,1-.12.3.41.41,0,0,1-.3.12H59.93a.41.41,0,0,1-.3-.12.4.4,0,0,1-.12-.3V18.71a.37.37,0,0,1,.12-.3.41.41,0,0,1,.3-.12h13a16.17,16.17,0,0,1,3.1.26,5,5,0,0,1,2.09.92,3.67,3.67,0,0,1,1.2,1.75A8.85,8.85,0,0,1,79.69,24,12.25,12.25,0,0,1,79.38,27a4.9,4.9,0,0,1-1,2.06,4.24,4.24,0,0,1-1.8,1.22,8.19,8.19,0,0,1-2.7.39H66Zm0-9.72h6.41a1.29,1.29,0,0,0,.76-.23.83.83,0,0,0,.34-.65.87.87,0,0,0-.36-.53,1.12,1.12,0,0,0-.68-.23H66Z" transform="translate(-14.66 -18.29)"/><path class="alpha" d="M87.33,29.79v5.44a.44.44,0,0,1-.12.3.41.41,0,0,1-.3.12H81.22a.41.41,0,0,1-.3-.12.44.44,0,0,1-.12-.3V18.71a.41.41,0,0,1,.42-.42h5.69a.41.41,0,0,1,.42.42v5.23h8.86V18.71a.41.41,0,0,1,.42-.42h5.7a.42.42,0,0,1,.3.12.41.41,0,0,1,.12.3V35.23a.44.44,0,0,1-.12.3.42.42,0,0,1-.3.12h-5.7a.41.41,0,0,1-.3-.12.44.44,0,0,1-.12-.3V29.79Z" transform="translate(-14.66 -18.29)"/><path class="alpha" d="M110.39,35.43a.66.66,0,0,1-.17.14.43.43,0,0,1-.23.08h-6.45a.4.4,0,0,1-.36-.2.43.43,0,0,1,0-.44l9-16.5a.44.44,0,0,1,.17-.16.48.48,0,0,1,.21-.06h6.8a.49.49,0,0,1,.22.06.45.45,0,0,1,.15.16l9,16.5a.39.39,0,0,1,0,.44.43.43,0,0,1-.37.2H122a.4.4,0,0,1-.22-.08.69.69,0,0,1-.15-.14L120.28,33l-2.67-5-.77-1.48h0l-.24-.43-.18-.31c-.06-.09-.13-.21-.2-.34l-.29-.53Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M134.7,30.21h13.92a.38.38,0,0,1,.42.42v4.58a.4.4,0,0,1-.42.42H134.73a6.44,6.44,0,0,1-4.49-1.31,3.8,3.8,0,0,1-1.1-1.61,6.41,6.41,0,0,1-.35-2.23v-7a4.85,4.85,0,0,1,1.45-3.87,6.44,6.44,0,0,1,4.49-1.3h13.89a.41.41,0,0,1,.31.11.53.53,0,0,1,.11.29v4.68a.42.42,0,0,1-.11.29.48.48,0,0,1-.31.13H134.68Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M157.25,35.43a.58.58,0,0,1-.16.14.46.46,0,0,1-.23.08h-6.45a.42.42,0,0,1-.37-.2.46.46,0,0,1,0-.44l9.06-16.5a.41.41,0,0,1,.16-.16.48.48,0,0,1,.21-.06h6.81a.49.49,0,0,1,.22.06.45.45,0,0,1,.15.16l9,16.5a.39.39,0,0,1,0,.44.44.44,0,0,1-.38.2h-6.36a.4.4,0,0,1-.22-.08.41.41,0,0,1-.15-.14L167.15,33l-2.67-5-.78-1.48h0l-.24-.43-.17-.31c-.06-.09-.13-.21-.2-.34l-.29-.53Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M192.22,24.58a5.49,5.49,0,0,1,2.21.58,3.29,3.29,0,0,1,1.43,1.32l.09.13a8.26,8.26,0,0,1,.31.77c.09.25.17.52.24.8a8.87,8.87,0,0,1,.14.9c0,.3,0,.62,0,1,0,2-.44,3.41-1.32,4.26a5.65,5.65,0,0,1-4.23,1.35H179.54a3.13,3.13,0,0,1-1.1-.18,3,3,0,0,1-.85-.46,3.27,3.27,0,0,1-.63-.66,3.52,3.52,0,0,1-.44-.74,6.41,6.41,0,0,1-.56-2.05v-.51c0-.29.14-.44.42-.44h12.7l.48,0a2.26,2.26,0,0,0,.41-.09.5.5,0,0,0,.39-.49.7.7,0,0,0-.07-.31.38.38,0,0,0-.26-.22,1.14,1.14,0,0,0-.33-.08l-.35,0-8.75,0a5.49,5.49,0,0,1-2.21-.55,4.48,4.48,0,0,1-1.52-1.24c-.13-.2-.27-.44-.41-.7a4.45,4.45,0,0,1-.34-.89A7.56,7.56,0,0,1,175.9,24,6.84,6.84,0,0,1,177,19.75a3.58,3.58,0,0,1,1.48-1.07,5.81,5.81,0,0,1,2.21-.37h12.21a5.14,5.14,0,0,1,1.55.46,5,5,0,0,1,1.19.87,2.8,2.8,0,0,1,.78,1.52l.09,1.74a.42.42,0,0,1-.11.31.56.56,0,0,1-.32.13H183.3a2.06,2.06,0,0,0-.76.14.48.48,0,0,0-.37.46.87.87,0,0,0,.14.4c.1.16.34.25.72.26Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M211.61,35.23a.44.44,0,0,1-.12.3.41.41,0,0,1-.3.12h-5.68a.41.41,0,0,1-.3-.12.44.44,0,0,1-.12-.3V23.57h-6.51a.38.38,0,0,1-.3-.13.4.4,0,0,1-.12-.29V18.71a.41.41,0,0,1,.42-.42h19.3a.41.41,0,0,1,.42.42v4.44a.4.4,0,0,1-.12.29.38.38,0,0,1-.3.13h-6.27Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M225.37,18.29a.44.44,0,0,1,.3.12.41.41,0,0,1,.12.3V35.23a.41.41,0,0,1-.42.42h-5.68a.41.41,0,0,1-.3-.12.44.44,0,0,1-.12-.3V18.71a.41.41,0,0,1,.42-.42Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M233.16,25.33v9.83a.41.41,0,0,1-.42.42h-5.43a.41.41,0,0,1-.42-.42V18.71a.41.41,0,0,1,.42-.42h8.61l.17.05a.75.75,0,0,1,.17.1l8.17,10V18.71a.41.41,0,0,1,.42-.42h5.41a.41.41,0,0,1,.42.42V35.16a.37.37,0,0,1-.42.42h-8.55a.62.62,0,0,1-.18,0l-.15-.1Z" transform="translate(-14.66 -18.29)"/><path class="casting" d="M273.29,23.37a.38.38,0,0,1-.13.3.45.45,0,0,1-.31.12H257.7v6a.43.43,0,0,0,.12.19.63.63,0,0,0,.5.15h8.48a.73.73,0,0,0,.5-.14c.09-.08.14-.14.14-.17V29.2l-4-1.82-4-1.83a.38.38,0,0,1-.21-.2.39.39,0,0,1,0-.26.38.38,0,0,1,.39-.33h13.39a.37.37,0,0,1,.42.42v5.5a6.09,6.09,0,0,1-.28,2,3.66,3.66,0,0,1-.94,1.45,5,5,0,0,1-1.39.91,7.17,7.17,0,0,1-1.79.48,9.65,9.65,0,0,1-1.07.11l-1.38.07c-.44,0-1,0-1.71,0h-4.55c-.66,0-1.21,0-1.65,0l-1.36-.07c-.43,0-.79-.06-1.07-.11a6.14,6.14,0,0,1-1.74-.48,4.68,4.68,0,0,1-1.3-.89,3.84,3.84,0,0,1-1-1.47,5.53,5.53,0,0,1-.32-2V23.46a4.9,4.9,0,0,1,1.47-3.87,6.64,6.64,0,0,1,4.51-1.3h15.09a.45.45,0,0,1,.31.12.38.38,0,0,1,.13.3Z" transform="translate(-14.66 -18.29)"/></svg>
                </a>
            </div>
            <div class="d-none d-md-block col-3 col-lg-4 py-4 text-right">
                <ul class="sub d-flex align-items-center justify-content-end mr-lg-4">
                    <li><a href="../<?php echo $pages['client']['url']; ?>" target="_blank"><?php echo $pages['client']['nom']; ?></a></li>
                    <li><a href="<?php echo $urlLang ?>"><?php echo $langAlt; ?></a></li>
                    <li class="phone d-flex">
                        <div class="phone-container">
                            <div class="phone-no"><a href="tel:18005677511">1 800 567-7511</a></div>
                            <div class="phone-icon"><img src="<?php echo $assetsPath; ?>images/icone-telephone.png" width="13" height="18" alt="Téléphone"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container_menu">
        <div class="container">
            <div class="row py-4">
                <div class="col-3 ">
                    <a href="#" class="close-menu ml-lg-4">Close</a>
                </div>
                <div class="col-9">
                    <div class="row flex-column">
                        <div class="col-12 pb-4">
                            <img src="<?php echo $assetsPath; ?>images/logoalphanoir.svg" width="226" height="23" alt="Logo Alphacasting" class="img-fluid">
                        </div>
                        <div class="col-12 pb-4">
                            <span class="leading"><?php Lang::write('f-slogan-leading'); ?></span>
                        </div>
                        <div class="col-12">
                            <ul>
                                <li<?php if($pagesKey=='home') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['home']['url']; ?>"><?php echo $pages['home']['nom']; ?></a>
                                </li>
                                <li<?php if($pages[$pagesKey]['sub']=='about') echo ' class="active"'; ?>>
									<a href="#subabout" data-toggle="collapse" data-target="#subabout" aria-expanded="false" aria-controls="subabout"><?php echo $pages['about']['titre']; ?> <span class="material-icons-outlined">arrow_drop_down</span></a>
									<div class="collapse<?php if($pages[$pagesKey]['sub']=='about') echo ' show'; ?>" id="subabout">
										<ul>
											<li <?php if($pagesKey=='about') echo ' class="active"'; ?>><a href="<?php echo $pages['about']['url']; ?>"><?php echo $pages['about']['nom']; ?></a></li>
											<li <?php if($pagesKey=='certifications') echo ' class="active"'; ?>><a href="<?php echo $pages['certifications']['url']; ?>"><?php echo $pages['certifications']['nom']; ?></a></li>
											<li <?php if($pagesKey=='quality') echo ' class="active"'; ?>><a href="<?php echo $pages['quality']['url']; ?>"><?php echo $pages['quality']['titre']; ?></a></li>
										</ul>
									</div>
                                </li>
                                <li<?php if($pagesKey=='casting' || $pages[$pagesKey]['sub']=='casting') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['casting']['url']; ?>"><?php echo $pages['casting']['nom']; ?></a>
                                </li>
                                <li<?php if($pagesKey=='markets' || $pages[$pagesKey]['sub']=='markets') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['markets']['url']; ?>"><?php echo $pages['markets']['nom']; ?></a>
                                </li>
                                <li<?php if($pagesKey=='news') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['news']['url']; ?>"><?php echo $pages['news']['nom']; ?></a>
                                </li>
                                <li<?php if($pagesKey=='career') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['career']['url']; ?>"><?php echo $pages['career']['nom']; ?></a>
                                </li>
                                <li<?php if($pagesKey=='contact') echo ' class="active"'; ?>>
                                    <a href="<?php echo $pages['contact']['url']; ?>"><?php echo $pages['contact']['nom']; ?></a>
                                </li>
								<li>
									<a href="../<?php echo $pages['client']['url']; ?>" target="_blank"><?php echo $pages['client']['nom']; ?></a>
								</li>
                                <li class="lang">
                                    <a href="<?php echo $urlLang ?>"><?php echo $langAlt; ?></a>
                                </li>
                                <li class="d-flex rs">
                                    <a href="<?php echo $urlFB; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-facebook.png" width="14" height="29" alt="Facebook"></a>
                                    <a href="<?php echo $urlLinkedin; ?>" target="_blank"><img src="<?php echo $assetsPath; ?>images/icone-linkedin.png" width="28" height="29" alt="Linkedin"></a>
                                </li>
                                <li class="search">
                                    <form action="https://google.ca/search" method="get">
                                        <fieldset role="search">
                                            <input type="hidden" name="q" class="q" value="site:www.alphacasting.com">                                                
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">
                                                        <img src="<?php echo $assetsPath; ?>images/icone-loupe.png" alt="Icône loupe pour la recherche">
                                                    </button>
                                                </div>
                                                <input type="search" name="q" results="0" placeholder="SEARCH" id="google-search" class="form-control" aria-label="Effectuer une recherche">
                                            </div>
                                        </fieldset>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="<?php echo $pagesKey; ?>" class="<?php echo $pages[$pagesKey]['sub']; ?>">
	<main class="container-fluid" >