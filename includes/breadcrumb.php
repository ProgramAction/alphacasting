<div class="row breadcrumb">
	<div class="col-xl-11 offset-xl-1">
		<nav aria-label="breadcrumb">
			<ol >
				<li class="breadcrumb-item"><a href="./"><?php Lang::write('home-nom'); ?></a></li>

				<?php if ($pages[$pagesKey]['sub']!="" && $pages[$pagesKey]['sub']!="about" && $pages[$pagesKey]['sub']!="new"): ?>
					<li class="breadcrumb-item"><a href="<?php echo $pages[$pages[$pagesKey]['sub']]['url']; ?>"><?php echo $pages[$pages[$pagesKey]['sub']]['titre']; ?></a></li>
				<?php endif; ?>

				<?php if(isset($new)): // Si on est dans une page Single nouvelle ?>
					<li class="breadcrumb-item"><a href="/<?php echo strtolower($lang).'/'.Lang::insert('news-url') ?>"><?php echo $pages[$pagesKey]['nom']; ?></a></li>
					<li class="breadcrumb-item active" aria-current="page" ><?php echo $new['titre'] ?></li>
            	<?php else: ?>
					<li class="breadcrumb-item active" aria-current="page"><?php echo $pages[$pagesKey]['nom']; ?></li>
            	<?php endif; ?>
			</ol>
		</nav>
	</div>
</div>