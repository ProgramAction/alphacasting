<div class="row py-5">
	<div id="form" class="col-lg-6 col-xl-5 offset-xl-1">
		<div class="contact-form">
			<form id="cf" name="cf" action="../includes/sendemail.php" method="post" autocomplete="off" novalidate="novalidate" enctype="multipart/form-data">
				<div class="form-process"></div>
				<div class="form-group">
					<label for="cf-name"><?php Lang::write('c-form-Name'); ?></label>
					<input type="text" id="cf-name" name="cf-name" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-cie"><?php Lang::write('c-form-Company'); ?></label>
					<input type="text" id="cf-cie" name="cf-cie" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-titre"><?php Lang::write('c-form-titre'); ?></label>
					<input type="text" id="cf-titre" name="cf-titre" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-adr"><?php Lang::write('c-form-adr'); ?></label>
					<input type="text" id="cf-adr" name="cf-adr" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-ville"><?php Lang::write('c-form-ville'); ?></label>
					<input type="text" id="cf-ville" name="cf-ville" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-state"><?php Lang::write('c-form-state'); ?></label>
					<input type="text" id="cf-state" name="cf-state" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-zip"><?php Lang::write('c-form-zip'); ?></label>
					<input type="text" id="cf-zip" name="cf-zip" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-pays"><?php Lang::write('c-form-pays'); ?></label>
					<select name="cf-pays" id="cf-pays" class="form-control rounded-0">
						<option value="AFG">Afghanistan</option>
						<option value="ALB">Albania</option>
						<option value="DZA">Algeria</option>
						<option value="ASM">American Samoa</option>
						<option value="AND">Andorra</option>
						<option value="AGO">Angola</option>
						<option value="AIA">Anguilla</option>
						<option value="ATG">Antigua and Barbuda</option>
						<option value="ARG">Argentina</option>
						<option value="ARM">Armenia</option>
						<option value="ABW">Aruba</option>
						<option value="AUS">Australia</option>
						<option value="AUT">Austria</option>
						<option value="AZE">Azerbaijan</option>
						<option value="BHS">Bahamas</option>
						<option value="BHR">Bahrain</option>
						<option value="BGD">Bangladesh</option>
						<option value="BRB">Barbados</option>
						<option value="BLR">Belarus</option>
						<option value="BEL">Belgium</option>
						<option value="BLZ">Belize</option>
						<option value="BEN">Benin</option>
						<option value="BMU">Bermuda</option>
						<option value="BTN">Bhutan</option>
						<option value="BOL">Bolivia</option>
						<option value="BIH">Bosnia and Herzegovina</option>
						<option value="BWA">Botswana</option>
						<option value="BRA">Brazil</option>
						<option value="VGB">British Virgin Islands</option>
						<option value="BRN">Brunei Darussalam</option>
						<option value="BGR">Bulgaria</option>
						<option value="BFA">Burkina Faso</option>
						<option value="BDI">Burundi</option>
						<option value="KHM">Cambodia</option>
						<option value="CMR">Cameroon</option>
						<option value="CAN" selected="selected">Canada</option>
						<option value="CPV">Cape Verde</option>
						<option value="CYM">Cayman Islands</option>
						<option value="CAF">Central African Republic</option>
						<option value="TCD">Chad</option>
						<option value="CHL">Chile</option>
						<option value="CHN">China</option>
						<option value="COL">Colombia</option>
						<option value="COM">Comoros</option>
						<option value="COG">Congo</option>
						<option value="COK">Cook Islands</option>
						<option value="CRI">Costa Rica</option>
						<option value="HRV">Croatia</option>
						<option value="CUB">Cuba</option>
						<option value="CYP">Cyprus</option>
						<option value="CZE">Czech Republic</option>
						<option value="DNK">Denmark</option>
						<option value="DJI">Djibouti</option>
						<option value="DMA">Dominica</option>
						<option value="DOM">Dominican Republic</option>
						<option value="ECU">Ecuador</option>
						<option value="EGY">Egypt</option>
						<option value="SLV">El Salvador</option>
						<option value="GNQ">Equatorial Guinea</option>
						<option value="ERI">Eritrea</option>
						<option value="EST">Estonia</option>
						<option value="ETH">Ethiopia</option>
						<option value="FLK">Falkland Islands</option>
						<option value="FRO">Faroe Islands</option>
						<option value="FJI">Fiji</option>
						<option value="FIN">Finland</option>
						<option value="FRA">France</option>
						<option value="GUF">French Guiana</option>
						<option value="PYF">French Polynesia</option>
						<option value="GAB">Gabon</option>
						<option value="GMB">Gambia</option>
						<option value="GEO">Georgia</option>
						<option value="DEU">Germany</option>
						<option value="GHA">Ghana</option>
						<option value="GIB">Gibraltar</option>
						<option value="GRC">Greece</option>
						<option value="GRL">Greenland</option>
						<option value="GRD">Grenada</option>
						<option value="GLP">Guadeloupe</option>
						<option value="GTM">Guatemala</option>
						<option value="GIN">Guinea</option>
						<option value="GNB">Guinea Bissau</option>
						<option value="GUY">Guyana</option>
						<option value="HTI">Haiti</option>
						<option value="HND">Honduras</option>
						<option value="HKG">HongKong</option>
						<option value="HUN">Hungary</option>
						<option value="ISL">Iceland</option>
						<option value="IND">India</option>
						<option value="IDN">Indonesia</option>
						<option value="IRN">Iran</option>
						<option value="IRQ">Iraq</option>
						<option value="IRL">Ireland</option>
						<option value="ISR">Israel</option>
						<option value="ITA">Italy</option>
						<option value="CIV">Ivory Coast</option>
						<option value="JAM">Jamaica</option>
						<option value="JPN">Japan</option>
						<option value="JOR">Jordan</option>
						<option value="KAZ">Kazakhstan</option>
						<option value="KEN">Kenya</option>
						<option value="KIR">Kiribati</option>
						<option value="KWT">Kuwait</option>
						<option value="KGZ">Kyrgyzstan</option>
						<option value="LAO">Laos</option>
						<option value="LVA">Latvia</option>
						<option value="LBN">Lebanon</option>
						<option value="LSO">Lesotho</option>
						<option value="LBR">Liberia</option>
						<option value="LBY">Libya</option>
						<option value="LIE">Liechtenstein</option>
						<option value="LTU">Lithuania</option>
						<option value="LUX">Luxembourg</option>
						<option value="MAC">Macau</option>
						<option value="MKD">Macedonia</option>
						<option value="MDG">Madagascar</option>
						<option value="MWI">Malawi</option>
						<option value="MYS">Malaysia</option>
						<option value="MDV">Maldives</option>
						<option value="MLI">Mali</option>
						<option value="MLT">Malta</option>
						<option value="MHL">Marshall Islands</option>
						<option value="MTQ">Martinique</option>
						<option value="MRT">Mauritania</option>
						<option value="MUS">Mauritius</option>
						<option value="MEX">Mexico</option>
						<option value="FSM">Micronesia</option>
						<option value="MDA">Moldova</option>
						<option value="MCO">Monaco</option>
						<option value="MNG">Mongolia</option>
						<option value="MSR">Montserrat</option>
						<option value="MAR">Morocco</option>
						<option value="MOZ">Mozambique</option>
						<option value="MMR">Myanmar</option>
						<option value="NAM">Namibia</option>
						<option value="NPL">Nepal</option>
						<option value="NLD">Netherlands</option>
						<option value="ANT">Netherlands Antilles</option>
						<option value="NCL">New Caledonia</option>
						<option value="NZL">New Zealand</option>
						<option value="NIC">Nicaragua</option>
						<option value="NER">Niger</option>
						<option value="NGA">Nigeria</option>
						<option value="NFK">NorfolkIsland</option>
						<option value="PRK">North Korea</option>
						<option value="MNP">Northern Mariana Islands</option>
						<option value="NOR">Norway</option>
						<option value="OMN">Oman</option>
						<option value="PAK">Pakistan</option>
						<option value="PLW">Palau</option>
						<option value="PAN">Panama</option>
						<option value="PNG">Papua New Guinea</option>
						<option value="PRY">Paraguay</option>
						<option value="PER">Peru</option>
						<option value="PHL">Philippines</option>
						<option value="POL">Poland</option>
						<option value="PRT">Portugal</option>
						<option value="PRI">Puerto Rico</option>
						<option value="QAT">Qatar</option>
						<option value="REU">Reunion</option>
						<option value="ROM">Romania</option>
						<option value="RUS">Russia</option>
						<option value="RWA">Rwanda</option>
						<option value="SHN">Saint Helena</option>
						<option value="KNA">Saint Kitts and Nevis</option>
						<option value="LCA">Saint Lucia</option>
						<option value="SPM">Saint Pierre and Miquelon</option>
						<option value="VCT">Saint Vincent/Grenadines</option>
						<option value="WSM">Samoa</option>
						<option value="SMR">San Marino</option>
						<option value="STP">Saotome and Principe</option>
						<option value="SAU">Saudi Arabia</option>
						<option value="SEN">Senegal</option>
						<option value="SYC">Seychelles</option>
						<option value="SLE">Sierra Leone</option>
						<option value="SGP">Singapore</option>
						<option value="SVK">Slovak Republic</option>
						<option value="SVN">Slovenia</option>
						<option value="SLB">Solomon Islands</option>
						<option value="SOM">Somalia</option>
						<option value="ZAF">South Africa</option>
						<option value="KOR">South Korea</option>
						<option value="ESP">Spain</option>
						<option value="LKA">Sri Lanka</option>
						<option value="SDN">Sudan</option>
						<option value="SUR">Suriname</option>
						<option value="SWZ">Swaziland</option>
						<option value="SWE">Sweden</option>
						<option value="CHE">Switzerland</option>
						<option value="SYR">Syria</option>
						<option value="TWN">Taiwan</option>
						<option value="TJK">Tajikistan</option>
						<option value="TZA">Tanzania</option>
						<option value="THA">Thailand</option>
						<option value="TGO">Togo</option>
						<option value="TKL">Tokelau</option>
						<option value="TON">Tonga</option>
						<option value="TTO">Trinidad and Tobago</option>
						<option value="TUN">Tunisia</option>
						<option value="TUR">Turkey</option>
						<option value="TKM">Turkmenistan</option>
						<option value="TCA">Turks and Caicos Islands</option>
						<option value="VIR">US Virgin Islands</option>
						<option value="UGA">Uganda</option>
						<option value="UKR">Ukraine</option>
						<option value="ARE">United Arab Emirates</option>
						<option value="GBR">United Kingdom</option>
						<option value="USA">United States</option>
						<option value="URY">Uruguay</option>
						<option value="UZB">Uzbekistan</option>
						<option value="VUT">Vanuatu</option>
						<option value="VAT">Vatican</option>
						<option value="VEN">Venezuela</option>
						<option value="VNM">Vietnam</option>
						<option value="ESH">Western Sahara</option>
						<option value="YEM">Yemen</option>
						<option value="YUG">Yugoslavia</option>
						<option value="ZMB">Zambia</option>
						<option value="ZWE">Zimbabwe</option>
					</select>
				</div>
				<div class="form-group">
					<label for="cf-phone"><?php Lang::write('c-form-Telephone'); ?></label>
					<input type="text" id="cf-phone" name="cf-phone" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-fax"><?php Lang::write('c-form-fax'); ?></label>
					<input type="text" id="cf-fax" name="cf-fax" placeholder="" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label for="cf-email"><?php Lang::write('c-form-Email'); ?></label>
					<input type="email" id="cf-email" name="cf-email" placeholder="" class="form-control rounded-0 required">
				</div>
				<div class="form-group">
					<label for="cf-format"><?php Lang::write('c-form-format'); ?></label>
					<select name="cf-format" id="cf-format" class="form-control rounded-0 required">
						<option disabled selected><?php Lang::write('c-form-select'); ?></option>
						<option value="<?php Lang::write('c-form-formatpdf'); ?>"><?php Lang::write('c-form-formatpdf'); ?></option>
						<option value="<?php Lang::write('c-form-formatprint'); ?>"><?php Lang::write('c-form-formatprint'); ?></option>
					</select>
				</div>

				<div class="form-group d-none">
					<label for="website">Website</label>
					<input id="website" name="website" type="text" value="" placeholder="website"  />
				</div>

				<div class="mb-4">
					<div class="g-recaptcha" data-sitekey="6LfGHAATAAAAAAYWE7uAA4x_wHcla3xwcML9lyXZ"></div>
				</div>
				<div class="form-group">
					<button class="btn btn-black" type="submit" id="cf-submit" name="cf-submit"><?php Lang::write('c-form-Send'); ?></button>
				</div>
				<input type="hidden" name="form" id="form" value="quality">
				<input type="hidden" name="lang" value="<?php echo $lang; ?>">
				<input type="hidden" name="assetsPath" value="<?php echo $assetsPath; ?>">
				<input type="hidden" name="sujetRequete" id="sujetRequete" value="Demande Manuel Qualité">
			</form>
			<div class="cf-form-result mt-4"></div>
		</div>
	</div>
</div>
<div class="row border-bottom rouge"></div>