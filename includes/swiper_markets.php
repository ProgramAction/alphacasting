<div class="row py-5 bg-black">
	<div class="swiper-container py-4 markets">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<a href="<?php echo $pages['aerospace']['url']; ?>">
					<span><?php echo $pages['aerospace']['nom']; ?></span>
					<img src="../assets/images/aerospace-thumb.jpg" alt="Airplane" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['medical']['url']; ?>">
					<span><?php echo $pages['medical']['nom']; ?></span>
					<img src="../assets/images/medical-thumb.jpg" alt="Doctor working surgery" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['defence']['url']; ?>">
					<span><?php echo $pages['defence']['nom']; ?></span>
					<img src="../assets/images/defence-thumb.jpg" alt="Fighter jet" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['telecom']['url']; ?>">
					<span><?php echo $pages['telecom']['nom']; ?></span>
					<img src="../assets/images/telecom-thumb.jpg" alt="Satellite in space" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['transport']['url']; ?>">
					<span><?php echo $pages['transport']['nom']; ?></span>
					<img src="../assets/images/transport-thumb.jpg" alt="High-speed train" class="img-fluid w-100">
				</a>
			</div>
			<div class="swiper-slide">
				<a href="<?php echo $pages['commercial']['url']; ?>">
					<span><?php echo $pages['commercial']['nom']; ?></span>
					<img src="../assets/images/commercial-thumb.jpg" alt="Construction Site And Crane" class="img-fluid w-100">
				</a>
			</div>
		</div>
	</div>
</div>