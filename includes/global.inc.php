<?php
date_default_timezone_set("America/Toronto");

include_once 'news-class.php';

$FilesVersion = '20220506';

require 'Lang.php';

Lang::$lang = $lang;

$assetsPath='../assets/';
if ($lang=='FR') {
	$langAlt='EN';
	$langAltAffiche='English';
	$nomSite='Alphacasting Inc.';
} else {
	$langAlt='FR';
	$langAltAffiche='Français';
	$nomSite='Alphacasting Inc.';
}
$urlLang = '../'.strtolower($langAlt).'/';

$pagecourante = basename(substr($_SERVER['PHP_SELF'],0,-4));

if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}

$pages=array(
	'home'=>array(
		'nom'=>Lang::insert('home-nom'),
		'url'=>Lang::insert('home-url'),
		'urlLang'=>Lang::insert('home-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('home-titre'),
		'desc'=>Lang::insert('home-desc'),
		'sub'=>'',
		'headerClass'=>''
	),
	'client'=>array(
		'nom'=>Lang::insert('client-nom'),
		'url'=>Lang::insert('client-url'),
		'urlLang'=>Lang::insert('client-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('client-titre'),
		'desc'=>Lang::insert('client-desc'),
		'sub'=>'',
		'headerClass'=>''
	),
	'about'=>array(
		'nom'=>Lang::insert('about-nom'),
		'url'=>Lang::insert('about-url'),
		'urlLang'=>Lang::insert('about-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('about-titre'),
		'desc'=>Lang::insert('about-desc'),
		'sub'=>'about',
		'headerClass'=>'bgblack-forced'
	),
	'certifications'=>array(
		'nom'=>Lang::insert('cert-nom'),
		'url'=>Lang::insert('cert-url'),
		'urlLang'=>Lang::insert('cert-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('cert-titre'),
		'desc'=>Lang::insert('cert-desc'),
		'sub'=>'about',
		'headerClass'=>'bgblack-forced'
	),
	'quality'=>array(
		'nom'=>Lang::insert('quality-nom'),
		'url'=>Lang::insert('quality-url'),
		'urlLang'=>Lang::insert('quality-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('quality-titre'),
		'desc'=>Lang::insert('quality-desc'),
		's2-img-1'=>'quality-s2-1.jpg',
		's2-img-2'=>'quality-s2-2.jpg',
		'icon'=>array('file'=>'bas-prix.svg','width'=>'270'),
		'sub'=>'about',
		'headerClass'=>'bgblack-forced'
	),
	'quality-form'=>array(
		'nom'=>Lang::insert('quality-form-nom'),
		'url'=>Lang::insert('quality-form-url'),
		'urlLang'=>Lang::insert('quality-form-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('quality-form-titre'),
		'desc'=>Lang::insert('quality-form-desc'),
		'sub'=>'quality',
		'headerClass'=>'bgblack-forced'
	),
	'career'=>array(
		'nom'=>Lang::insert('career-nom'),
		'url'=>Lang::insert('career-url'),
		'urlLang'=>Lang::insert('career-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('career-titre'),
		'desc'=>Lang::insert('career-desc'),
		'sub'=>'',
		'headerClass'=>'bgblack-forced'
	),
	'casting'=>array(
		'nom'=>Lang::insert('casting-nom'),
		'url'=>Lang::insert('casting-url'),
		'urlLang'=>Lang::insert('casting-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('casting-titre'),
		'desc'=>Lang::insert('casting-desc'),
		'sub'=>'',
		'headerClass'=>'bgblack-forced'
	),
	'alloys'=>array(
		'nom'=>Lang::insert('alloys-nom'),
		'url'=>Lang::insert('alloys-url'),
		'urlLang'=>Lang::insert('alloys-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('alloys-titre'),
		'desc'=>Lang::insert('alloys-desc'),
		's1-img'=>'non-ferrous-alloys.jpg',
		's2-img'=>'ferrous-alloys.jpg',
		'icon'=>array('file'=>'alloys-moulding-was.svg','width'=>'270'),
		'img-full'=>'liquid-metal-mold.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'heattreatment'=>array(
		'nom'=>Lang::insert('heattreatment-nom'),
		'url'=>Lang::insert('heattreatment-url'),
		'urlLang'=>Lang::insert('heattreatment-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('heattreatment-titre'),
		'desc'=>Lang::insert('heattreatment-desc'),
		's1-img'=>'heat-treatment-s1.jpg',
		's2-img'=>'heat-treatment-s2.jpg',
		'icon'=>array('file'=>'heat-treatment.svg','width'=>'250'),
		'img-full'=>'heat-treatment-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'sizinghipping'=>array(
		'nom'=>Lang::insert('sizinghipping-nom'),
		'url'=>Lang::insert('sizinghipping-url'),
		'urlLang'=>Lang::insert('sizinghipping-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('sizinghipping-titre'),
		'desc'=>Lang::insert('sizinghipping-desc'),
		's1-img'=>'sizing-hipping-s1.jpg',
		's2-img'=>'sizing-hipping-s2.jpg',
		'icon'=>array('file'=>'sizing-and-hipping.svg','width'=>'250'),
		'img-full'=>'sizing-hipping-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'moulding'=>array(
		'nom'=>Lang::insert('moulding-nom'),
		'url'=>Lang::insert('moulding-url'),
		'urlLang'=>Lang::insert('moulding-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('moulding-titre'),
		'desc'=>Lang::insert('moulding-desc'),
		's1-img'=>'moulding-s1.jpg',
		's2-img'=>'moulding-s2.jpg',
		'icon'=>array('file'=>'alloys-moulding-was.svg','width'=>'250'),
		'img-full'=>'moulding-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'rapidprototyping'=>array(
		'nom'=>Lang::insert('rapidprototyping-nom'),
		'url'=>Lang::insert('rapidprototyping-url'),
		'urlLang'=>Lang::insert('rapidprototyping-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('rapidprototyping-titre'),
		'desc'=>Lang::insert('rapidprototyping-desc'),
		's1-img'=>'rapid-prototyping-s1.jpg',
		's2-img'=>'rapid-prototyping-s2.jpg',
		'icon'=>array('file'=>'rapid-prototyping.svg','width'=>'250'),
		'img-full'=>'rapid-prototyping-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'titanium'=>array(
		'nom'=>Lang::insert('titanium-nom'),
		'url'=>Lang::insert('titanium-url'),
		'urlLang'=>Lang::insert('titanium-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('titanium-titre'),
		'desc'=>Lang::insert('titanium-desc'),
		's1-img'=>'working-titanium.jpg',
		's2-img'=>'titanium-alloys.jpg',
		'icon'=>array('file'=>'titanium.svg','width'=>'250'),
		'img-full'=>'titanium-casting-equipment.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'toolroom'=>array(
		'nom'=>Lang::insert('toolroom-nom'),
		'url'=>Lang::insert('toolroom-url'),
		'urlLang'=>Lang::insert('toolroom-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('toolroom-titre'),
		'desc'=>Lang::insert('toolroom-desc'),
		's1-img'=>'tool-room-s1.jpg',
		's2-img'=>'tool-room-s2.jpg',
		'icon'=>array('file'=>'tool-room.svg','width'=>'250'),
		'img-full'=>'tool-room-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'waxinjection'=>array(
		'nom'=>Lang::insert('waxinjection-nom'),
		'url'=>Lang::insert('waxinjection-url'),
		'urlLang'=>Lang::insert('waxinjection-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('waxinjection-titre'),
		'desc'=>Lang::insert('waxinjection-desc'),
		's1-img'=>'wax-injection-s1.jpg',
		's2-img'=>'wax-injection-s2.jpg',
		'icon'=>array('file'=>'alloys-moulding-was.svg','width'=>'250'),
		'img-full'=>'wax-injection-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'shipping'=>array(
		'nom'=>Lang::insert('shipping-nom'),
		'url'=>Lang::insert('shipping-url'),
		'urlLang'=>Lang::insert('shipping-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('shipping-titre'),
		'desc'=>Lang::insert('shipping-desc'),
		's1-img'=>'shipping-s1.jpg',
		's2-img'=>'shipping-s2.jpg',
		'icon'=>array('file'=>'shipping.svg','width'=>'250'),
		'img-full'=>'shipping-full.jpg',
		'sub'=>'casting',
		'headerClass'=>''
	),
	'markets'=>array(
		'nom'=>Lang::insert('markets-nom'),
		'url'=>Lang::insert('markets-url'),
		'urlLang'=>Lang::insert('markets-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('markets-titre'),
		'desc'=>Lang::insert('markets-desc'),
		'sub'=>'',
		'headerClass'=>'bgblack-forced'
	),
	'aerospace'=>array(
		'nom'=>Lang::insert('aerospace-nom'),
		'url'=>Lang::insert('aerospace-url'),
		'urlLang'=>Lang::insert('aerospace-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('aerospace-titre'),
		'desc'=>Lang::insert('aerospace-desc'),
		'sub'=>'markets',
		'headerClass'=>'aerospace',
		'video'=>Lang::insert('aerospace-video')
	),
	'medical'=>array(
		'nom'=>Lang::insert('medical-nom'),
		'url'=>Lang::insert('medical-url'),
		'urlLang'=>Lang::insert('medical-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('medical-titre'),
		'desc'=>Lang::insert('medical-desc'),
		'sub'=>'markets',
		'headerClass'=>'',
		'video'=>Lang::insert('medical-video')
	),
	'defence'=>array(
		'nom'=>Lang::insert('defence-nom'),
		'url'=>Lang::insert('defence-url'),
		'urlLang'=>Lang::insert('defence-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('defence-titre'),
		'desc'=>Lang::insert('defence-desc'),
		'sub'=>'markets',
		'headerClass'=>'',
		'video'=>Lang::insert('defence-video')
	),
	'telecom'=>array(
		'nom'=>Lang::insert('telecom-nom'),
		'url'=>Lang::insert('telecom-url'),
		'urlLang'=>Lang::insert('telecom-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('telecom-titre'),
		'desc'=>Lang::insert('telecom-desc'),
		'sub'=>'markets',
		'headerClass'=>'',
		'video'=>Lang::insert('telecom-video')
	),
	'transport'=>array(
		'nom'=>Lang::insert('transport-nom'),
		'url'=>Lang::insert('transport-url'),
		'urlLang'=>Lang::insert('transport-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('transport-titre'),
		'desc'=>Lang::insert('transport-desc'),
		'sub'=>'markets',
		'headerClass'=>'',
		'video'=>Lang::insert('transport-video')
	),
	'commercial'=>array(
		'nom'=>Lang::insert('commercial-nom'),
		'url'=>Lang::insert('commercial-url'),
		'urlLang'=>Lang::insert('commercial-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('commercial-titre'),
		'desc'=>Lang::insert('commercial-desc'),
		'sub'=>'markets',
		'headerClass'=>'',
		'video'=>Lang::insert('commercial-video')
	),
	'news'=>array(
		'nom'=>Lang::insert('news-nom'),
		'url'=>Lang::insert('news-url'),
		'urlLang'=>Lang::insert('news-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('news-titre'),
		'desc'=>Lang::insert('news-desc'),
		'sub'=>'new',
		'headerClass'=>'bgblack-forced'
	),
	'case'=>array(
		'nom'=>Lang::insert('case-nom'),
		'url'=>Lang::insert('case-url'),
		'urlLang'=>Lang::insert('case-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('case-titre'),
		'desc'=>Lang::insert('case-desc'),
		'sub'=>'',
		'headerClass'=>''
	),
	'contact'=>array(
		'nom'=>Lang::insert('contact-nom'),
		'url'=>Lang::insert('contact-url'),
		'urlLang'=>Lang::insert('contact-url',array('lang'=>$langAlt)),
		'titre'=>Lang::insert('contact-titre'),
		'desc'=>Lang::insert('contact-desc'),
		'sub'=>'',
		'headerClass'=>'bgblack-forced'
	)
);

$pagesKey = array_recursive_search_key_map($pagecourante, $pages)[0];
$pageInfos = $pages[$pagesKey];

$pageTitre=$pageInfos['titre'].' | '.$nomSite;
if ($pagesKey=='home') {
    $pageTitre=$pageInfos['titre'];
}
$pageDesc = $pageInfos['desc'];

$urlFB = 'https://www.facebook.com/Alphacasting/';
$urlLinkedin = 'https://www.linkedin.com/company/alphacasting-mtl';
$urlInstagram = '';
$urlYoutube = '';
$urlTwitter = '';

if ($pagesKey!='home') {
	$urlLang .= $pages[$pagesKey]['urlLang'];
}

//Trouver l'index de la page
function array_recursive_search_key_map($needle, $haystack) {
    foreach($haystack as $first_level_key=>$value) {
        if ($needle === $value) {
            return array($first_level_key);
        } elseif (is_array($value)) {
            $callback = array_recursive_search_key_map($needle, $value);
            if ($callback) {
                return array_merge(array($first_level_key), $callback);
            }
        }
    }
    return false;
}

function baseURL(){
	return sprintf( "%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['HTTP_HOST'], dirname($_SERVER['PHP_SELF']) );
}

$baseURL = baseURL();
if ($_SERVER['SERVER_NAME']=="localhost" || $_SERVER['SERVER_NAME']=="www.localhost" || $lang=="EN") {
	$baseURL .= "/";
}

$arrayMoisFR = array(1=>"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

function afficheDateFR($date) {
	global $arrayMoisFR;
	$mois = $arrayMoisFR[date("n", strtotime($date))];
	return date("j",strtotime($date)).' '.strtolower($mois).' '.date("Y",strtotime($date));
}

function afficheDateEN($date) {
	return date("F",strtotime($date)).' '.date("j",strtotime($date)).', '.date("Y",strtotime($date));
}

$funcDate = 'afficheDate'.$lang;

?>